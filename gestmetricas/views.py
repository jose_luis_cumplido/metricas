import json
import logging
import sys
import uuid
import zipfile
from collections import namedtuple
import base64
import io
from datetime import datetime
from doctest import TestResults
from uuid import UUID

import django_filters
import requests
from django.core.files.base import ContentFile
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseServerError, HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from requests import Response
from rest_framework import viewsets, status, decorators, response, permissions
from django.contrib.auth.models import User


from django.core import serializers
# Create your views here.
from rest_framework.permissions import AllowAny

from gestmetricas.models import ExcelMetricas, ExcelUpdates, TestDefinition, StatusInfo, TestsQA, IosTestRun, \
    IosTestResults, StepsDefinition, StepsResultsIos, AndroidTestRun, Feature, StepsResults, AndroidTestResults, \
    StatusInfoPL, ConfluenceInfo, ZeplinProject, ZeplinScreen, ZeplinScreenVersion, ZeplinScreenComponent, \
    IosTestResultsCert
from gestmetricas.serializers import ExcelMetricasSerializer, UserSerializer, TestDefinitionSerializer, \
    StatusInfoSerializer, TestsQASerializer, IosTestRunSerializer, IosTestResultsSerializer, StepsDefinitionSerializer, \
    StepsResultsIosSerializer, FeatureSerializer, AndroidTestRunSerializer, AndroidTestResultsSerializer, \
    StepsResultsSerializer, StatusInfoPLSerializer, TestsQACertSerializer, IosTestRunCertSerializer, \
    IosTestResultsCertSerializer, StepsDefinitionCertSerializer, StepsResultsIosCertSerializer, FeatureCertSerializer
from gestmetricas.utils.ParseExcelMetricas import parseExcelMetricas
from gestmetricas.utils.ParseExcelTestDefinition import parseExcelTestDef, parseExcelTestStatus, parseExcelTestStatusPL
from gestmetricas.utils.graphs.plotutils import pltcreatepie, pltcreatebarruns
from gestmetricas.utils.htmlparser import createsteps, createstepsfromhtml, createrunfromhtml, createstepsfromhtml2, \
    createrunfromhtml2, createrunfromhtml2android
from gestmetricas.utils.htmlparsercert import createstepsfromhtmlcert, createstepsfromhtml2cert, createrunfromhtmlcert, \
    createrunfromhtml2cert
from gestmetricas.utils.htmlparsercertandroid import createrunfromhtml2certandroid
from gestmetricas.utils.htmlparsernewlogs import createnewlogsfromhtml
from gestmetricas.utils.jira.jirautils import listjiraissues2, makejiracertreport
from gestmetricas.utils.jsonparsenewlogs import createnewlogsfromjson
from gestmetricas.utils.jsonparser.parsefile import createrunfromjson, createandroidrunfromjson
from atlassian import Confluence

from django.db.models import Lookup

from gestmetricas.utils.zeplin.project import ProjectElement
from gestmetricas.utils.zeplin.screen import Screen
from gestmetricas.utils.zeplin.screencomponent import  ScreenComponents
from gestmetricas.utils.zeplin.screenversion import ScreenVersion

zeplintoken='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0eXBlIjoicGVyc29uYWxfYWNjZXNzX3Rva2VuIiwiY2xpZW50X2lkIjoiNjE5YmFkZDVjNjE3YzZhODIxMTIwNzMwIiwic2NvcGUiOiJhZG1pbiIsImlhdCI6MTYzNzU5MjUzMywiZXhwIjoxOTUzMTYxNzkzLCJpc3MiOiJ6ZXBsaW46YXBpLnplcGxpbi5pbyIsInN1YiI6IjVjMzczZWYwZWRkYzUwNGJkMjM5YzMyMyIsImp0aSI6ImNkZDNkNWY0LTI4OGItNDNiMi05MWM5LTQ5YTk1MDhjN2JlOSJ9.vhGIWrcLKs4RKdDI5Ji1toxSlZJBIlKpFy-GrSQ-7zA'


class NotEqual(Lookup):
    lookup_name = 'ne'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params


class IosTestResultFilter(django_filters.FilterSet):
    has_accessiderror = django_filters.CharFilter(name="accesibilityid", lookup_type='ne')
    class Meta:
        model = IosTestResults
        fields = ['runid', 'test', 'estado', 'has_accessiderror']


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class ExcelMetricasViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows
    """
    queryset = ExcelMetricas.objects.all()
    serializer_class = ExcelMetricasSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}

class TestDefinitionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows .
    """
    queryset =  TestDefinition.objects.all()
    serializer_class =  TestDefinitionSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class StatusInfoViewSet(viewsets.ModelViewSet):

    queryset =  StatusInfo.objects.select_related('scenarioid').all()
    serializer_class =  StatusInfoSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}



class StatusInfoPLViewSet(viewsets.ModelViewSet):

    queryset =  StatusInfoPL.objects.select_related('scenarioid').all()
    serializer_class =  StatusInfoPLSerializer



class TestsQAViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['test', 'feature']
    queryset = TestsQA.objects.all()
    serializer_class = TestsQASerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class IosTestRunViewSet(viewsets.ModelViewSet):
    queryset = IosTestRun.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runtype']
    serializer_class = IosTestRunSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class IosTestResultsViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runid', 'test', 'estado']


    queryset = IosTestResults.objects.all().select_related('runid').select_related('test').select_related('feature')
    serializer_class = IosTestResultsSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class StepsDefinitionViewSet(viewsets.ModelViewSet):
    queryset = StepsDefinition.objects.all()
    serializer_class = StepsDefinitionSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}



class StepsResultsIosViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runid', 'stepid','resultsid','testid', 'estado','accesibilityid']
    queryset = StepsResultsIos.objects.select_related('runid').select_related('stepid').select_related('testid').select_related('resultsid').all()
    serializer_class = StepsResultsIosSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class FeatureViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'creationdate']
    queryset = Feature.objects.all()
    serializer_class = FeatureSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class AndroidTestRunViewSet(viewsets.ModelViewSet):
    queryset = AndroidTestRun.objects.all()
    serializer_class = AndroidTestRunSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class AndroidTestResultsViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runid', 'test', 'estado']
    queryset = AndroidTestResults.objects.all().select_related('runid').select_related('test')
    serializer_class = AndroidTestResultsSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}



class StepsResultsViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runid', 'stepid','resultsid','testid', 'estado','accesibilityid']
    queryset = StepsResults.objects.all().select_related('runid').select_related('stepid').select_related('testid').select_related('resultsid')
    serializer_class = StepsResultsSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}






class TestsQACertViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['test', 'feature']
    queryset = TestsQA.objects.all()
    serializer_class = TestsQACertSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class IosTestRunCertViewSet(viewsets.ModelViewSet):
    queryset = IosTestRun.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runtype']
    serializer_class = IosTestRunCertSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class IosTestResultsCertViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runid', 'test', 'estado','country']
    queryset = IosTestResults.objects.all().select_related('runid').select_related('test').select_related('feature')
    serializer_class = IosTestResultsCertSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class StepsDefinitionCertViewSet(viewsets.ModelViewSet):
    queryset = StepsDefinition.objects.all()
    serializer_class = StepsDefinitionCertSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}



class StepsResultsIosCertViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['runid', 'stepid','resultsid','testid', 'estado','accesibilityid']
    queryset = StepsResultsIos.objects.select_related('runid').select_related('stepid').select_related('testid').select_related('resultsid').all()
    serializer_class = StepsResultsIosCertSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}


class FeatureCertViewSet(viewsets.ModelViewSet):
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'creationdate']
    queryset = Feature.objects.all()
    serializer_class = FeatureCertSerializer
    permission_classes_by_action = {'create': [AllowAny],
                                    'list': [AllowAny]}





@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def updatefile(request):
    data2=request.data

    #evt = LaptrackerEvento.objects.filter( id=data2["evento"]).first()
    tipofichero=data2["tipofichero"]
    datosfichero = data2["file"]
    dataraw = base64.b64decode(datosfichero)
    f = io.BytesIO(base64.b64decode(datosfichero))
    data = ContentFile(base64.b64decode(datosfichero))

  #  evt.pdf.save('eventos/'+uuid+'/'+tipofichero, ContentFile('more content'.encode()))
    if tipofichero == 'excel':
        evt = ExcelUpdates()
        evt.ultimaactualizacion = datetime.now()
        xid = str(uuid.uuid4())
        evt.save()
        dataxls = parseExcelMetricas(f, xid)
      #  dataxls['url'] = evt.xlsparticipantes.url
        evt.excelfile.save('importaciones/'+xid+'/excelmetrcas.xlsx', data)
        evt.ultimaactualizacion = datetime.now()
        evt.save()

    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def updateexcel(request):
    data2=request.data

    #evt = LaptrackerEvento.objects.filter( id=data2["evento"]).first()
    tipofichero=data2["tipofichero"]
    datosfichero = data2["file"]
    dataraw = base64.b64decode(datosfichero)
    f = io.BytesIO(base64.b64decode(datosfichero))
    data = ContentFile(base64.b64decode(datosfichero))

  #  evt.pdf.save('eventos/'+uuid+'/'+tipofichero, ContentFile('more content'.encode()))
    if tipofichero == 'testdef':
        dataxls = parseExcelTestDef(f, "AS IS_ Tests Automaticos")
      #  dataxls['url'] = evt.xlsparticipantes.url
    elif tipofichero == 'teststatus':
        dataxls = parseExcelTestStatus(f, "Listado")
        findrelations()
    elif tipofichero == 'teststatuspl':
        dataxls = parseExcelTestStatusPL(f, "Listado")
        findrelationspl()
    #  dataxls['url'] = evt.xlsparticipantes.url

    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getcounters(request):
    data2=request.data

    cdef = TestDefinition.objects.count()
    cstat = StatusInfo.objects.count()
    cstatpending = StatusInfo.objects.filter(status='PENDING').count()
    cstatimplemented = StatusInfo.objects.filter(status='IMPLEMENTED').count()
    cstatcannot = StatusInfo.objects.filter(status='COULD NOT BE IMPLEMENTED').count()
    cstatinprogress = StatusInfo.objects.filter(status='IN PROGRESS').count()
    cstatblocked = StatusInfo.objects.filter(status='BLOCKED').count()

    listsp=['Log in','Bizum']

    cstathub = StatusInfo.objects.filter(~Q(bloque__in=listsp)).count()
    cstatpendinghub = StatusInfo.objects.filter(Q(status='PENDING') & ~Q(bloque__in=listsp)).count()
    cstatimplementedhub = StatusInfo.objects.filter( Q(status='IMPLEMENTED') & ~Q(bloque__in=listsp)).count()
    cstatcannothub = StatusInfo.objects.filter(Q(status='COULD NOT BE IMPLEMENTED') & ~Q(bloque__in=listsp)).count()
    cstatinprogresshub = StatusInfo.objects.filter( Q(status='IN PROGRESS') & ~Q(bloque__in=listsp)).count()
    cstatblockedhub = StatusInfo.objects.filter( Q(status='BLOCKED') & ~Q(bloque__in=listsp)).count()

    cstatsp = StatusInfo.objects.filter(Q(bloque__in=listsp)).count()
    cstatpendingsp = StatusInfo.objects.filter(Q(status='PENDING') & Q(bloque__in=listsp)).count()
    cstatimplementedsp = StatusInfo.objects.filter(Q(status='IMPLEMENTED') & Q(bloque__in=listsp)).count()
    cstatcannotsp = StatusInfo.objects.filter(Q(status='COULD NOT BE IMPLEMENTED') & Q(bloque__in=listsp)).count()
    cstatinprogresssp = StatusInfo.objects.filter(Q(status='IN PROGRESS') & Q(bloque__in=listsp)).count()
    cstatblockedsp = StatusInfo.objects.filter(Q(status='BLOCKED') & Q(bloque__in=listsp)).count()


    qastepsdef = StepsDefinition.objects.count()
    qaiosruns = IosTestRun.objects.count()
    qaandroidruns = AndroidTestRun.objects.count()
    qatests = TestsQA.objects.count()



    counters={
        "testdef":cdef,
        "teststat":cstat,
        "statpending": cstatpending,
        "statimplemented": cstatimplemented,
        "statcannot": cstatcannot,
        "statinprogress": cstatinprogress,
        "statblocked": cstatblocked,

        "qastepsdef": qastepsdef,
        "qaiosruns" : qaiosruns,
        "qaandroidruns" : qaandroidruns,
        "qatests" : qatests,
        "teststatsp": cstatsp,
        "statpendingsp": cstatpendingsp,
        "statimplementedsp": cstatimplementedsp,
        "statcannotsp": cstatcannotsp,
        "statinprogresssp": cstatinprogresssp,
        "teststathub": cstathub,
        "statpendinghub": cstatpendinghub,
        "statimplementedhub": cstatimplementedhub,
        "statcannothub": cstatcannothub,
        "statinprogresshub": cstatinprogresshub,
        "statblockedsp": cstatblockedsp,
        "statblockedhub": cstatblockedhub,

    }
    json_string = json.dumps(counters)
    return JsonResponse ( counters,  safe=False)




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getcountersqa(request):
    data2=request.data

    lastrunios = IosTestRun.objects.order_by('-id').first()
    lastiosscenok = IosTestResults.objects.filter(runid=lastrunios, estado='passed').count()
    lastiosscenko = IosTestResults.objects.filter(runid=lastrunios, estado='failed').count()
    lastiosscenskip = IosTestResults.objects.filter(runid=lastrunios, estado='skipped').count()
    lastiosstepsok = StepsResultsIos.objects.filter(runid=lastrunios, estado='passed').count()
    lastiosstepsko = StepsResultsIos.objects.filter(runid=lastrunios, estado='failed').count()
    lastiosstepsskip = StepsResultsIos.objects.filter(runid=lastrunios, estado='skipped').count()

    lastrunandroid = AndroidTestRun.objects.order_by('-id').first()
    lastandroidscenok = AndroidTestResults.objects.filter(runid=lastrunandroid, estado='passed').count()
    lastandroidscenko = AndroidTestResults.objects.filter(runid=lastrunandroid, estado='failed').count()
    lastandroidscenskip = AndroidTestResults.objects.filter(runid=lastrunandroid, estado='skipped').count()
    lastandroidstepsok = StepsResults.objects.filter(runid=lastrunandroid, estado='passed').count()
    lastandroidstepsko = StepsResults.objects.filter(runid=lastrunandroid, estado='failed').count()
    lastandroidstepsskip = StepsResults.objects.filter(runid=lastrunandroid, estado='skipped').count()

    counters={
        "lastiosscenok":lastiosscenok,
        "lastiosscenko":lastiosscenko,
        "lastiosscenskip": lastiosscenskip,
        "lastiosstepsok": lastiosstepsok,
        "lastiosstepsko": lastiosstepsko,
        "lastiosstepsskip": lastiosstepsskip,

        "lastandroidscenok": lastandroidscenok,
        "lastandroidscenko" : lastandroidscenko,
        "lastandroidscenskip" : lastandroidscenskip,
        "lastandroidstepsok" : lastandroidstepsok,
        "lastandroidstepsko": lastandroidstepsko,
        "lastandroidstepsskip": lastandroidstepsskip,

    }
    json_string = json.dumps(counters)
    return JsonResponse ( counters,  safe=False)



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.AllowAny])
def teststeps(request):
    data2=request.data
    createsteps()
    return HttpResponse( status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadhtml(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    tipofichero=request.POST.get('filetype')
    data = datosfichero.read()
    total = createstepsfromhtml( data )
    return HttpResponse( total , status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadhtml2(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    tipofichero=request.POST.get('filetype')
    data = datosfichero.read()
    total = createstepsfromhtml2( data )
    return HttpResponse( total , status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadhtmlcert(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    tipofichero=request.POST.get('filetype')
    data = datosfichero.read()
    total = createstepsfromhtmlcert( data )
    return HttpResponse( total , status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadhtml2cert(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    tipofichero=request.POST.get('filetype')
    data = datosfichero.read()
    total = createstepsfromhtml2cert( data )
    return HttpResponse( total , status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadiosruncert(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    country=request.POST.get('country')
    target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    target = country
    data = datosfichero.read()
    createrunfromhtmlcert( data, target  )
    return HttpResponse( status.HTTP_201_CREATED)

@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadiosrun2cert(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    country=request.POST.get('country')
    target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    target = country
    data = datosfichero.read()
    createrunfromhtml2cert( data, target  )
    return HttpResponse( status.HTTP_201_CREATED)

@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadandroidrun2cert(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    country=request.POST.get('country')
    target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    target = country
    data = datosfichero.read()
    createrunfromhtml2certandroid( data, target  )
    return HttpResponse( status.HTTP_201_CREATED)




@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadiosrun(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    target = request.POST.get('target')

  #  target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    data = datosfichero.read()
    country = request.POST.get('country')
    if country is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    createrunfromhtml( data, target, country  )
    return HttpResponse( status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadiosrun2(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    target = request.POST.get('target')
    #target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    data = datosfichero.read()
    country = request.POST.get('country')
    if country is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    createrunfromhtml2( data, target , country )
    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadandroidrun2(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    target = request.POST.get('target')
    #target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    data = datosfichero.read()
    country = request.POST.get('country')
    if country is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    createrunfromhtml2android( data, target , country )
    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def uploadiosrunjson(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('file')
    target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    country=request.POST.get('country')
    if country is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    data = datosfichero.read()

    runid = createrunfromjson( data, target, 'IOS', country  )
    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def uploadandroidrunjson(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('file')
    target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    data = datosfichero.read()
    country=request.POST.get('country')
    if country is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    createandroidrunfromjson( data, target, 'ANDROID' , country )
    return HttpResponse( status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def fixiosruncounters(request):
    data2=request.data

    runname = data2["runid"]
    contscenok = IosTestResults.objects.filter(runid=runname, estado='passed').count()
    contscenko = IosTestResults.objects.filter(runid=runname, estado__in=['failed','error']).count()
    contscenskip = IosTestResults.objects.filter(runid=runname, estado='skipped').count()
    runobj = IosTestRun.objects.filter(id=runname).first()
    contstepok = StepsResultsIos.objects.filter(runid=runname, estado='passed').count()
    contstepko = StepsResultsIos.objects.filter(runid=runname,  estado__in=['failed','error']).count()
    contstepskipped = StepsResultsIos.objects.filter(runid=runname, estado='skipped').count()

    runobj.testsok = contscenok
    runobj.testsko =contscenko
    runobj.testsskipped = contscenskip
    runobj.stepsok = contstepok
    runobj.stepsko = contstepko
    runobj.stepsskipped = contstepskipped
    runobj.save()

    return HttpResponse( status.HTTP_201_CREATED)

def fixcountersios2(runname):
    contscenok = IosTestResults.objects.filter(runid=runname, estado='passed').count()
    contscenko = IosTestResults.objects.filter(runid=runname,  estado__in=['failed','error']).count()
    contscenskip = IosTestResults.objects.filter(runid=runname, estado='skipped').count()
    runobj = IosTestRun.objects.filter(id=runname).first()
    contstepok = StepsResultsIos.objects.filter(runid=runname, estado='passed').count()
    contstepko = StepsResultsIos.objects.filter(runid=runname, estado__in=['failed','error']).count()
    contstepskipped = StepsResultsIos.objects.filter(runid=runname, estado='skipped').count()

    runobj.testsok = contscenok
    runobj.testsko =contscenko
    runobj.testsskipped = contscenskip
    runobj.stepsok = contstepok
    runobj.stepsko = contstepko
    runobj.stepsskipped = contstepskipped
    runobj.save()

def fixcountersandroid2(runname):
    contscenok = AndroidTestResults.objects.filter(runid=runname, estado='passed').count()
    contscenko = AndroidTestResults.objects.filter(runid=runname,  estado__in=['failed','error']).count()
    contscenskip = AndroidTestResults.objects.filter(runid=runname, estado='skipped').count()
    runobj = AndroidTestRun.objects.filter(id=runname).first()
    contstepok = StepsResults.objects.filter(runid=runname, estado='passed').count()
    contstepko = StepsResults.objects.filter(runid=runname,  estado__in=['failed','error']).count()
    contstepskipped = StepsResults.objects.filter(runid=runname, estado='skipped').count()

    runobj.testsok = contscenok
    runobj.testsko =contscenko
    runobj.testsskipped = contscenskip
    runobj.stepsok = contstepok
    runobj.stepsko = contstepko
    runobj.stepsskipped = contstepskipped
    runobj.save()


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def fixandroidruncounters(request):
    data2=request.data

    runname = data2["runid"]
    contscenok = AndroidTestResults.objects.filter(runid=runname, estado='passed').count()
    contscenko = AndroidTestResults.objects.filter(runid=runname,  estado__in=['failed','error']).count()
    contscenskip = AndroidTestResults.objects.filter(runid=runname, estado='skipped').count()
    runobj = AndroidTestRun.objects.filter(id=runname).first()
    contstepok = StepsResults.objects.filter(runid=runname, estado='passed').count()
    contstepko = StepsResults.objects.filter(runid=runname,  estado__in=['failed','error']).count()
    contstepskipped = StepsResults.objects.filter(runid=runname, estado='skipped').count()

    runobj.testsok = contscenok
    runobj.testsko =contscenko
    runobj.testsskipped = contscenskip
    runobj.stepsok = contstepok
    runobj.stepsko = contstepko
    runobj.stepsskipped = contstepskipped
    runobj.save()

    return HttpResponse( status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.AllowAny])
def fixskippedcounters(request):
    data2=request.data
    runname = data2["runid"]
    allruns = IosTestRun.objects.filter().all()
    for a in allruns:
        badresults= IosTestResults.objects.filter(runid=a.id, estado='passed',stepsok=0).all()
        for res in badresults:
            res.estado='skipped'
            res.save()
        runname = a.id
        contscenok = IosTestResults.objects.filter(runid=runname, estado='passed').count()
        contscenko = IosTestResults.objects.filter(runid=runname, estado__in=['failed', 'error']).count()
        contscenskip = IosTestResults.objects.filter(runid=runname, estado='skipped').count()
        runobj = IosTestRun.objects.filter(id=runname).first()
        contstepok = StepsResultsIos.objects.filter(runid=runname, estado='passed').count()
        contstepko = StepsResultsIos.objects.filter(runid=runname, estado__in=['failed', 'error']).count()
        contstepskipped = StepsResultsIos.objects.filter(runid=runname, estado='skipped').count()

        runobj.testsok = contscenok
        runobj.testsko = contscenko
        runobj.testsskipped = contscenskip
        runobj.stepsok = contstepok
        runobj.stepsko = contstepko
        runobj.stepsskipped = contstepskipped
        runobj.save()



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def gettestsinrun(request):
    data2=request.data
    createsteps()
    return HttpResponse( status.HTTP_201_CREATED)


def findrelations():
    statusinfos = StatusInfo.objects.all()
    for stinfo in statusinfos:
        if stinfo.scenario is not None and stinfo.scenario != 'nan':
            print('Trying ' + stinfo.test +'  with  ' + stinfo.scenario)
            curscen = stinfo.scenario
            if "-" in curscen:
                spstr = curscen.split("-")
                curscen = spstr[1].strip()
            curscen = curscen.replace(" 4 digits", "")
            tsqa = TestsQA.objects.filter(test__contains=curscen).first()
            if tsqa is not None and stinfo.scenario != 'nan':
                stinfo.scenarioid = tsqa
                stinfo.save()
                tsqa.iscritical = True
                tsqa.save()
                print( 'Connecting ' + stinfo.test + ' with ' +tsqa.test)
            else:
                print('========> No encontrado')



def findrelationspl():
    statusinfos = StatusInfoPL.objects.all()
    for stinfo in statusinfos:
        if stinfo.scenario is not None and stinfo.scenario != 'nan':
            print('Trying ' + stinfo.test +'  with  ' + stinfo.scenario)
            curscen = stinfo.scenario
            if "-" in curscen:
                spstr = curscen.split("-")
                curscen = spstr[1].strip()
            curscen = curscen.replace(" 4 digits", "")
            tsqa = TestsQA.objects.filter(test__contains=curscen).first()
            if tsqa is not None and stinfo.scenario != 'nan':
                stinfo.scenarioid = tsqa
                stinfo.save()
                tsqa.iscritical = True
                tsqa.save()
                print( 'Connecting ' + stinfo.test + ' with ' +tsqa.test)
            else:
                print('========> No encontrado')


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.AllowAny])
def connectscenarios(request):
    data2=request.data
    findrelations()
    return HttpResponse( status.HTTP_200_OK)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadstatuspl(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('excelfile')
    target = "CORE"
    if "SPAIN" in datosfichero.name.upper():
        target="SPAIN"
    data = datosfichero.read()
    parseExcelTestStatusPL( data, target  )
    return HttpResponse( status.HTTP_201_CREATED)



def createpage():
    confluence = Confluence(url="https://sanone.atlassian.net",
                            username="x851335@gruposantander.es",
                            password="UJGw8KrPUDvStXZVDBok0F61")
    body="<p>Pagina automatica</p>"
    parent=41041461450
    status = confluence.create_page(space="MOVONE", title="Test results 3", body=body, parent_id=parent)
    print (status)


def uploadimage( ):
    confluence = Confluence(url="https://sanone.atlassian.net",
                            username="x851335@gruposantander.es",
                            password="UJGw8KrPUDvStXZVDBok0F61")
    filename = 'graphimages/my_plot.png'
    confl = ConfluenceInfo.objects.filter(country='TESTIMG1').first()
    parentpageid=confl.confpageid
    confluence.attach_file('graphimages/my_plot.png', page_id=parentpageid)

    image = '<tabla><th>Aaaaa</th><tr><td><ac:image> <ri:attachment ri:filename=\"my_plot.png\" /> </ac:image></td></tr></table>'
    bodyc = "<p>Pagina automatica<span>zzzz</span><ac:image> <ri:attachment ri:filename=\"my_plot.png\" /> </ac:image></p>"
    status = confluence.update_page( type='page', title='TGraph1', body=bodyc, page_id=parentpageid , always_update=True)
    print(status)
 #   confluence.append_page('Test G1', 'Image 1', image, space="MOVONE",parent_id=parentpageid,
 #                          type='page', representation='storage', minor_edit=False)


def updatepage(title,  pageid, htmlcontent):
    confluence = Confluence(url="https://sanone.atlassian.net",
                            username="x851335@gruposantander.es",
                            password="UJGw8KrPUDvStXZVDBok0F61")
    body="<p>Pagina automatica</p>"
    parent=41041461450
    htmlcontent = htmlcontent.replace("&","_")
    status = confluence.update_page( type='page', title=title, body=htmlcontent, page_id=pageid, always_update=True)
    print (status)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createtestpage(request):
    #docname = data2["docname"]
    createpage()
    return HttpResponse( status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createimagepage(request):
    #docname = data2["docname"]
    uploadimage()
    return HttpResponse( status.HTTP_201_CREATED)


def uploadimagerun( pageid , imagename):
    confluence = Confluence(url="https://sanone.atlassian.net",
                            username="x851335@gruposantander.es",
                            password="UJGw8KrPUDvStXZVDBok0F61")
    filename = 'graphimages/my_plot.png'
    confl = ConfluenceInfo.objects.filter(country='TESTIMG1').first()
    parentpageid=confl.confpageid
    confluence.attach_file('graphimages/'+imagename+'.png', page_id=pageid)

  #  image = '<tabla><th>Aaaaa</th><tr><td><ac:image> <ri:attachment ri:filename=\"my_plot.png\" /> </ac:image></td></tr></table>'
  #  bodyc = "<p>Pagina automatica<span>zzzz</span><ac:image> <ri:attachment ri:filename=\"my_plot.png\" /> </ac:image></p>"
  #  status = confluence.update_page( type='page', title='TGraph1', body=bodyc, page_id=parentpageid , always_update=True)
  #  print(status)


def uploadimagebar( pageid , imagename ):
    confluence = Confluence(url="https://sanone.atlassian.net",
                            username="x851335@gruposantander.es",
                            password="UJGw8KrPUDvStXZVDBok0F61")
 #   filename = 'graphimages/my_plot.png'
  #  confl = ConfluenceInfo.objects.filter(country=iosand).first()
  #  parentpageid=confl.confpageid
    confluence.attach_file('graphimages/bars/'+imagename+'.png', page_id=pageid)


def uploadexceltoconfl( pageid , filename ):
    confluence = Confluence(url="https://sanone.atlassian.net",
                            username="x851335@gruposantander.es",
                            password="UJGw8KrPUDvStXZVDBok0F61")
 #   filename = 'graphimages/my_plot.png'
  #  confl = ConfluenceInfo.objects.filter(country=iosand).first()
  #  parentpageid=confl.confpageid
    confluence.attach_file('graphimages/excel/'+filename+'.xlsx', page_id=pageid)
    a=1

@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadvideostoconfl(request):
    confluence = Confluence(url="https://sanone.atlassian.net",
                            username="x851335@gruposantander.es",
                            password="UJGw8KrPUDvStXZVDBok0F61")
    #   filename = 'graphimages/my_plot.png'
    #  confl = ConfluenceInfo.objects.filter(country=iosand).first()
    #  parentpageid=confl.confpageid
    confluence.attach_file('graphimages/SendMoneyInterAndroidErrorx2.mp4', page_id=41042968635)
    a = 1
    return HttpResponse( status.HTTP_201_CREATED)


def creahtml(country, data, iosand):
    table=''
    if iosand=='IOS':
        table += "<p><h3>SPAIN</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_SP_ALL.png\" /> </ac:image></p>"
        table +=  "<p><h3>UK</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_UK_ALL.png\" /> </ac:image></p>"
        table += "<p><h3>PORTUGAL</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_PT_ALL.png\" /> </ac:image></p>"
        table += "<p><h3>POLAND</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_PL_ALL.png\" /> </ac:image></p>"
    else:
        table += "<p>SPAIN<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_SP_ALL.png\" /> </ac:image></p>"
        table += "<p>UK<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_UK_ALL.png\" /> </ac:image></p>"
        table += "<p>PORTUGAL<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_PT_ALL.png\" /> </ac:image></p>"
        table += "<p>POLAND<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_PL_ALL.png\" /> </ac:image></p>"

    #table+='  <ac:structured-macro ac:name=\"space-attachments\"><ac:parameter ac:name=\"showFilter\">false</ac:parameter></ac:structured-macro>'
    table += "<br/><br/><table>\n"

    header = data[0]
    table += "  <tr>\n"
    cont = 1
    for i in range(len(header)):
        val = header['H'+str(i +1)]
        table += "    <th><strong>{0}</strong></th>\n".format(val)


    table += "  </tr>\n"

    for line in data[1:]:
        row = line
        table += "  <tr>\n"
        for i in range(len(row)):
            val = row['H' + str(i + 1)]
            table += "    <td ><h6>{0}</h6></td>\n".format(val)

        table+='<td><ac:image> <ri:attachment ri:filename=\"'+row["H1"]+'.png\" /> </ac:image></td>'

        table+='<td><ac:link><ri:attachment ri:filename=\"'+row["H1"]+'.xlsx\" /><ac:plain-text-link-body><![CDATA[Results ]]></ac:plain-text-link-body></ac:link> <br/>' \
                                                                      '<ac:link><ri:attachment ri:filename=\"IDERROR_'+row["H1"]+'.xlsx\" /><ac:plain-text-link-body><![CDATA[Errors ]]></ac:plain-text-link-body></ac:link></td>'

        table += "  </tr>\n"

    table += "</table>"
    table += "<br/><br/>"

    return table


def creahtmlmaster(country, data, iosand):
    table=''
    if iosand=='IOS':
        table += "<p><h3>SPAIN</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_SP_MASTER.png\" /> </ac:image></p>"
        table +=  "<p><h3>UK</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_UK_MASTER.png\" /> </ac:image></p>"
        table += "<p><h3>PORTUGAL</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_PT_MASTER.png\" /> </ac:image></p>"
        table += "<p><h3>POLAND</h3><span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_IOS_PL_MASTER.png\" /> </ac:image></p>"
    else:
        table += "<p>SPAIN<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_SP_MASTER.png\" /> </ac:image></p>"
        table += "<p>UK<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_UK_MASTER.png\" /> </ac:image></p>"
        table += "<p>PORTUGAL<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_PT_MASTER.png\" /> </ac:image></p>"
        table += "<p>POLAND<span></span><br/><ac:image> <ri:attachment ri:filename=\"BARCHART_ANDROID_PL_MASTER.png\" /> </ac:image></p>"

    #table+='  <ac:structured-macro ac:name=\"space-attachments\"><ac:parameter ac:name=\"showFilter\">false</ac:parameter></ac:structured-macro>'
    table += "<br/><br/><table>\n"

    header = data[0]
    table += "  <tr>\n"
    cont = 1
    for i in range(len(header)):
        val = header['H'+str(i +1)]
        table += "    <th><strong>{0}</strong></th>\n".format(val)


    table += "  </tr>\n"

    for line in data[1:]:
        row = line
        table += "  <tr>\n"
        for i in range(len(row)):
            val = row['H' + str(i + 1)]
            table += "    <td ><h6>{0}</h6></td>\n".format(val)

        table+='<td><ac:image> <ri:attachment ri:filename=\"'+row["H1"]+'.png\" /> </ac:image></td>'

        table+='<td><ac:link><ri:attachment ri:filename=\"'+row["H1"]+'.xlsx\" /><ac:plain-text-link-body><![CDATA[Results ]]></ac:plain-text-link-body></ac:link> <br/>' \
                                                                      '<ac:link><ri:attachment ri:filename=\"IDERROR_'+row["H1"]+'.xlsx\" /><ac:plain-text-link-body><![CDATA[ Errors ]]></ac:plain-text-link-body></ac:link></td>'
        table += "  </tr>\n"

    table += "</table>"
    table += "<br/><br/>"

    return table


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def updatecountrypage(request):
    data2=request.data
    country = data2["countrycode"]
    datatest = []
    tests = TestsQA.objects.all()
    datatest.append( {'H2':'Test', 'H1':'Feature', 'H3':'Background', 'H4':'Basico'})
    for t in tests:
        iscrit = "NO"
        if t.iscritical:
            iscrit = "YES"
        datatest.append({'H2':t.test, 'H1':t.feature, 'H3':t.background, 'H4':iscrit } )

    body = creahtml(country, datatest,'IOS')
    confl = ConfluenceInfo.objects.filter(countrycode=country).first()
    pageid=confl.confpageid

    updatepage('Tests Plan 3', pageid, body )
    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def updatecountryrunpage(request):
    data2=request.data
    country = data2["countrycode"]
    platform = data2["platform"]
    datatest = []
    if platform == 'iOS':
        IosTestRun.objects.filter()
    tests = TestsQA.objects.all()
    datatest.append( {'H2':'Test', 'H1':'Feature', 'H3':'Background', 'H4':'Basico'})
    for t in tests:
        iscrit = "NO"
        if t.iscritical:
            iscrit = "YES"
        datatest.append({'H2':t.test, 'H1':t.feature, 'H3':t.background, 'H4':iscrit } )

    body = creahtml(country, datatest,'IOS')
    confl = ConfluenceInfo.objects.filter(countrycode=country).first()
    pageid=confl.confpageid

    updatepage('Tests Plan 3', pageid, body )
    return HttpResponse( status.HTTP_201_CREATED)


def getdataforbars(country, platf, envi):
    if platf =='IOS':
        nruns=IosTestRun.objects.filter(country=country).all()
    else:
        nruns=AndroidTestRun.objects.filter(country=country).all()

    alldata =[]
    allnames=[]
    d1 = []
    d2=[]
    d3=[]
    d4=[]
    for r in nruns:
        if envi!= 'ALL' and 'DEVELOP' in r.testname:
            continue
        if '_LOANS_' in r.testname:
            continue
        if '_ACCOUNTS_' in r.testname:
            continue
        total = r.testsok+r.testsko+r.testsskipped
        d1.append(total)
        d2.append(r.testsok)
        d3.append(r.testsko)
        d4.append(r.testsskipped)
        allnames.append(r.testname)
      #  d1=[total, r.testsok, r.testsko, r.testsskipped]
    alldata.append(d1)
    alldata.append(d2)
    alldata.append(d3)
    alldata.append(d4)
    return alldata, allnames



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createpie1(request):
    #docname = data2["docname"]
    data1 = [100,50,24,98,44]
    pltcreatepie( data1)
    return HttpResponse( status.HTTP_201_CREATED)

@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createbarsmaster(request):
    #docname = data2["docname"]
    confios = ConfluenceInfo.objects.filter(country='IOSRESUMEMASTER').first()
    confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUMEMASTER').first()
    alldata,allnames = getdataforbars('SP','IOS','MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_SP_MASTER')
    uploadimagebar( confios.confpageid,'BARCHART_IOS_SP_MASTER' )
    alldata,allnames = getdataforbars('SP','ANDROID','MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_SP_MASTER')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_SP_MASTER' )

    alldata,allnames = getdataforbars('PT','IOS','MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_PT_MASTER')
    uploadimagebar( confios.confpageid,'BARCHART_IOS_PT_MASTER' )

    alldata,allnames = getdataforbars('PT','ANDROID','MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_PT_MASTER')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_PT_MASTER' )

    alldata,allnames = getdataforbars('UK','IOS','MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_UK_MASTER')
    uploadimagebar( confios.confpageid,'BARCHART_IOS_UK_MASTER' )

    alldata,allnames = getdataforbars('UK','ANDROID','MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_UK_MASTER')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_UK_MASTER' )

    alldata, allnames = getdataforbars('PL', 'IOS', 'MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_PL_MASTER')
    uploadimagebar(confios.confpageid, 'BARCHART_IOS_PL_MASTER')

    alldata,allnames = getdataforbars('PL','ANDROID','MASTER')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_PL_MASTER')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_PL_MASTER' )

    return HttpResponse( status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createbars(request):
    #docname = data2["docname"]
    confios = ConfluenceInfo.objects.filter(country='IOSRESUME').first()
    confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUME').first()
    alldata,allnames = getdataforbars('SP','IOS','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_SP_ALL')
    uploadimagebar( confios.confpageid,'BARCHART_IOS_SP_ALL' )
    alldata,allnames = getdataforbars('SP','ANDROID','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_SP_ALL')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_SP_ALL' )

    alldata,allnames = getdataforbars('PT','IOS','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_PT_ALL')
    uploadimagebar( confios.confpageid,'BARCHART_IOS_PT_ALL' )

    alldata,allnames = getdataforbars('PT','ANDROID','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_PT_ALL')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_PT_ALL' )

    alldata,allnames = getdataforbars('UK','IOS','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_UK_ALL')
    uploadimagebar( confios.confpageid,'BARCHART_IOS_UK_ALL' )

    alldata,allnames = getdataforbars('UK','ANDROID','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_UK_ALL')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_UK_ALL' )

    alldata,allnames = getdataforbars('PL','ANDROID','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_ANDROID_PL_ALL')
    uploadimagebar( confand.confpageid,'BARCHART_ANDROID_PL_ALL' )
    alldata,allnames = getdataforbars('PL','IOS','ALL')
    pltcreatebarruns(alldata, allnames, 'BARCHART_IOS_PL_ALL')
    uploadimagebar( confand.confpageid,'BARCHART_IOS_PL_ALL' )
    return HttpResponse( status.HTTP_201_CREATED)

@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def refreshjirabugs(request):
    #docname = data2["docname"]
    listjiraissues2()
    return HttpResponse( status.HTTP_201_CREATED)

@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def jiracertreleasereport(request):
    #docname = data2["docname"]
    releasename= request.GET.get("release")
    if releasename is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    makejiracertreport( releasename=releasename)
    return HttpResponse( status.HTTP_201_CREATED)




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def listzeplinprojects(request):
    url = "https://api.zeplin.dev/v1/projects?limit=100"

    payload = {}
    headers = {
        'Authorization': 'Bearer ' + zeplintoken
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    print(response.text)
    projects = json.loads(response.text)

    for prj in projects:
        project = ProjectElement(prj)
        x = project.status
        print( project.name)
        zeplinproj = ZeplinProject.objects.filter(id=project.id).first()
        if zeplinproj is None:
            try:
                zeplinproj = ZeplinProject(id=project.id, name=project.name, status=project.status, platform=project.platform , description='')
                zeplinproj.save()
            except Exception as e:
                print( e)
        else:
            zeplinproj.platform =project.platform
            zeplinproj.numberofscreens=project.number_of_screens
            zeplinproj.numberofcomponents =project.number_of_components
            zeplinproj.save()

    return HttpResponse( status.HTTP_201_CREATED)




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getscreens(request):
    prjid= request.GET.get("project")
    url = "https://api.zeplin.dev/v1/projects/"+prjid +"/screens?limit=100"

    payload = {}
    headers = {
        'Authorization': 'Bearer ' + zeplintoken
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    screensdata = json.loads(response.text)
    lastquerylength = len(screensdata)
    offset=100
    while lastquerylength == 100:    #paginacion
        url2 = "https://api.zeplin.dev/v1/projects/" + prjid + "/screens?offset="+str(offset)+"&limit=100"
        response2 = requests.request("GET", url2, headers=headers, data=payload)
        screensdata2 = json.loads(response2.text)
        screensdata = screensdata + screensdata2
        lastquerylength = len(screensdata2)
        offset = len(screensdata)

    project = ZeplinProject.objects.filter(id=prjid).first()
    if project is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    for zepscr in screensdata:
        screen = Screen(zepscr)
        print(screen.name)
        zeplinscreen = ZeplinScreen.objects.filter(id=project.id).first()
        if zeplinscreen is None:
            zeplinscreen = ZeplinScreen(id=screen.id, project=project,section=screen.section["id"],
                                        name=screen.name, numberofversions=screen.number_of_versions,
                                        numberofnotes=screen.number_of_notes)
            zeplinscreen.save()
        else:
            zeplinscreen.numberofversions=screen.number_of_versions
            zeplinscreen.save()

    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getscreenversions(request):
    prjid= request.GET.get("project")
    screenid = request.GET.get("screen")

    url = "https://api.zeplin.dev/v1/projects/"+prjid+"/screens/"+ screenid +"/versions"

    payload = {}
    headers = {
        'Authorization': 'Bearer ' + zeplintoken
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    versions = json.loads(response.text)
    project = ZeplinProject.objects.filter(id=prjid).first()
    if project is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    screen = ZeplinScreen.objects.filter(id=screenid).first()
    if screen is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    for zepscr in versions:
        scrversion = ScreenVersion(zepscr)
        print(screen.name + '-- ' +scrversion.id)
        zeplinscreenver = ZeplinScreenVersion.objects.filter(id=scrversion.id).first()
        if zeplinscreenver is None:
            zeplinscreenver = ZeplinScreenVersion( id= scrversion.id, imageurl=scrversion.image_url, screen=screen)
            zeplinscreenver.save()
    return HttpResponse( status.HTTP_201_CREATED)




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getallscreenversions(request):
    prjid= request.GET.get("project")
    project = ZeplinProject.objects.filter(id=prjid).first()
    if project is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)


    screens = ZeplinScreen.objects.filter(project=project).all()
    if screens is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    for scr1 in screens:
        url = "https://api.zeplin.dev/v1/projects/"+prjid+"/screens/"+ scr1.id +"/versions"

        payload = {}
        headers = {
            'Authorization': 'Bearer ' + zeplintoken
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        versions = json.loads(response.text)

        for zepscr in versions:
            try:
                scrversion = ScreenVersion(zepscr)
                print(scr1.name + '-- ' + scrversion.id)

                zeplinscreenver = ZeplinScreenVersion.objects.filter(id=scrversion.id).first()
                if zeplinscreenver is None:
                    zeplinscreenver = ZeplinScreenVersion( id= scrversion.id, imageurl=scrversion.image_url, screen=scr1)
                    zeplinscreenver.save()
            except Exception as e:
                print(e)
    return HttpResponse( status.HTTP_201_CREATED)




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getscreencomponents(request):
    prjid= request.GET.get("project")
    screenid = request.GET.get("screen")
    versionid = request.GET.get("version")
    url = "https://api.zeplin.dev/v1/projects/"+prjid+"/screens/"+screenid+"/versions/" + versionid+"?limit=100"

    payload = {}
    headers = {
        'Authorization': 'Bearer ' + zeplintoken
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    if response.status_code!=200:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    versioncomps = json.loads(response.text)
    screen = ZeplinScreen.objects.filter(id=screenid).first()
    if screen is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    scrversion = ZeplinScreenVersion.objects.filter(id=versionid).first()
    if scrversion is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    all = ScreenComponents(versioncomps)
    x = all.id

    print( all.id)
    for onelay in all.layers:
        scrcomp = ZeplinScreenComponent.objects.filter(id=onelay["id"]).first()
        if scrcomp is None:
            scrcomp = ZeplinScreenComponent( id= onelay["id"], name=onelay["name"], width=onelay["rect"]["width"],
                                             height=onelay["rect"]["height"], absx=onelay["rect"]["absolute"]["x"],
                                             absy=onelay["rect"]["absolute"]["y"], screen=screen, screenversion=scrversion)
            scrcomp.save()

        print( onelay["id"] + '  ' + onelay["name"]  +'  '+  str(onelay["rect"]["absolute"]["x"]) )
    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getallscreencomponents(request):
    prjid= request.GET.get("project")

    project = ZeplinProject.objects.filter(id=prjid).first()
    if project is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)


    screens = ZeplinScreen.objects.filter(project=project).all()
    if screens is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    for scr1 in screens:
        screenversions = ZeplinScreenVersion.objects.filter(screen=scr1).order_by("id").last()
        #for scrvr in screenversions:
         #   print(scrvr.id)
        url = "https://api.zeplin.dev/v1/projects/"+prjid+"/screens/"+scr1.id+"/versions/" + screenversions.id+"?limit=100"

        payload = {}
        headers = {
            'Authorization': 'Bearer ' + zeplintoken
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code!=200:
            return HttpResponse(status.HTTP_400_BAD_REQUEST)

        versioncomps = json.loads(response.text)
        lastquerylength = len(versioncomps)
        while lastquerylength == 100:  # paginacion
            url2 = "https://api.zeplin.dev/v1/projects/"+prjid+"/screens/"+scr1.id+"/versions/" + screenversions.id+"?offset=100&limit=100"
            response2 = requests.request("GET", url2, headers=headers, data=payload)
            versioncomps2 = json.loads(response2.text)
            versioncomps = versioncomps + versioncomps2
            lastquerylength = len(versioncomps2)

        all = ScreenComponents(versioncomps)
        x = all.id

        print( all.id)
        for onelay in all.layers:
            scrcomp = ZeplinScreenComponent.objects.filter(id=onelay["id"]).first()
            if scrcomp is None:
                try:
                    scrcomp = ZeplinScreenComponent( id= onelay["id"], name=onelay["name"], width=onelay["rect"]["width"],
                                                 height=onelay["rect"]["height"], absx=onelay["rect"]["absolute"]["x"],
                                                 absy=onelay["rect"]["absolute"]["y"], screen=scr1, screenversion=screenversions)
                    scrcomp.save()
                except Exception as e:
                    print(e)
            else:
                if scrcomp.elemtype == '':
                    scrcomp.elemtype = onelay["type"]
                    scrcomp.save()
            print('SCREEN: '+ scr1.name+ '  '+ onelay["id"] + '  ' + onelay["name"]  +'  '+  str(onelay["rect"]["absolute"]["x"]) )
    return HttpResponse( status.HTTP_201_CREATED)





@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createlogtests(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('htmlfile')
    target = request.POST.get('target')
    #target = "CORE"

    data = datosfichero.read()
    country = request.POST.get('country')
    if country is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    createnewlogsfromhtml( data, target , country )
    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createlogtestsjson(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('file')
    target = request.POST.get('target')
    #target = "CORE"

    data = datosfichero.read()
    country = request.POST.get('country')
    if country is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    createnewlogsfromjson( data, target , country )
    return HttpResponse( status.HTTP_201_CREATED)


