import json
import logging
import sys
import uuid
import zipfile
from collections import namedtuple
import base64
import io
from datetime import datetime
from doctest import TestResults
from uuid import UUID

import django_filters
import requests
from django.core.files.base import ContentFile
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseServerError, HttpResponse, JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from requests import Response
from rest_framework import viewsets, status, decorators, response, permissions
from django.contrib.auth.models import User

from gestmetricas.models import TestCertificacion, TestCertificacionRunVersion, IosTestResultsCert, StepsResultsIosCert, \
    StepsResultsAndroidCert, AndroidTestResultsCert, TestsQA, LogTestDefinition, JiraCertificationReport
from gestmetricas.utils.ParseExcelTestCertificacionDef import parseExcelTestCertificacionDef, \
    parseExcelTestCertificacionEjecucion
from xlsxwriter import Workbook


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadexcelcert(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('excelfile')
    tipofichero=request.POST.get('filetype')
    data = datosfichero.read()
    if tipofichero == 'certdef':
        dataxls = parseExcelTestCertificacionDef(data, "CORE")
    elif tipofichero == 'certdefSP':
        dataxls = parseExcelTestCertificacionDef(data, "LOCAL", "SP")
      #  dataxls['url'] = evt.xlsparticipantes.url

    return HttpResponse(  status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def uploadexcelcertrun(request):
    #docname = data2["docname"]
    datosfichero=request.FILES.get('excelfile')
    tipofichero=request.POST.get('filetype')
    relversion=request.POST.get('version')
    country=request.POST.get('country')

    data = datosfichero.read()
    if tipofichero == 'certrun':
        dataxls = parseExcelTestCertificacionEjecucion(data, "CORE-" + country, relversion, countrysel=country)
    elif tipofichero == 'certdefSP':
        dataxls = parseExcelTestCertificacionDef(data, "LOCAL", "SP")
      #  dataxls['url'] = evt.xlsparticipantes.url

    return HttpResponse(  status.HTTP_201_CREATED)


@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def addmodulestocert(request):
    #docname = data2["docname"]
    release=request.POST.get('release')
    modules=request.POST.get('modules')
    countries=request.POST.get('countries')
    iscore=request.POST.get('iscore')
    allmods = modules.split(',')
    forsp = 'NO'
    forpt= 'NO'
    forpl = 'NO'
    foruk ='NO'
    if countries is not None:
        allcountries = countries.split(',')
        if 'SP' in allcountries:
            forsp='YES'
        if 'PT' in allcountries:
            forpt='YES'
        if 'PL' in allcountries:
            forpl='YES'
        if 'UK' in allcountries:
            foruk = 'YES'
    for mod in allmods:
        tests=TestsQA.objects.filter(feature__iexact=mod).all()
        for test in tests:
            testcert = TestCertificacion.objects.filter(releasename=release, testid=test.id).first()
            if testcert is None:
                testcert = TestCertificacion(testid=test.id, releasename=release, test=test.test, bloque=mod,
                                             issp=forsp, ispt=forpt, ispl=forpl, isuk=foruk,iscore=iscore)
                testcert.save()
            else:
                testcert.issp=forsp
                testcert.ispt=forpt
                testcert.ispl=forpl
                testcert.isuk=foruk
                testcert.iscore=iscore
                testcert.save()

            logtests = LogTestDefinition.objects.filter(testid=test).all()
            for logtest in logtests:
                tescertlog =TestCertificacion.objects.filter(releasename=release, logtest=logtest).first()
                if tescertlog is None:
                    tescertlog = TestCertificacion(testid=20000+logtest.id, releasename=release, test=test.test, bloque=mod,
                                                 issp=forsp, ispt=forpt, ispl=forpl, isuk=foruk, iscore=iscore, logtest=logtest)
                    tescertlog.save()
                else:
                    tescertlog.issp = forsp
                    tescertlog.ispt = forpt
                    tescertlog.ispl = forpl
                    tescertlog.isuk = foruk
                    tescertlog.iscore = iscore
                    tescertlog.save()


    return HttpResponse(  status.HTTP_201_CREATED)


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelcert(request):
    tipo= request.GET.get("tipo")
    pais= request.GET.get("pais")
    release= request.GET.get("release")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet(tipo+ '-'+ pais)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', 'Certification Release: ' + release, merge_format)
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 15)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'BLOCK', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'AUTO IOS', cell_format)
    worksheet.write(2, 4, 'AUTO ANDROID', cell_format)
    worksheet.write(2, 5, 'MANUAL', cell_format)
    worksheet.write(2, 6, 'COMMENTS', cell_format)
    worksheet.write(2, 7, 'CORE', cell_format)
    worksheet.write(2, 8, 'COUNTRY', cell_format)
    worksheet.write(2, 9, 'TESTER', cell_format)
    worksheet.write(2, 10, 'DEVICE IOS', cell_format_ios)
    worksheet.write(2, 11, 'APP VERSION IOS', cell_format_ios)
    worksheet.write(2, 12, 'ENV IOS', cell_format_ios)
    worksheet.write(2, 13, 'USER IOS', cell_format_ios)
    worksheet.write(2, 14, ' MANUAL REPORT IOS', cell_format_ios)
    worksheet.write(2, 15, 'AUTO REPORT IOS', cell_format_ios)
    worksheet.write(2, 16, 'BUG JIRA IOS', cell_format_ios)
    worksheet.write(2, 17, 'PRIORITY IOS', cell_format_ios)
    worksheet.write(2, 18, 'DEVICE ANDROID', cell_format_android)
    worksheet.write(2, 19, 'APP VERSION ANDROID', cell_format_android)
    worksheet.write(2, 20, 'ENV ANDROID', cell_format_android)
    worksheet.write(2, 21, 'USER ANDROID', cell_format_android)
    worksheet.write(2, 22, 'MANUAL REPORT ANDROID', cell_format_android)
    worksheet.write(2, 23, 'AUTO REPORT ANDROID', cell_format_android)
    worksheet.write(2, 24, 'BUG JIRA ANDROID', cell_format_android)
    worksheet.write(2, 25, 'PRIORITY ANDROID', cell_format_android)

    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})
    testrows = TestCertificacion.objects.filter(iscore='YES', issp='YES')

    if tipo == 'CORE' and pais=='SP':
        testrows = TestCertificacion.objects.filter(iscore='YES', issp='YES', releasename=release)
    elif tipo == 'CORE' and pais=='PT':
        testrows = TestCertificacion.objects.filter(iscore='YES', ispt='YES', releasename=release)
    elif tipo == 'CORE' and pais=='PL':
        testrows = TestCertificacion.objects.filter(iscore='YES', ispl='YES', releasename=release)
    elif tipo == 'CORE' and pais=='UK':
        testrows = TestCertificacion.objects.filter(iscore='YES', isuk='YES', releasename=release)

    for testline in testrows:
        worksheet.write(row, 0, testline.id, )
        worksheet.write(row, 1, testline.bloque,cell_format_data)
        worksheet.write(row, 2, testline.test,cell_format_data)
        worksheet.write(row, 3, testline.autoios)
        worksheet.write(row, 4, testline.autoandroid)
        worksheet.write(row, 5, testline.manual)
        worksheet.write(row, 6, testline.comentario,cell_format_data)
        worksheet.write(row, 7, testline.iscore)
        worksheet.write(row, 8, pais)
        worksheet.write(row, 9, "")
        worksheet.write(row, 10, "",cell_format_data_ios)
        worksheet.write(row, 11, "",cell_format_data_ios)
        worksheet.write(row, 12, "",cell_format_data_ios)
        worksheet.write(row, 13, "",cell_format_data_ios)
        worksheet.write(row, 14, "",cell_format_data_ios)
        worksheet.write(row, 15, "",cell_format_data_ios)
        worksheet.write(row, 16, "",cell_format_data_ios)
        worksheet.write(row, 17, "",cell_format_data_ios)
        worksheet.write(row, 18, "",cell_format_data_android)
        worksheet.write(row, 19, "",cell_format_data_android)
        worksheet.write(row, 20, "",cell_format_data_android)
        worksheet.write(row, 21, "",cell_format_data_android)
        worksheet.write(row, 22, "",cell_format_data_android)
        worksheet.write(row, 23, "",cell_format_data_android)
        worksheet.write(row, 24, "",cell_format_data_android)
        worksheet.write(row, 25, "",cell_format_data_android)


        row += 1

    book.close()

    return response



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelcertwithlogs(request):
    tipo= request.GET.get("tipo")
    pais= request.GET.get("pais")
    modules=request.GET.get("modules")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet(tipo+ '-'+ pais)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 15)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'BLOCK', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'AUTO IOS', cell_format)
    worksheet.write(2, 4, 'AUTO ANDROID', cell_format)
    worksheet.write(2, 5, 'MANUAL', cell_format)
    worksheet.write(2, 6, 'COMMENTS', cell_format)
    worksheet.write(2, 7, 'CORE', cell_format)
    worksheet.write(2, 8, 'COUNTRY', cell_format)
    worksheet.write(2, 9, 'TESTER', cell_format)
    worksheet.write(2, 10, 'DEVICE IOS', cell_format_ios)
    worksheet.write(2, 11, 'APP VERSION IOS', cell_format_ios)
    worksheet.write(2, 12, 'ENV IOS', cell_format_ios)
    worksheet.write(2, 13, 'USER IOS', cell_format_ios)
    worksheet.write(2, 14, ' MANUAL REPORT IOS', cell_format_ios)
    worksheet.write(2, 15, 'AUTO REPORT IOS', cell_format_ios)
    worksheet.write(2, 16, 'BUG JIRA IOS', cell_format_ios)
    worksheet.write(2, 17, 'PRIORITY IOS', cell_format_ios)
    worksheet.write(2, 18, 'DEVICE ANDROID', cell_format_android)
    worksheet.write(2, 19, 'APP VERSION ANDROID', cell_format_android)
    worksheet.write(2, 20, 'ENV ANDROID', cell_format_android)
    worksheet.write(2, 21, 'USER ANDROID', cell_format_android)
    worksheet.write(2, 22, 'MANUAL REPORT ANDROID', cell_format_android)
    worksheet.write(2, 23, 'AUTO REPORT ANDROID', cell_format_android)
    worksheet.write(2, 24, 'BUG JIRA ANDROID', cell_format_android)
    worksheet.write(2, 25, 'PRIORITY ANDROID', cell_format_android)

    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})
    testrows = TestCertificacion.objects.filter(iscore='YES', issp='YES')

    if tipo == 'CORE' and pais=='SP':
        testrows = TestCertificacion.objects.filter(iscore='YES', issp='YES')
    elif tipo == 'CORE' and pais=='PT':
        testrows = TestCertificacion.objects.filter(iscore='YES', ispt='YES')
    elif tipo == 'CORE' and pais=='PL':
        testrows = TestCertificacion.objects.filter(iscore='YES', ispl='YES')
    elif tipo == 'CORE' and pais=='UK':
        testrows = TestCertificacion.objects.filter(iscore='YES', isuk='YES')

    for testline in testrows:
        worksheet.write(row, 0, testline.id, )
        worksheet.write(row, 1, testline.bloque,cell_format_data)
        worksheet.write(row, 2, testline.test,cell_format_data)
        worksheet.write(row, 3, testline.autoios)
        worksheet.write(row, 4, testline.autoandroid)
        worksheet.write(row, 5, testline.manual)
        worksheet.write(row, 6, testline.comentario,cell_format_data)
        worksheet.write(row, 7, testline.iscore)
        worksheet.write(row, 8, pais)
        worksheet.write(row, 9, "")
        worksheet.write(row, 10, "",cell_format_data_ios)
        worksheet.write(row, 11, "",cell_format_data_ios)
        worksheet.write(row, 12, "",cell_format_data_ios)
        worksheet.write(row, 13, "",cell_format_data_ios)
        worksheet.write(row, 14, "",cell_format_data_ios)
        worksheet.write(row, 15, "",cell_format_data_ios)
        worksheet.write(row, 16, "",cell_format_data_ios)
        worksheet.write(row, 17, "",cell_format_data_ios)
        worksheet.write(row, 18, "",cell_format_data_android)
        worksheet.write(row, 19, "",cell_format_data_android)
        worksheet.write(row, 20, "",cell_format_data_android)
        worksheet.write(row, 21, "",cell_format_data_android)
        worksheet.write(row, 22, "",cell_format_data_android)
        worksheet.write(row, 23, "",cell_format_data_android)
        worksheet.write(row, 24, "",cell_format_data_android)
        worksheet.write(row, 25, "",cell_format_data_android)


        row += 1

    book.close()

    return response




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelcertrun(request):
    tipo= request.GET.get("tipo")
    pais= request.GET.get("pais")
    relversion= request.GET.get("relversion")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if relversion is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet(tipo+ '-'+ pais)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', 'Certification Release: ' + relversion, merge_format)
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 15)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(9, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'BLOCK', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'AUTO IOS', cell_format)
    worksheet.write(2, 4, 'AUTO ANDROID', cell_format)
    worksheet.write(2, 5, 'MANUAL', cell_format)
    worksheet.write(2, 6, 'COMMENTS', cell_format)
    worksheet.write(2, 7, 'CORE', cell_format)
    worksheet.write(2, 8, 'COUNTRY', cell_format)
    worksheet.write(2, 9, 'TESTER', cell_format)
    worksheet.write(2, 10, 'DEVICE IOS', cell_format_ios)
    worksheet.write(2, 11, 'APP VERSION IOS', cell_format_ios)
    worksheet.write(2, 12, 'ENV IOS', cell_format_ios)
    worksheet.write(2, 13, 'USER IOS', cell_format_ios)
    worksheet.write(2, 14, ' MANUAL REPORT IOS', cell_format_ios)
    worksheet.write(2, 15, 'AUTO REPORT IOS', cell_format_ios)
    worksheet.write(2, 16, 'BUG JIRA IOS', cell_format_ios)
    worksheet.write(2, 17, 'PRIORITY IOS', cell_format_ios)
    worksheet.write(2, 18, 'DEVICE ANDROID', cell_format_android)
    worksheet.write(2, 19, 'APP VERSION ANDROID', cell_format_android)
    worksheet.write(2, 20, 'ENV ANDROID', cell_format_android)
    worksheet.write(2, 21, 'USER ANDROID', cell_format_android)
    worksheet.write(2, 22, 'MANUAL REPORT ANDROID', cell_format_android)
    worksheet.write(2, 23, 'AUTO REPORT ANDROID', cell_format_android)
    worksheet.write(2, 24, 'BUG JIRA ANDROID', cell_format_android)
    worksheet.write(2, 25, 'PRIORITY ANDROID', cell_format_android)

    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})


    testrows = TestCertificacionRunVersion.objects.filter(iscore='YES', country=pais, releaseversion=relversion)


    for testline in testrows:
        if testline.comentario.startswith('N.A.'):
            continue
        if testline.comentario.startswith('N/A'):
            continue
        worksheet.write(row, 0, testline.testid, )
        worksheet.write(row, 1, testline.bloque,cell_format_data)
        worksheet.write(row, 2, testline.test,cell_format_data)
        worksheet.write(row, 3, testline.autoios)
        worksheet.write(row, 4, testline.autoandroid)
        worksheet.write(row, 5, testline.manual)
        worksheet.write(row, 6, testline.comentario,cell_format_data)
        worksheet.write(row, 7, testline.iscore)
        worksheet.write(row, 8, pais)
        worksheet.write(row, 9, testline.tester)
        worksheet.write(row, 10, testline.deviceios,cell_format_data_ios)
        worksheet.write(row, 11, testline.versionios,cell_format_data_ios)
        worksheet.write(row, 12, testline.envios,cell_format_data_ios)
        worksheet.write(row, 13, testline.userios,cell_format_data_ios)
        worksheet.write(row, 14, testline.manresultios,cell_format_data_ios)
        worksheet.write(row, 15, testline.autoresultios,cell_format_data_ios)
        worksheet.write(row, 16, testline.bugjiraios,cell_format_data_ios)
        worksheet.write(row, 17, testline.priorityios,cell_format_data_ios)
        worksheet.write(row, 18, testline.deviceandroid,cell_format_data_android)
        worksheet.write(row, 19, testline.versionandroid,cell_format_data_android)
        worksheet.write(row, 20, testline.envandroid,cell_format_data_android)
        worksheet.write(row, 21, testline.userandroid,cell_format_data_android)
        worksheet.write(row, 22, testline.manresultandroid,cell_format_data_android)
        worksheet.write(row, 23, testline.autoresultandroid,cell_format_data_android)
        worksheet.write(row, 24, testline.bugjiraandroid,cell_format_data_android)
        worksheet.write(row, 25, testline.priorityandroid,cell_format_data_android)

        row += 1

    book.close()

    return response


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createqaexcel(request):
    pais= request.GET.get("country")
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet = book.add_worksheet( 'AUTO'+ '-'+ pais)
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
   # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = IosTestResultsCert.objects.filter(country=pais).all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        accessibilityid = ''
        print(rx.country)
        if rx.estado != 'passed':
            accessids = StepsResultsIosCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id )
        worksheet.write(row, 1, rx.feature.name )
        worksheet.write(row, 2, rx.test.test )
        worksheet.write(row, 3, rx.feature.bloque )
        worksheet.write(row, 4, rx.country )
        worksheet.write(row, 5, rx.estado )
        worksheet.write(row, 6, rx.test.scenario )
        worksheet.write(row, 7, rx.test.background )
        worksheet.write(row, 8, "YES" )
        worksheet.write(row, 9, accessibilityid )
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y" ))
        row += 1

    worksheet = book.add_worksheet('AUTO-PL')
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
    # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = IosTestResultsCert.objects.filter(country='PL').all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 15, 25)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        print(rx.country)
        accessibilityid = ''
        if rx.estado != 'passed':
            accessids = StepsResultsIosCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id)
        worksheet.write(row, 1, rx.feature.name)
        worksheet.write(row, 2, rx.test.test)
        worksheet.write(row, 3, rx.feature.bloque)
        worksheet.write(row, 4, rx.country)
        worksheet.write(row, 5, rx.estado)
        worksheet.write(row, 6, rx.test.scenario)
        worksheet.write(row, 7, rx.test.background)
        worksheet.write(row, 8, "YES")
        worksheet.write(row, 9, accessibilityid)
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y"))
        row += 1

    worksheet = book.add_worksheet('AUTO-PT')
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
    # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = IosTestResultsCert.objects.filter(country='PT').all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        print(rx.country)
        accessibilityid = ''
        if rx.estado != 'passed':
            accessids = StepsResultsIosCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id)
        worksheet.write(row, 1, rx.feature.name)
        worksheet.write(row, 2, rx.test.test)
        worksheet.write(row, 3, rx.feature.bloque)
        worksheet.write(row, 4, rx.country)
        worksheet.write(row, 5, rx.estado)
        worksheet.write(row, 6, rx.test.scenario)
        worksheet.write(row, 7, rx.test.background)
        worksheet.write(row, 8, "YES")
        worksheet.write(row, 9, accessibilityid)
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y"))
        row += 1

    worksheet = book.add_worksheet('AUTO-UK')
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
    # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = IosTestResultsCert.objects.filter(country='UK').all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        print(rx.country)
        accessibilityid = ''
        if rx.estado != 'passed':
            accessids = StepsResultsIosCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id)
        worksheet.write(row, 1, rx.feature.name)
        worksheet.write(row, 2, rx.test.test)
        worksheet.write(row, 3, rx.feature.bloque)
        worksheet.write(row, 4, rx.country)
        worksheet.write(row, 5, rx.estado)
        worksheet.write(row, 6, rx.test.scenario)
        worksheet.write(row, 7, rx.test.background)
        worksheet.write(row, 8, "YES")
        worksheet.write(row, 9, accessibilityid)
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y"))
        row += 1

    book.close()

    return response



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createqaexcelandroid(request):
    pais= request.GET.get("country")
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet = book.add_worksheet( 'AUTO'+ '-'+ pais)
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
   # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = AndroidTestResultsCert.objects.filter(country=pais).all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        accessibilityid = ''
        print(rx.country)
        if rx.estado != 'passed':
            accessids = StepsResultsAndroidCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id )
        worksheet.write(row, 1, rx.feature.name )
        worksheet.write(row, 2, rx.test.test )
        worksheet.write(row, 3, rx.feature.bloque )
        worksheet.write(row, 4, rx.country )
        worksheet.write(row, 5, rx.estado )
        worksheet.write(row, 6, rx.test.scenario )
        worksheet.write(row, 7, rx.test.background )
        worksheet.write(row, 8, "YES" )
        worksheet.write(row, 9, accessibilityid )
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y" ))
        row += 1

    worksheet = book.add_worksheet('AUTO-PL')
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
    # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = AndroidTestResultsCert.objects.filter(country='PL').all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 15, 25)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        print(rx.country)
        accessibilityid = ''
        if rx.estado != 'passed':
            accessids = StepsResultsAndroidCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id)
        worksheet.write(row, 1, rx.feature.name)
        worksheet.write(row, 2, rx.test.test)
        worksheet.write(row, 3, rx.feature.bloque)
        worksheet.write(row, 4, rx.country)
        worksheet.write(row, 5, rx.estado)
        worksheet.write(row, 6, rx.test.scenario)
        worksheet.write(row, 7, rx.test.background)
        worksheet.write(row, 8, "YES")
        worksheet.write(row, 9, accessibilityid)
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y"))
        row += 1

    worksheet = book.add_worksheet('AUTO-PT')
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
    # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = AndroidTestResultsCert.objects.filter(country='PT').all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        print(rx.country)
        accessibilityid = ''
        if rx.estado != 'passed':
            accessids = StepsResultsAndroidCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id)
        worksheet.write(row, 1, rx.feature.name)
        worksheet.write(row, 2, rx.test.test)
        worksheet.write(row, 3, rx.feature.bloque)
        worksheet.write(row, 4, rx.country)
        worksheet.write(row, 5, rx.estado)
        worksheet.write(row, 6, rx.test.scenario)
        worksheet.write(row, 7, rx.test.background)
        worksheet.write(row, 8, "YES")
        worksheet.write(row, 9, accessibilityid)
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y"))
        row += 1

    worksheet = book.add_worksheet('AUTO-UK')
    worksheet.merge_range('A2:D2', 'Certification Release: 2022-1', merge_format)
    # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = AndroidTestResultsCert.objects.filter(country='UK').all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 20)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 5, 20)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 20)
    worksheet.set_column(10, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'COUNTRY', cell_format)
    worksheet.write(2, 5, 'RESULT', cell_format)
    worksheet.write(2, 6, 'SCENARIO', cell_format)
    worksheet.write(2, 7, 'BACKGROUND', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)
    worksheet.write(2, 9, 'ACC ID ERRORS', cell_format)
    worksheet.write(2, 10, 'RUN DATE', cell_format)
    for rx in resx:
        print(rx.country)
        accessibilityid = ''
        if rx.estado != 'passed':
            accessids = StepsResultsAndroidCert.objects.filter(resultsid=rx ).exclude(accesibilityid__exact='').all()
            for acc in accessids:
                accessibilityid += acc.accesibilityid
        worksheet.write(row, 0, rx.id)
        worksheet.write(row, 1, rx.feature.name)
        worksheet.write(row, 2, rx.test.test)
        worksheet.write(row, 3, rx.feature.bloque)
        worksheet.write(row, 4, rx.country)
        worksheet.write(row, 5, rx.estado)
        worksheet.write(row, 6, rx.test.scenario)
        worksheet.write(row, 7, rx.test.background)
        worksheet.write(row, 8, "YES")
        worksheet.write(row, 9, accessibilityid)
        worksheet.write(row, 10, rx.runid.testdate.strftime("%d/%m/%Y"))
        row += 1

    book.close()

    return response

@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getstatsforreleasee(request):
    release= request.GET.get("release")
    countries =['SP','PT','PL','UK']
    results=[]
    for pais in countries:
        totalios = TestCertificacionRunVersion.objects.filter(releaseversion=release, country=pais, manresultios__icontains='K' ).count()
        totalandroid = TestCertificacionRunVersion.objects.filter(releaseversion=release, country=pais, manresultandroid__icontains='K' ).count()
        okios = TestCertificacionRunVersion.objects.filter(releaseversion=release, country=pais, manresultandroid__iexact='OK' ).count()
        koios = TestCertificacionRunVersion.objects.filter(releaseversion=release, country=pais, manresultandroid__iexact='KO' ).count()
        okandroid = TestCertificacionRunVersion.objects.filter(releaseversion=release, country=pais, manresultandroid__iexact='OK' ).count()
        koandroid = TestCertificacionRunVersion.objects.filter(releaseversion=release, country=pais, manresultandroid__iexact='KO' ).count()
        result1 ={}
        result1["country"] = pais
        result1["totalios"] = totalios
        result1["okios"] =okios
        result1["koios"] =koios
        result1["totalandroid"] =totalandroid
        result1["okandroid"] =okandroid
        result1["koandroid"] =koandroid
        results.append(result1)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format(
            {'bold': True, 'font_color': 'white', 'font_size': 12, 'text_wrap': True, 'align': 'center',
             'bg_color': 'red', 'border': 1, 'border_color': 'black'})
    cell_format_ios = book.add_format(
            {'bold': True, 'font_color': 'white', 'font_size': 12, 'text_wrap': True, 'align': 'center',
             'bg_color': '853a7d', 'border': 1, 'border_color': 'black'})
    cell_format_android = book.add_format(
            {'bold': True, 'font_color': 'white', 'font_size': 12, 'text_wrap': True, 'align': 'center',
             'bg_color': '9760a3', 'border': 1, 'border_color': 'black'})
    merge_format = book.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'yellow', 'locked': True})

    worksheet = book.add_worksheet('MANUAL TESTS RESULTS' )
    worksheet.merge_range('A2:D2', 'Certification Release: ' + release, merge_format)
        # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    row = 3
    col = 0
    worksheet.set_column(0, 0, 20)
    worksheet.set_column(1, 10, 20)

    worksheet.write(2, 0, 'Country', cell_format)
    worksheet.write(2, 1, 'Tests Executed iOS', cell_format)
    worksheet.write(2, 2, 'Tests Passed iOS', cell_format)
    worksheet.write(2, 3, 'Test KO iOS', cell_format)
    worksheet.write(2, 4, 'Tests Executed Android', cell_format)
    worksheet.write(2, 5, 'Tests Passed Android', cell_format)
    worksheet.write(2, 6, 'Tests KO Android', cell_format)
    for rx in results:
        worksheet.write(row, 0, rx['country'])
        worksheet.write(row, 1, rx['totalios'])
        worksheet.write(row, 2, rx['okios'])
        worksheet.write(row, 3, rx['koios'])
        worksheet.write(row, 4, rx['totalandroid'])
        worksheet.write(row, 5, rx['okandroid'])
        worksheet.write(row, 6, rx['koandroid'])
        row = row +1
    book.close()

    return response



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def getjirareportforrelease(request):
    release= request.GET.get("release")
    results=[]

    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format(
            {'bold': True, 'font_color': 'white', 'font_size': 12, 'text_wrap': True, 'align': 'center',
             'bg_color': 'red', 'border': 1, 'border_color': 'black'})
    cell_format_ios = book.add_format(
            {'bold': True, 'font_color': 'white', 'font_size': 12, 'text_wrap': True, 'align': 'center',
             'bg_color': '853a7d', 'border': 1, 'border_color': 'black'})
    cell_format_android = book.add_format(
            {'bold': True, 'font_color': 'white', 'font_size': 12, 'text_wrap': True, 'align': 'center',
             'bg_color': '9760a3', 'border': 1, 'border_color': 'black'})
    merge_format = book.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'yellow', 'locked': True})

    worksheet = book.add_worksheet('BUGS IN CERT' )
    worksheet.merge_range('A2:D2', 'Certification Release: ' + release, merge_format)
        # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    row = 3
    col = 0
    worksheet.set_column(0, 1, 20)
    worksheet.set_column(2, 2, 40)
    worksheet.set_column(3, 4, 20)
    worksheet.set_column(5 ,10, 80)

    worksheet.write(2, 0, 'Issueid', cell_format)
    worksheet.write(2, 1, 'Status', cell_format)
    worksheet.write(2, 2, 'Summary', cell_format)
    worksheet.write(2, 3, 'Components', cell_format)
    worksheet.write(2, 4, 'Priority', cell_format)
    worksheet.write(2, 5, 'Jira Link', cell_format)

    issuesinfo = JiraCertificationReport.objects.filter(reportrelease=release).all()
    for issueinfo in issuesinfo:
        worksheet.write(row, 0, issueinfo.issueid)
        status1 = issueinfo.status
        if status1 in ['Developed','Cerrada']:
            status = 'Fixed'
        elif status1 in ['Developing', 'Ready to Develop', 'In Testing', 'On Hold']:
            status = 'Will be Fixed Later'
        else:
            status = status1
        worksheet.write(row, 1, status)
        worksheet.write(row, 2, issueinfo.summary)
        worksheet.write(row, 3, issueinfo.components)
        worksheet.write(row, 4, issueinfo.priority)
        worksheet.write(row, 5, issueinfo.issuelink)
        row = row +1

    worksheet2 = book.add_worksheet('ISSUES STATS' )
    prios = ['P2', 'P3', 'P4', 'P5']
    results = []
    for prio in prios:

        totalprio = JiraCertificationReport.objects.filter(reportrelease=release, priority=prio).count()
        fixedprio = JiraCertificationReport.objects.filter(reportrelease=release, priority=prio, status__in=['Cerrada','Developed']).count()
        cancelledprio = JiraCertificationReport.objects.filter(reportrelease=release, priority=prio, status__in=['Cancelled']).count()
        laterprio = JiraCertificationReport.objects.filter(reportrelease=release, priority=prio, status__in=['In Testing', 'Ready to Develop','Developing', 'On Hold']).count()

        result1 = {}
        result1["prio"] = prio
        result1["total"] = totalprio
        result1["fixed"] = fixedprio
        result1["cancelled"] = cancelledprio
        result1["later"] = laterprio

        results.append(result1)


    worksheet2.write(2, 0, 'Priority', cell_format)
    worksheet2.write(2, 1, 'Total', cell_format)
    worksheet2.write(2, 2, 'Fixed', cell_format)
    worksheet2.write(2, 3, 'Cancelled', cell_format)
    worksheet2.write(2, 4, 'Will be fixed later', cell_format)
    row=3
    for rx in results:
        worksheet2.write(row, 0, rx['prio'])
        worksheet2.write(row, 1, rx['total'])
        worksheet2.write(row, 2, rx['fixed'])
        worksheet2.write(row, 3, rx['cancelled'])
        worksheet2.write(row, 4, rx['later'])

        row = row +1
    book.close()

    return response


