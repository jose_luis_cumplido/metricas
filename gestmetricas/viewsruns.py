import os
from datetime import datetime

from django.db.models import Q
from django.http import HttpResponseServerError, HttpResponse, JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from requests import Response
from rest_framework import viewsets, status, decorators, response, permissions
from django.contrib.auth.models import User

from gestmetricas.models import TestCertificacion, TestCertificacionRunVersion, IosTestResultsCert, StepsResultsIosCert, \
    StepsResultsAndroidCert, AndroidTestResultsCert, IosTestResults, StepsResultsIos, StepsResults, AndroidTestResults, \
    IosTestRun, ConfluenceInfo, AndroidTestRun
from gestmetricas.utils.ParseExcelTestCertificacionDef import parseExcelTestCertificacionDef, \
    parseExcelTestCertificacionEjecucion
from xlsxwriter import Workbook

from gestmetricas.utils.ftputils import downloadall
from gestmetricas.utils.graphs.plotutils import pltcreatepierun
from gestmetricas.utils.jsonparser.parsefile import createrunfromjson, createandroidrunfromjson
from gestmetricas.views import fixcountersios2, fixcountersandroid2, creahtml, updatepage, uploadimagerun, \
    creahtmlmaster
from metricas import settings


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelrun1(request):
    tipo= request.GET.get("tipo")
    runnumber= request.GET.get("runid")
    pais= request.GET.get("pais")
    relversion= request.GET.get("relversion")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if relversion is None:
        relversion="3-2022"
    a_string = "1 2 3"
    a_list = runnumber.split(",")
    map_runs = map(int, a_list)


    runnumbers = list(map_runs)
    print(runnumbers)

    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet(tipo+ '-'+ pais)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', 'Certification Release: ' + relversion, merge_format)

    # Light red fill with dark red text.
    format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

    # Light yellow fill with dark yellow text.
    format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

    # Green fill with dark green text.
    format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 25)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(9, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'STATUS', cell_format)
    worksheet.write(2, 4, 'OS', cell_format)
    worksheet.write(2, 5, 'COUNTRY', cell_format)
    worksheet.write(2, 6, 'RUNID', cell_format)
    worksheet.write(2, 7, 'DATE', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)

    worksheet.write(2, 9, 'QASTATUS', cell_format)
    worksheet.write(2, 10, 'STEPS OK', cell_format)
    worksheet.write(2, 11, 'STEPS KO', cell_format)
    worksheet.write(2, 12, 'STEPS SKIP', cell_format)


    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

    if tipo == 'IOS':
        testrows = IosTestResults.objects.filter( Q(runid__country=pais)& Q(runid__corecodeversion=relversion) & Q( runid__in=runnumbers) )
    else:
        testrows = AndroidTestResults.objects.filter( Q(runid__country=pais)& Q(runid__corecodeversion=relversion) & Q( runid__in=runnumbers) )

    for testline in testrows:

        worksheet.write(row, 0, testline.test.id, )
        worksheet.write(row, 1, testline.feature.name,cell_format_data)
        worksheet.write(row, 2, testline.test.test,cell_format_data)
        worksheet.write(row, 3, testline.estado,cell_format_data)
        worksheet.write(row, 4, tipo)
        worksheet.write(row, 5, testline.country)
        worksheet.write(row, 6, testline.runid.testname)
        worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
        worksheet.write(row, 8, testline.runid.corecodeversion)
        worksheet.write(row, 9, testline.test.qastatus)
        worksheet.write(row, 10, testline.stepsok,cell_format_data_ios)
        worksheet.write(row, 11, testline.stepsko,cell_format_data_ios)
        worksheet.write(row, 12, testline.stepsskipped,cell_format_data_ios)


        row += 1

    book.close()

    return response



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelaccessidserror(request):
    tipo= request.GET.get("tipo")
    runnumber= request.GET.get("runid")
    pais= request.GET.get("pais")
    relversion= request.GET.get("relversion")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if relversion is None:
        relversion="3-2022"
    a_string = "1 2 3"
    a_list = runnumber.split(",")
    map_runs = map(int, a_list)


    runnumbers = list(map_runs)
    print(runnumbers)

    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet(tipo+ '-'+ pais)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', 'Certification Release: ' + relversion, merge_format)

    # Light red fill with dark red text.
    format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

    # Light yellow fill with dark yellow text.
    format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

    # Green fill with dark green text.
    format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})
    worksheet.conditional_format('L4:L500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('L4:L500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})

    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 25)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(9, 25, 20)

    worksheet.write(2, 0, 'TEST ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'TEST STATUS', cell_format)
    worksheet.write(2, 4, 'OS', cell_format)
    worksheet.write(2, 5, 'COUNTRY', cell_format)
    worksheet.write(2, 6, 'RUNID', cell_format)
    worksheet.write(2, 7, 'DATE', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)

    worksheet.write(2, 9, 'QASTATUS', cell_format)
    worksheet.write(2, 10, 'STEP', cell_format)
    worksheet.write(2, 11, 'STEP STATUS', cell_format)
    worksheet.write(2, 12, 'STEP ERROR', cell_format)


    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

    if tipo == 'IOS':
        testrows = StepsResultsIos.objects.filter( runid__country=pais, runid__corecodeversion=relversion,  runid__in=runnumbers , accesibilityid__contains='{')
    else:
        testrows = StepsResults.objects.filter(runid__country=pais, runid__corecodeversion=relversion,
                                                  runid__in=runnumbers, accesibilityid__contains='accessibility')

    # testrows = IosTestResults.objects.filter( Q(runid__country=pais)& Q(runid__corecodeversion=relversion) & Q( runid__in=runnumbers) )


    for testline in testrows:

        worksheet.write(row, 0, testline.testid.id, )
        worksheet.write(row, 1, testline.testid.featureid.name,cell_format_data)
        worksheet.write(row, 2, testline.testid.test,cell_format_data)
        worksheet.write(row, 3, testline.resultsid.estado,cell_format_data)
        worksheet.write(row, 4, tipo)
        worksheet.write(row, 5, testline.runid.country)
        worksheet.write(row, 6, testline.runid.testname)
        worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
        worksheet.write(row, 8, testline.runid.corecodeversion)
        worksheet.write(row, 9, testline.testid.qastatus)
        worksheet.write(row, 10, testline.stepid.stepname,cell_format_data_ios)

        worksheet.write(row, 11, testline.estado,cell_format_data_ios)
        worksheet.write(row, 12, testline.accesibilityid,cell_format_data_ios)

        row += 1

    book.close()

    return response


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createallfromftp(request):
    rundate = datetime.now().strftime("%Y%m%d")
    mustdownload= request.GET.get("download")
    if mustdownload=='YES':
        downloadall(rundate)
    runpath = '/Users/jose/jenkinsreports/' + rundate
    countries =['uk','portugal','poland','spain']
    plats=['ios', 'android']
    envs =['master'  ,'develop']
    envir= request.GET.get("env")
    pais= request.GET.get("pais")
    platform= request.GET.get("platform")

    runpath = settings.FTP_DOWNLOAD_DIR +  rundate

    if not os.path.exists(runpath):
        os.makedirs(runpath)
    for country in countries:
        p1 = runpath + '/' + country
        if not os.path.exists(p1):
            continue
        for plat in plats:
            p2 = p1 + '/' + plat
            if not os.path.exists(p2):
                continue

            for env1 in envs:
                p3 = p2 + '/' + env1
                if not os.path.exists(p3):
                    continue
                if country == 'spain':
                    countrycode = 'SP'
                if country == 'portugal':
                    countrycode = 'PT'
                if country == 'poland':
                    countrycode = 'PL'
                if country == 'uk':
                    countrycode = 'UK'
                name1 = "CORE"
                if envir is not None  and len(envir) > 0 and envir!=env1:
                    continue
                if pais is not None and len(pais) > 0 and pais!=countrycode:
                    continue
                if platform is not None and len(platform) > 0 and plat!=platform:
                    continue
                if env1 == 'develop':
                    name1 = 'DEVELOP_CORE'
                    if envir == 'master':
                        continue

                try:
                    print( 'Procesando ' + p3 + '/cucumber.json')
                    datosfichero = open(p3 + '/cucumber.json', "r")
                    data = datosfichero.read()
                    if plat == "ios":
                        runid = createrunfromjson(data, name1, 'IOS', countrycode)
                        try:
                            fixcountersios2(runid.id)
                        except Exception as e:
                            print('Error  fixcounters ')
                            print(e)
                    elif plat=='android':
                        runid = createandroidrunfromjson(data, name1, 'ANDROID', countrycode)
                        try:
                            fixcountersandroid2(runid.id)
                        except Exception as e:
                            print('Error  fixcounters ' )
                            print(e)
                except Exception as e:
                    print('Error  con ' + country + '/' + plat + '/' + env1)
                    print(e)


  #  datosfichero = open(runpath+'/' + 'spain' +'/android'+'/develop/' + 'cucumber.json', "r")
  #  data = datosfichero.read()

  #  runid = createandroidrunfromjson( data, 'DEVELOP_CORE', 'ANDROID', 'SP'  )
    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["POST"])
@decorators.permission_classes([permissions.IsAuthenticated])
def updaterunspage(request):
    data2=request.data
    filter=''
    filter=request.POST.get('filter')
    if filter is None:
        filter='ALL'

    datatest = []
    rebuildimagesstr = request.POST.get('rebuildimages')
    if rebuildimagesstr is None:
        rebuildimages = True
    else:
        if rebuildimagesstr=='true':
            rebuildimages= True
        else:
            rebuildimages = False
    confl = ConfluenceInfo.objects.filter(country='IOSRESUME').first()
    if filter == 'MASTER':
        confl = ConfluenceInfo.objects.filter(country='IOSRESUMEMASTER').first()
    pageid=confl.confpageid
    runsios = IosTestRun.objects.all()
    datatest.append( {'H1':'Run ID', 'H2':'Date', 'H3':'Country', 'H4':'Type' ,'H5':'Core Release' , 'H6':'OK', 'H7':'KO', 'H8':'Skipped',
                      'H9':'ID errors', 'H10':'Login errors',
                      'H11':'Graphs','H12':'Excel'})
    for r in runsios:
        if 'IOS_CORE_' not in r.testname and 'IOS_DEVELOP_CORE_' not in r.testname:
            continue
        if filter=='MASTER':
            if '_DEVELOP_CORE_'  in r.testname:
                continue
        runtype = 'LOCAL'
        if r.runtype == 'JSON':
            runtype='JENKINS'
        loginerrors = IosTestResults.objects.filter(runid=r, loginerror=True).count()
        iderrors = IosTestResults.objects.filter(runid=r, accessibilityerror=True , loginerror=False).count()
        othererrors = r.testsko -loginerrors -iderrors
        datatest.append({'H1': r.testname, 'H2': r.testdate.strftime('%d/%m/%Y'), 'H3': r.country, 'H4': runtype, 'H5': r.corecodeversion, 'H6':r.testsok,
                         'H7': othererrors, 'H8':r.testsskipped, 'H9':iderrors,  'H10':loginerrors})
        if rebuildimages:
            datapie = [r.testsok,othererrors,r.testsskipped, loginerrors, iderrors]
            imagepath= pltcreatepierun(datapie, r.testname)
            uploadimagerun(pageid, r.testname)


    if filter=='MASTER':
        body = creahtmlmaster('', datatest, 'IOS')
    else:
        body = creahtml('', datatest,'IOS')
    pagetitle = confl.pagename + ' -> updated on ' + datetime.now().strftime('%d/%m%/%Y %H:%M')
    confl.lastupdate = datetime.now()
    confl.save()
    updatepage(pagetitle, pageid, body )

    datatest = []
    pageid=''
    confl = ConfluenceInfo.objects.filter(country='ANDROIDRESUME').first()
    if filter == 'MASTER':
        confl = ConfluenceInfo.objects.filter(country='ANDROIDRESUMEMASTER').first()

    pageid=confl.confpageid
    runsandroid = AndroidTestRun.objects.all()
    datatest.append( {'H1':'Run ID', 'H2':'Date', 'H3':'Country', 'H4':'Type' ,'H5':'Core Release' , 'H6':'OK', 'H7':'KO', 'H8':'Skipped',
                      'H9':'ID errors', 'H10':'Login errors',
                      'H11':'Graphs','H12':'Excel'})
    for r in runsandroid:
        runtype = 'LOCAL'
        if r.runtype == 'JSON':
            runtype='JENKINS'
        if 'ANDROID_CORE_' not in r.testname and 'ANDROID_DEVELOP_CORE_' not in r.testname:
            continue
        if filter=='MASTER':
            if '_DEVELOP_CORE_'  in r.testname:
                continue
        errorsTot = AndroidTestResults.objects.filter(runid=r, estado='failed').count()
        loginerrors = AndroidTestResults.objects.filter(runid=r, loginerror=True, accessibilityerror=False).count()
        iderrors = AndroidTestResults.objects.filter(runid=r, accessibilityerror=True, loginerror=False).count()
        othererrors = errorsTot -loginerrors -iderrors
        datatest.append({'H1': r.testname, 'H2': r.testdate.strftime('%d/%m/%Y'), 'H3': r.country, 'H4': runtype, 'H5': r.corecodeversion, 'H6':r.testsok,
                         'H7': othererrors, 'H8':r.testsskipped, 'H9':iderrors,  'H10':loginerrors})
      #  datatest.append({'H1': r.testname, 'H2': r.testdate.strftime('%d/%m/%Y'), 'H3': r.country, 'H4': runtype, 'H5': r.corecodeversion, 'H6':r.testsok,
      #                   'H7': r.testsko, 'H8':r.testsskipped})
        if rebuildimages:
            datapie = [r.testsok,othererrors,r.testsskipped, loginerrors, iderrors]
      #      datapie = [r.testsok,r.testsko,r.testsskipped]
            imagepath= pltcreatepierun(datapie, r.testname)
            uploadimagerun(pageid, r.testname)
    if filter=='MASTER':
        body = creahtmlmaster('', datatest, 'ANDROID')
    else:
        body = creahtml('', datatest, 'ANDROID')

    pagetitle = confl.pagename + ' -> updated on ' + datetime.now().strftime('%d/%m%/%Y %H:%M')
    updatepage(pagetitle, pageid, body )
    confl.lastupdate = datetime.now()
    confl.save()

    return HttpResponse( status.HTTP_201_CREATED)


