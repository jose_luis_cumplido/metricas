import json
import logging
import sys
import uuid
import zipfile
from collections import namedtuple
import base64
import io
from datetime import datetime
from doctest import TestResults
from uuid import UUID

import django_filters
import requests
from django.core.files.base import ContentFile
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseServerError, HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from requests import Response
from rest_framework import viewsets, status, decorators, response, permissions
from django.contrib.auth.models import User
from xlsxwriter import Workbook

from gestmetricas.models import ZeplinProject, ZeplinScreen, ZeplinScreenComponent
from gestmetricas.serializers import ZeplinProjectSerializer, ZeplinScreenSerializer


class ZeplinProjectViewSet(viewsets.ModelViewSet):
    queryset =  ZeplinProject.objects.all()
    serializer_class =  ZeplinProjectSerializer


class ZeplinScreenViewSet(viewsets.ModelViewSet):
    queryset =  ZeplinScreen.objects.select_related('project').all()
    serializer_class =  ZeplinScreenSerializer

@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcel(request):
    prjid= request.GET.get("project")
    project = ZeplinProject.objects.filter(id=prjid).first()
    if project is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    screens = ZeplinScreen.objects.filter(project=project).all()
    if screens is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 14, 'align':'center','bg_color':'red'})
    worksheet = book.add_worksheet(project.name+ '-'+ project.platform)
    row = 3
    col = 0
    worksheet.set_column(0, 1, 45)
    worksheet.set_column(2, 4, 15)
    worksheet.set_column(5, 6, 8)
    worksheet.set_column(7, 7, 25)
    worksheet.write(2, 0, 'SCREEN', cell_format)
    worksheet.write(2, 1, 'ELEMENT', cell_format )
    worksheet.write(2, 2, 'TYPE', cell_format)
    worksheet.write(2, 3, 'WIDTH', cell_format)
    worksheet.write(2, 4, 'HEIGHT', cell_format)
    worksheet.write(2, 5, 'X', cell_format)
    worksheet.write(2, 6, 'Y', cell_format)
    worksheet.write(2, 7, 'IMAGE', cell_format)

    for scr1 in screens:
        all = ZeplinScreenComponent.objects.filter(screen=scr1).all()
        a = scr1.versions.all()
        img = a[0].imageurl

    # Iterate over the data and write it out row by row.
        for item in (all):
            worksheet.write(row, 0, scr1.name)
            worksheet.write(row, 1, item.name)
            worksheet.write(row, 2, item.elemtype)
            worksheet.write(row, 3, item.width)
            worksheet.write(row, 4, item.height)
            worksheet.write(row,5, item.absx)
            worksheet.write(row, 6, item.absx)
            worksheet.write_url(row,7, img, string='Show screen image')

            row += 1


    book.close()

    return response