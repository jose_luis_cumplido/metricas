from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers, permissions
from gestmetricas import views, zeplinviews, viewscertificacion, viewsruns, viewsexcelrunslocal, historyviews
from django.conf.urls import url
from django.urls import path, include, re_path


router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)
router.register(r'excelmetricas', views.ExcelMetricasViewSet, basename='metricas')
router.register(r'testdefinition', views.TestDefinitionViewSet)
router.register(r'teststatus', views.StatusInfoViewSet)
router.register(r'testsqa', views.TestsQAViewSet)
router.register(r'iostestrun', views.IosTestRunViewSet)
router.register(r'iostestresults', views.IosTestResultsViewSet)
router.register(r'stepsdefinition', views.StepsDefinitionViewSet)
router.register(r'stepsresultsios', views.StepsResultsIosViewSet)
router.register(r'feature', views.FeatureViewSet)
router.register(r'androidtestrun', views.AndroidTestRunViewSet)
router.register(r'androidtestresults', views.AndroidTestResultsViewSet)
router.register(r'stepsresults', views.StepsResultsViewSet)
router.register(r'zeplinprojects', zeplinviews.ZeplinProjectViewSet)
router.register(r'zeplinscreens', zeplinviews.ZeplinScreenViewSet)
router.register(r'iostestruncert', views.IosTestRunCertViewSet)
router.register(r'iostestresultscert', views.IosTestResultsCertViewSet)
router.register(r'stepsdefinitioncert', views.StepsDefinitionCertViewSet)
router.register(r'stepsresultsioscert', views.StepsResultsIosCertViewSet)
router.register(r'feature', views.FeatureViewSet)

schema_view = get_schema_view(
   openapi.Info(
      title="Metricas API",
      default_version='v1',
      description="This API is for you",
      terms_of_service="https://www.groupchrono.com",
      contact=openapi.Contact(email="info@gestmetricas.com"),
      license=openapi.License(name=""),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('', include(router.urls)),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^updatefile/$', views.updatefile),
    url(r'^updateexcel/$', views.updateexcel),
    url(r'^getcounters/$', views.getcounters),
    url(r'^getcountersqa/$', views.getcountersqa),
    url(r'^teststeps/$', views.teststeps),
    url(r'^uploadhtml/$', views.uploadhtml),
    url(r'^uploadiosrun/$', views.uploadiosrun),
    url(r'^uploadiosrunjson/$', views.uploadiosrunjson),
    url(r'^uploadandroidrunjson/$', views.uploadandroidrunjson),
    url(r'^connectscenarios/$', views.connectscenarios),
    url(r'^fixiosruncounters/$', views.fixiosruncounters),
    url(r'^fixandroidruncounters/$', views.fixandroidruncounters),
    url(r'^createconfpage/$', views.createtestpage),
    url(r'^createtestpage/$', views.createtestpage),
    url(r'^updatecountrypage/$', views.updatecountrypage),
    url(r'^createimagepage/$', views.createimagepage),

    url(r'^fixskippedcounters/$', views.fixskippedcounters),
    url(r'^jiracertreleasereport/$', views.jiracertreleasereport),
    url(r'^getjirareportforrelease/$', viewscertificacion.getjirareportforrelease),

    url(r'^listzeplinprojects/$', views.listzeplinprojects),
    url(r'^getscreencomponents/$', views.getscreencomponents),
    url(r'^getscreens/$', views.getscreens),
    url(r'^getscreenversions/$', views.getscreenversions),
    url(r'^createexcel/$', zeplinviews.createexcel),
    url(r'^getallscreenversions/$', views.getallscreenversions),
    url(r'^getallscreencomponents/$', views.getallscreencomponents),

    url(r'^createpie1/$', views.createpie1),
    url(r'^createbars/$', views.createbars),
    url(r'^createbarsmaster/$', views.createbarsmaster),

    url(r'^uploadhtml2/$', views.uploadhtml2),
    url(r'^uploadiosrun2/$', views.uploadiosrun2),
    url(r'^uploadandroidrun2/$', views.uploadandroidrun2),

    url(r'^uploadexcelcert/$', viewscertificacion.uploadexcelcert),
    url(r'^createexcelcert/$', viewscertificacion.createexcelcert),

    url(r'^uploadhtmlcert/$', views.uploadhtmlcert),
    url(r'^uploadiosruncert/$', views.uploadiosruncert),
    url(r'^uploadhtml2cert/$', views.uploadhtml2cert),
    url(r'^uploadiosrun2cert/$', views.uploadiosrun2cert),
    url(r'^uploadandroidrun2cert/$', views.uploadandroidrun2cert),

    url(r'^refreshjirabugs/$', views.refreshjirabugs),

    url(r'^addmodulestocert/$', viewscertificacion.addmodulestocert),

    url(r'^uploadexcelcertrun/$', viewscertificacion.uploadexcelcertrun),
    url(r'^createexcelcertrun/$', viewscertificacion.createexcelcertrun),
    url(r'^uploadvideo/$', views.uploadvideostoconfl),

    url(r'^createqaexcel/$', viewscertificacion.createqaexcel),
    url(r'^createqaexcelandroid/$', viewscertificacion.createqaexcelandroid),
    url(r'^getstatsforreleasee/$', viewscertificacion.getstatsforreleasee),


    url(r'^createexcelrun1/$', viewsruns.createexcelrun1),
    url(r'^createexcelaccessidserror/$', viewsruns.createexcelaccessidserror),
    url(r'^createexcelaccessidserror/$', viewsruns.createexcelaccessidserror),


    url(r'^createallfromftp/$', viewsruns.createallfromftp),
    url(r'^updaterunspage/$', viewsruns.updaterunspage),

    url(r'^createexcelrunsave/$', viewsexcelrunslocal.createexcelrunsave),
    url(r'^createexcelrunsaveall/$', viewsexcelrunslocal.createexcelrunsaveall),
    url(r'^createexcelaccessidserrorsaveall/$', viewsexcelrunslocal.createexcelaccessidserrorsaveall),
    url(r'^createexcelloginerrorsaveall/$', viewsexcelrunslocal.createexcelloginerrorsaveall),

    url(r'^createlogtests/$', views.createlogtests),
    url(r'^createlogtestsjson/$', views.createlogtestsjson),
    url(r'^createexcelrunlogs/$', viewsexcelrunslocal.createexcelrunlogs),
    url(r'^createqaexcellogs/$', viewsexcelrunslocal.createqaexcellogs),
    url(r'^analizechanges/$', historyviews.analizechanges),
    url(r'^createexcelhistorychanges/$', historyviews.createexcelhistorychanges),


]