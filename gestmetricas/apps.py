from django.apps import AppConfig


class GestmetricasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gestmetricas'
