from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from rest_framework import serializers
from django.core.serializers.json import Serializer

from gestmetricas.models import ExcelMetricas, TestDefinition, StatusInfo, TestsQA, IosTestRun, IosTestResults, \
    StepsDefinition, StepsResultsIos, Feature, AndroidTestRun, AndroidTestResults, StepsResults, StatusInfoPL, \
    ZeplinProject, ZeplinScreen, TestsQACert, IosTestRunCert, IosTestResultsCert, StepsDefinitionCert, \
    StepsResultsIosCert, FeatureCert


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'groups','first_name','last_name']


class ExcelMetricasSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExcelMetricas
        fields = '__all__'


class TestDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = '__all__'


class StatusInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatusInfo
        fields = '__all__'
        depth=1

class StatusInfoPLSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatusInfoPL
        fields = '__all__'
        depth=1

class TestsQASerializer(serializers.ModelSerializer):
    class Meta:
        model = TestsQA
        fields = '__all__'


class IosTestRunSerializer(serializers.ModelSerializer):
    class Meta:
        model = IosTestRun
        fields = '__all__'


class IosTestResultsSerializer(serializers.ModelSerializer):
    class Meta:
        model = IosTestResults
        fields = '__all__'
        depth=1


class StepsDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = StepsDefinition
        fields = '__all__'


class StepsResultsIosSerializer(serializers.ModelSerializer):
    class Meta:
        model = StepsResultsIos
        fields = '__all__'
        depth=1


class FeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feature
        fields = '__all__'
        depth=1



class AndroidTestRunSerializer(serializers.ModelSerializer):
    class Meta:
        model = AndroidTestRun
        fields = '__all__'


class AndroidTestResultsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AndroidTestResults
        fields = '__all__'
        depth=1


class StepsResultsSerializer(serializers.ModelSerializer):
    class Meta:
        model = StepsResults
        fields = '__all__'
        depth=1


class ZeplinProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = ZeplinProject
        fields = '__all__'
        depth=1


class ZeplinScreenSerializer(serializers.ModelSerializer):
    class Meta:
        model = ZeplinScreen
        fields = '__all__'
        depth=1



class TestsQACertSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestsQACert
        fields = '__all__'


class IosTestRunCertSerializer(serializers.ModelSerializer):
    class Meta:
        model = IosTestRunCert
        fields = '__all__'


class IosTestResultsCertSerializer(serializers.ModelSerializer):
    class Meta:
        model = IosTestResultsCert
        fields = '__all__'
        depth=1


class StepsDefinitionCertSerializer(serializers.ModelSerializer):
    class Meta:
        model = StepsDefinitionCert
        fields = '__all__'


class StepsResultsIosCertSerializer(serializers.ModelSerializer):
    class Meta:
        model = StepsResultsIosCert
        fields = '__all__'
        depth=1


class FeatureCertSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeatureCert
        fields = '__all__'
        depth=1

