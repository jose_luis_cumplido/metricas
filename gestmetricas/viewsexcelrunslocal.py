import os
from datetime import datetime

from django.db.models import Q
from django.http import HttpResponseServerError, HttpResponse, JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from requests import Response
from rest_framework import viewsets, status, decorators, response, permissions
from django.contrib.auth.models import User

from gestmetricas.models import TestCertificacion, TestCertificacionRunVersion, IosTestResultsCert, StepsResultsIosCert, \
    StepsResultsAndroidCert, AndroidTestResultsCert, IosTestResults, StepsResultsIos, StepsResults, AndroidTestResults, \
    IosTestRun, ConfluenceInfo, AndroidTestRun, TestsQA, LogTestDefinition
from gestmetricas.utils.ParseExcelTestCertificacionDef import parseExcelTestCertificacionDef, \
    parseExcelTestCertificacionEjecucion
from xlsxwriter import Workbook

from gestmetricas.utils.ftputils import downloadall
from gestmetricas.utils.graphs.plotutils import pltcreatepierun
from gestmetricas.utils.jsonparser.parsefile import createrunfromjson, createandroidrunfromjson
from gestmetricas.views import fixcountersios2, fixcountersandroid2, creahtml, updatepage, uploadimagerun, \
    uploadexceltoconfl


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelrunsave(request):
    tipo= request.GET.get("tipo")
    testname= request.GET.get("testname")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    book = Workbook( 'graphimages/excel/'+testname+'.xlsx')
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet( testname)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', ' Auto : ' + testname, merge_format)

    # Light red fill with dark red text.
    format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

    # Light yellow fill with dark yellow text.
    format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

    # Green fill with dark green text.
    format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 25)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(9, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'STATUS', cell_format)
    worksheet.write(2, 4, 'OS', cell_format)
    worksheet.write(2, 5, 'COUNTRY', cell_format)
    worksheet.write(2, 6, 'RUNID', cell_format)
    worksheet.write(2, 7, 'DATE', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)

    worksheet.write(2, 9, 'QASTATUS', cell_format)
    worksheet.write(2, 10, 'STEPS OK', cell_format)
    worksheet.write(2, 11, 'STEPS KO', cell_format)
    worksheet.write(2, 12, 'STEPS SKIP', cell_format)


    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

    if tipo == 'IOS':
        testrows = IosTestResults.objects.filter( runid__testname =testname )
    else:
        testrows = AndroidTestResults.objects.filter( runid__testname=testname )

    for testline in testrows:

        worksheet.write(row, 0, testline.test.id, )
        worksheet.write(row, 1, testline.feature.name,cell_format_data)
        worksheet.write(row, 2, testline.test.test,cell_format_data)
        worksheet.write(row, 3, testline.estado,cell_format_data)
        worksheet.write(row, 4, tipo)
        worksheet.write(row, 5, testline.country)
        worksheet.write(row, 6, testline.runid.testname)
        worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
        worksheet.write(row, 8, testline.runid.corecodeversion)
        worksheet.write(row, 9, testline.test.qastatus)
        worksheet.write(row, 10, testline.stepsok,cell_format_data_ios)
        worksheet.write(row, 11, testline.stepsko,cell_format_data_ios)
        worksheet.write(row, 12, testline.stepsskipped,cell_format_data_ios)


        row += 1

    book.close()
    confios = ConfluenceInfo.objects.filter(country='IOSRESUME').first()
    confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUME').first()
    if tipo=='IOS':
        uploadexceltoconfl(confios.confpageid, testname)
    else:
        uploadexceltoconfl(confand.confpageid, testname)


    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelaccessidserrorsave(request):
    tipo= request.GET.get("tipo")
    runnumber= request.GET.get("runid")
    pais= request.GET.get("pais")
    relversion= request.GET.get("relversion")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if relversion is None:
        relversion="3-2022"
    a_string = "1 2 3"
    a_list = runnumber.split(",")
    map_runs = map(int, a_list)


    runnumbers = list(map_runs)
    print(runnumbers)

    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet(tipo+ '-'+ pais)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', 'Certification Release: ' + relversion, merge_format)

    # Light red fill with dark red text.
    format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

    # Light yellow fill with dark yellow text.
    format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

    # Green fill with dark green text.
    format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})
    worksheet.conditional_format('L4:L500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('L4:L500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})

    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 25)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(9, 25, 20)

    worksheet.write(2, 0, 'TEST ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'TEST STATUS', cell_format)
    worksheet.write(2, 4, 'OS', cell_format)
    worksheet.write(2, 5, 'COUNTRY', cell_format)
    worksheet.write(2, 6, 'RUNID', cell_format)
    worksheet.write(2, 7, 'DATE', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)

    worksheet.write(2, 9, 'QASTATUS', cell_format)
    worksheet.write(2, 10, 'STEP', cell_format)
    worksheet.write(2, 11, 'STEP STATUS', cell_format)
    worksheet.write(2, 12, 'STEP ERROR', cell_format)


    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

    if tipo == 'IOS':
        testrows = StepsResultsIos.objects.filter( runid__country=pais, runid__corecodeversion=relversion,  runid__in=runnumbers , accesibilityid__contains='{')
    else:
        testrows = StepsResults.objects.filter(runid__country=pais, runid__corecodeversion=relversion,
                                                  runid__in=runnumbers, accesibilityid__contains='accessibility')

    # testrows = IosTestResults.objects.filter( Q(runid__country=pais)& Q(runid__corecodeversion=relversion) & Q( runid__in=runnumbers) )


    for testline in testrows:

        worksheet.write(row, 0, testline.testid.id, )
        worksheet.write(row, 1, testline.testid.featureid.name,cell_format_data)
        worksheet.write(row, 2, testline.testid.test,cell_format_data)
        worksheet.write(row, 3, testline.resultsid.estado,cell_format_data)
        worksheet.write(row, 4, tipo)
        worksheet.write(row, 5, testline.runid.country)
        worksheet.write(row, 6, testline.runid.testname)
        worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
        worksheet.write(row, 8, testline.runid.corecodeversion)
        worksheet.write(row, 9, testline.testid.qastatus)
        worksheet.write(row, 10, testline.stepid.stepname,cell_format_data_ios)

        worksheet.write(row, 11, testline.estado,cell_format_data_ios)
        worksheet.write(row, 12, testline.accesibilityid,cell_format_data_ios)

        row += 1

    book.close()

    return response




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def  createexcelrunsaveall(request):
    tipo= request.GET.get("tipo")
    filter= request.GET.get("filter")
    release= request.GET.get("release")
    country= request.GET.get("country")
    fromdate= request.GET.get("fromdate")
    datefrom = None
    if fromdate is None or fromdate=='':
        print("All dates ")
    else:
        datefrom = datetime.strptime(fromdate, '%d/%m/%Y')
    if filter is None:
        filter='ALL'
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if tipo== 'IOS':
        if datefrom is None:
            runs = IosTestRun.objects.all()
        else:
            runs=IosTestRun.objects.filter(testdate__gt=datefrom).all()
    else:
        if datefrom is None:
            runs = AndroidTestRun.objects.all()
        else:
            runs=AndroidTestRun.objects.filter(testdate__gt=datefrom).all()


    for r in runs:
        if '_ACCOUNTS_' in r.testname:
            continue
        if '_LOANS_' in r.testname:
            continue
        if filter=='MASTER':
            if '_DEVELOP_' in r.testname:
                continue
        if release is None or release=='ALL':
            print( 'Release ' + r.corecodeversion)
        else:
            if r.corecodeversion != release:
                continue
        if country is None or country=='ALL':
            print( 'Country ' + r.country)
        else:
            if r.country != country:
                print(' Bypassing Country ' + r.country)
                continue

        tname= r.testname
        tname = tname.replace("_DEVELOP_", "_D_")
        tname = tname.replace("_JSON_", "_J_")
        print("Creating excel for " + tipo + " " + r.testname)
        book = Workbook( 'graphimages/excel/'+r.testname+'.xlsx')
        cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
        cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
        cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
        cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
        worksheet = book.add_worksheet( tname)
        merge_format = book.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'yellow', 'locked':True})

        worksheet.merge_range('A2:D2', ' Auto : ' + r.testname, merge_format)

        # Light red fill with dark red text.
        format1 = book.add_format({'bg_color': '#FFC7CE',
                                       'font_color': '#9C0006'})

        # Light yellow fill with dark yellow text.
        format2 = book.add_format({'bg_color': '#FFEB9C',
                                       'font_color': '#9C6500'})

        # Green fill with dark green text.
        format3 = book.add_format({'bg_color': '#C6EFCE',
                                       'font_color': '#006100'})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'failed',
                                               'format': format1})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'passed',
                                               'format': format3})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                                 'criteria': 'containing',
                                                 'value': 'skipped',
                                                 'format': format2})
        row = 3
        col = 0
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1,1, 25)
        worksheet.set_column(2,2, 35)
        worksheet.set_column(3, 5, 10)
        worksheet.set_column(6, 6, 25)
        worksheet.set_column(7, 8, 10)
        worksheet.set_column(9, 25, 20)

        worksheet.write(2, 0, 'ID', cell_format)
        worksheet.write(2, 1, 'FEATURE', cell_format )
        worksheet.write(2, 2, 'TEST', cell_format)
        worksheet.write(2, 3, 'STATUS', cell_format)
        worksheet.write(2, 4, 'OS', cell_format)
        worksheet.write(2, 5, 'COUNTRY', cell_format)
        worksheet.write(2, 6, 'RUNID', cell_format)
        worksheet.write(2, 7, 'DATE', cell_format)
        worksheet.write(2, 8, 'CORE', cell_format)

        worksheet.write(2, 9, 'QASTATUS', cell_format)
        worksheet.write(2, 10, 'STEPS OK', cell_format)
        worksheet.write(2, 11, 'STEPS KO', cell_format)
        worksheet.write(2, 12, 'STEPS SKIP', cell_format)
        worksheet.write(2, 13, 'LOGIN ERROR', cell_format)
        worksheet.write(2, 14, 'LOGIN Error Text', cell_format)
        worksheet.write(2, 15, 'ID ERROR ', cell_format)


        cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
        cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
        cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

        if tipo == 'IOS':
            testrows = IosTestResults.objects.filter( runid__testname =r.testname )
        else:
            testrows = AndroidTestResults.objects.filter( runid__testname=r.testname )

        for testline in testrows:

            worksheet.write(row, 0, testline.test.id, )
            worksheet.write(row, 1, testline.feature.name,cell_format_data)
            worksheet.write(row, 2, testline.test.test,cell_format_data)
            worksheet.write(row, 3, testline.estado,cell_format_data)
            worksheet.write(row, 4, tipo)
            worksheet.write(row, 5, testline.country)
            worksheet.write(row, 6, testline.runid.testname)
            worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
            worksheet.write(row, 8, testline.runid.corecodeversion)
            worksheet.write(row, 9, testline.test.qastatus)
            worksheet.write(row, 10, testline.stepsok,cell_format_data_ios)
            worksheet.write(row, 11, testline.stepsko,cell_format_data_ios)
            worksheet.write(row, 12, testline.stepsskipped,cell_format_data_ios)
            worksheet.write(row, 13, testline.loginerror,cell_format_data_ios)
            worksheet.write(row, 14, testline.loginerrortext,cell_format_data_ios)
            worksheet.write(row, 15, testline.accessibilityerror,cell_format_data_ios)


            row += 1

        book.close()
        confios = ConfluenceInfo.objects.filter(country='IOSRESUME').first()
        confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUME').first()
        if filter=='MASTER':
            confios = ConfluenceInfo.objects.filter(country='IOSRESUMEMASTER').first()
            confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUMEMASTER').first()
        if tipo=='IOS':
            uploadexceltoconfl(confios.confpageid, r.testname)
        else:
            uploadexceltoconfl(confand.confpageid, r.testname)
        print("Created excel for "  + r.testname)


    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelaccessidserrorsaveall(request):
    tipo= request.GET.get("tipo")
    release= request.GET.get("release")
    filter= request.GET.get("filter")
    country= request.GET.get("country")
    fromdate= request.GET.get("fromdate")
    datefrom = None
    if fromdate is None or fromdate=='':
        print("All dates ")
    else:
        datefrom = datetime.strptime(fromdate, '%d/%m/%Y')
    if filter is None:
        filter='ALL'
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if tipo== 'IOS':
        if datefrom is None:
            runs = IosTestRun.objects.all()
        else:
            runs=IosTestRun.objects.filter(testdate__gt=datefrom).all()
    else:
        if datefrom is None:
            runs = AndroidTestRun.objects.all()
        else:
            runs=AndroidTestRun.objects.filter(testdate__gt=datefrom).all()

    for r in runs:
        if '_ACCOUNTS_' in r.testname:
            continue
        if '_LOANS_' in r.testname:
            continue
        if filter=='MASTER':
            if '_DEVELOP_' in r.testname:
                continue
        if release is None or release=='ALL':
            print( 'Release ' + r.corecodeversion)
        else:
            if r.corecodeversion != release:
                continue
        if country is None or country=='ALL':
            print( 'Country ' + r.country)
        else:
            if r.country != country:
                print(' Bypassing Country ' + r.country)
                continue
        tname= r.testname
        tname = tname.replace("_DEVELOP_", "_D_")
        tname = tname.replace("_JSON_", "_J_")
        print("Creating excel for " + tipo + " " + r.testname)
        book = Workbook( 'graphimages/excel/'+'IDERROR_'+r.testname+'.xlsx')
        cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
        cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
        cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
        cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
        worksheet = book.add_worksheet( tname)
        merge_format = book.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'yellow', 'locked':True})

        worksheet.merge_range('A2:D2', 'Run: ' + r.testname, merge_format)

        # Light red fill with dark red text.
        format1 = book.add_format({'bg_color': '#FFC7CE',
                                       'font_color': '#9C0006'})

        # Light yellow fill with dark yellow text.
        format2 = book.add_format({'bg_color': '#FFEB9C',
                                       'font_color': '#9C6500'})

        # Green fill with dark green text.
        format3 = book.add_format({'bg_color': '#C6EFCE',
                                       'font_color': '#006100'})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'failed',
                                               'format': format1})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'passed',
                                               'format': format3})
        worksheet.conditional_format('L4:L500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'failed',
                                               'format': format1})
        worksheet.conditional_format('L4:L500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'passed',
                                               'format': format3})

        row = 3
        col = 0
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1,1, 25)
        worksheet.set_column(2,2, 35)
        worksheet.set_column(3, 5, 10)
        worksheet.set_column(6, 6, 25)
        worksheet.set_column(7, 8, 10)
        worksheet.set_column(9, 25, 20)

        worksheet.write(2, 0, 'TEST ID', cell_format)
        worksheet.write(2, 1, 'FEATURE', cell_format )
        worksheet.write(2, 2, 'TEST', cell_format)
        worksheet.write(2, 3, 'TEST STATUS', cell_format)
        worksheet.write(2, 4, 'OS', cell_format)
        worksheet.write(2, 5, 'COUNTRY', cell_format)
        worksheet.write(2, 6, 'RUNID', cell_format)
        worksheet.write(2, 7, 'DATE', cell_format)
        worksheet.write(2, 8, 'CORE', cell_format)

        worksheet.write(2, 9, 'QASTATUS', cell_format)
        worksheet.write(2, 10, 'STEP', cell_format)
        worksheet.write(2, 11, 'STEP STATUS', cell_format)
        worksheet.write(2, 12, 'STEP ERROR', cell_format)


        cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
        cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
        cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

        if tipo == 'IOS':
            testrows = StepsResultsIos.objects.filter( runid__testname=r.testname, accesibilityid__contains='{')
        else:
            testrows = StepsResults.objects.filter(runid__testname=r.testname, accesibilityid__contains='{')

        # testrows = IosTestResults.objects.filter( Q(runid__country=pais)& Q(runid__corecodeversion=relversion) & Q( runid__in=runnumbers) )


        for testline in testrows:

            worksheet.write(row, 0, testline.testid.id, )
            worksheet.write(row, 1, testline.testid.featureid.name,cell_format_data)
            worksheet.write(row, 2, testline.testid.test,cell_format_data)
            worksheet.write(row, 3, testline.resultsid.estado,cell_format_data)
            worksheet.write(row, 4, tipo)
            worksheet.write(row, 5, testline.runid.country)
            worksheet.write(row, 6, testline.runid.testname)
            worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
            worksheet.write(row, 8, testline.runid.corecodeversion)
            worksheet.write(row, 9, testline.testid.qastatus)
            worksheet.write(row, 10, testline.stepid.stepname,cell_format_data_ios)

            worksheet.write(row, 11, testline.estado,cell_format_data_ios)
            worksheet.write(row, 12, testline.accesibilityid,cell_format_data_ios)

            row += 1

        worksheet = book.add_worksheet('LOGINERRORS')
        merge_format = book.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'yellow', 'locked': True})

        worksheet.merge_range('A2:D2', 'Run: ' + r.testname, merge_format)

        # Light red fill with dark red text.
        format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

        # Light yellow fill with dark yellow text.
        format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

        # Green fill with dark green text.
        format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                                 'criteria': 'containing',
                                                 'value': 'failed',
                                                 'format': format1})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                                 'criteria': 'containing',
                                                 'value': 'passed',
                                                 'format': format3})
        worksheet.conditional_format('L4:L500', {'type': 'text',
                                                 'criteria': 'containing',
                                                 'value': 'failed',
                                                 'format': format1})
        worksheet.conditional_format('L4:L500', {'type': 'text',
                                                 'criteria': 'containing',
                                                 'value': 'passed',
                                                 'format': format3})

        row = 3
        col = 0
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 1, 25)
        worksheet.set_column(2, 2, 35)
        worksheet.set_column(3, 5, 10)
        worksheet.set_column(6, 6, 25)
        worksheet.set_column(7, 8, 10)
        worksheet.set_column(9, 11, 20)
        worksheet.set_column(12, 13, 100)

        worksheet.write(2, 0, 'TEST ID', cell_format)
        worksheet.write(2, 1, 'FEATURE', cell_format)
        worksheet.write(2, 2, 'TEST', cell_format)
        worksheet.write(2, 3, 'TEST STATUS', cell_format)
        worksheet.write(2, 4, 'OS', cell_format)
        worksheet.write(2, 5, 'COUNTRY', cell_format)
        worksheet.write(2, 6, 'RUNID', cell_format)
        worksheet.write(2, 7, 'DATE', cell_format)
        worksheet.write(2, 8, 'CORE', cell_format)

        worksheet.write(2, 9, 'QASTATUS', cell_format)
        worksheet.write(2, 10, 'STEP', cell_format)
        worksheet.write(2, 11, 'STEP STATUS', cell_format)
        worksheet.write(2, 12, 'STEP ERROR', cell_format)

        cell_format_data = book.add_format(
            {'font_size': 12, 'text_wrap': True, 'align': 'center', 'border': 1, 'border_color': 'black'})
        cell_format_data_ios = book.add_format(
            {'font_size': 12, 'text_wrap': True, 'align': 'center', 'bg_color': '#65b4ba', 'border': 1,
             'border_color': 'black'})
        cell_format_data_android = book.add_format(
            {'font_size': 12, 'text_wrap': True, 'align': 'center', 'bg_color': '#9dbd77', 'border': 1,
             'border_color': 'black'})

        if tipo == 'IOS':
            testrows = IosTestResults.objects.filter(runid__testname=r.testname, loginerror=True)
        else:
            testrows = AndroidTestResults.objects.filter(runid__testname=r.testname, loginerror=True)

        # testrows = IosTestResults.objects.filter( Q(runid__country=pais)& Q(runid__corecodeversion=relversion) & Q( runid__in=runnumbers) )

        for testline in testrows:
            worksheet.write(row, 0, testline.id, )
            worksheet.write(row, 1, testline.feature.name, cell_format_data)
            worksheet.write(row, 2, testline.test.test, cell_format_data)
            worksheet.write(row, 3, testline.estado, cell_format_data)
            worksheet.write(row, 4, tipo)
            worksheet.write(row, 5, testline.runid.country)
            worksheet.write(row, 6, testline.runid.testname)
            worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
            worksheet.write(row, 8, testline.runid.corecodeversion)
            worksheet.write(row, 9, testline.test.qastatus)
            worksheet.write(row, 10, '', cell_format_data_ios)

            worksheet.write(row, 11, testline.estado, cell_format_data_ios)
            worksheet.write(row, 12, testline.loginerrortext, cell_format_data_ios)

            row += 1

        book.close()
        confios = ConfluenceInfo.objects.filter(country='IOSRESUME').first()
        confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUME').first()
        if filter=='MASTER':
            confios = ConfluenceInfo.objects.filter(country='IOSRESUMEMASTER').first()
            confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUMEMASTER').first()
        if tipo=='IOS':
            uploadexceltoconfl(confios.confpageid, 'IDERROR_'+r.testname)
        else:
            uploadexceltoconfl(confand.confpageid, 'IDERROR_'+r.testname)
        print("Created excel for "  + r.testname)

    return HttpResponse( status.HTTP_201_CREATED)




@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelloginerrorsaveall(request):
    tipo= request.GET.get("tipo")
    release= request.GET.get("release")
    filter= request.GET.get("filter")
    country= request.GET.get("country")
    fromdate= request.GET.get("fromdate")
    datefrom = None
    if fromdate is None or fromdate=='':
        print("All dates ")
    else:
        datefrom = datetime.strptime(fromdate, '%d/%m/%Y')
    if filter is None:
        filter='ALL'
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if tipo== 'IOS':
        if datefrom is None:
            runs = IosTestRun.objects.all()
        else:
            runs=IosTestRun.objects.filter(testdate__gt=datefrom).all()
    else:
        if datefrom is None:
            runs = AndroidTestRun.objects.all()
        else:
            runs=AndroidTestRun.objects.filter(testdate__gt=datefrom).all()

    for r in runs:
        if '_ACCOUNTS_' in r.testname:
            continue
        if '_LOANS_' in r.testname:
            continue
        if filter=='MASTER':
            if '_DEVELOP_' in r.testname:
                continue
        if release is None or release=='ALL':
            print( 'Release ' + r.corecodeversion)
        else:
            if r.corecodeversion != release:
                continue
        if country is None or country=='ALL':
            print( 'Country ' + r.country)
        else:
            if r.country != country:
                print(' Bypassing Country ' + r.country)
                continue
        tname= r.testname
        tname = tname.replace("_DEVELOP_", "_D_")
        tname = tname.replace("_JSON_", "_J_")
        print("Creating excel for " + tipo + " " + r.testname)
        book = Workbook( 'graphimages/excel/'+'LOGINERROR_'+r.testname+'.xlsx')
        cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
        cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
        cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
        cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
        worksheet = book.add_worksheet( tname)
        merge_format = book.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'fg_color': 'yellow', 'locked':True})

        worksheet.merge_range('A2:D2', 'Run: ' + r.testname, merge_format)

        # Light red fill with dark red text.
        format1 = book.add_format({'bg_color': '#FFC7CE',
                                       'font_color': '#9C0006'})

        # Light yellow fill with dark yellow text.
        format2 = book.add_format({'bg_color': '#FFEB9C',
                                       'font_color': '#9C6500'})

        # Green fill with dark green text.
        format3 = book.add_format({'bg_color': '#C6EFCE',
                                       'font_color': '#006100'})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'failed',
                                               'format': format1})
        worksheet.conditional_format('D4:D500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'passed',
                                               'format': format3})
        worksheet.conditional_format('L4:L500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'failed',
                                               'format': format1})
        worksheet.conditional_format('L4:L500', {'type': 'text',
                                               'criteria': 'containing',
                                               'value': 'passed',
                                               'format': format3})

        row = 3
        col = 0
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1,1, 25)
        worksheet.set_column(2,2, 35)
        worksheet.set_column(3, 5, 10)
        worksheet.set_column(6, 6, 25)
        worksheet.set_column(7, 8, 10)
        worksheet.set_column(9, 25, 20)

        worksheet.write(2, 0, 'TEST ID', cell_format)
        worksheet.write(2, 1, 'FEATURE', cell_format )
        worksheet.write(2, 2, 'TEST', cell_format)
        worksheet.write(2, 3, 'TEST STATUS', cell_format)
        worksheet.write(2, 4, 'OS', cell_format)
        worksheet.write(2, 5, 'COUNTRY', cell_format)
        worksheet.write(2, 6, 'RUNID', cell_format)
        worksheet.write(2, 7, 'DATE', cell_format)
        worksheet.write(2, 8, 'CORE', cell_format)

        worksheet.write(2, 9, 'QASTATUS', cell_format)
        worksheet.write(2, 10, 'STEP', cell_format)
        worksheet.write(2, 11, 'STEP STATUS', cell_format)
        worksheet.write(2, 12, 'STEP ERROR', cell_format)


        cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
        cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
        cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

        if tipo == 'IOS':
            testrows = IosTestResults.objects.filter(   runid__testname=r.testname, loginerror=True)
        else:
            testrows = AndroidTestResults.objects.filter( runid__testname=r.testname, loginerror=True)

        # testrows = IosTestResults.objects.filter( Q(runid__country=pais)& Q(runid__corecodeversion=relversion) & Q( runid__in=runnumbers) )


        for testline in testrows:

            worksheet.write(row, 0, testline.id , )
            worksheet.write(row, 1, testline.feature.name,cell_format_data)
            worksheet.write(row, 2, testline.test.test,cell_format_data)
            worksheet.write(row, 3, testline.estado,cell_format_data)
            worksheet.write(row, 4, tipo)
            worksheet.write(row, 5, testline.runid.country)
            worksheet.write(row, 6, testline.runid.testname)
            worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
            worksheet.write(row, 8, testline.runid.corecodeversion)
            worksheet.write(row, 9, testline.test.qastatus)
            worksheet.write(row, 10, '',cell_format_data_ios)

            worksheet.write(row, 11, testline.estado,cell_format_data_ios)
            worksheet.write(row, 12, testline.loginerrortext,cell_format_data_ios)

            row += 1

        book.close()
        if len(testrows) == 0:
            continue
        confios = ConfluenceInfo.objects.filter(country='IOSRESUME').first()
        confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUME').first()
        if filter=='MASTER':
            confios = ConfluenceInfo.objects.filter(country='IOSRESUMEMASTER').first()
            confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUMEMASTER').first()
        if tipo=='IOS':
            uploadexceltoconfl(confios.confpageid, 'LOGINERROR_'+r.testname)
        else:
            uploadexceltoconfl(confand.confpageid, 'LOGINERROR_'+r.testname)
        print("Created login errors excel for "  + r.testname)

    return HttpResponse( status.HTTP_201_CREATED)









@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelrunsave(request):
    tipo= request.GET.get("tipo")
    testname= request.GET.get("testname")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    book = Workbook( 'graphimages/excel/'+testname+'.xlsx')
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet( testname)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', ' Auto : ' + testname, merge_format)

    # Light red fill with dark red text.
    format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

    # Light yellow fill with dark yellow text.
    format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

    # Green fill with dark green text.
    format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 25)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(9, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'STATUS', cell_format)
    worksheet.write(2, 4, 'OS', cell_format)
    worksheet.write(2, 5, 'COUNTRY', cell_format)
    worksheet.write(2, 6, 'RUNID', cell_format)
    worksheet.write(2, 7, 'DATE', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)

    worksheet.write(2, 9, 'QASTATUS', cell_format)
    worksheet.write(2, 10, 'STEPS OK', cell_format)
    worksheet.write(2, 11, 'STEPS KO', cell_format)
    worksheet.write(2, 12, 'STEPS SKIP', cell_format)


    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

    if tipo == 'IOS':
        testrows = IosTestResults.objects.filter( runid__testname =testname )
    else:
        testrows = AndroidTestResults.objects.filter( runid__testname=testname )

    for testline in testrows:

        worksheet.write(row, 0, testline.test.id, )
        worksheet.write(row, 1, testline.feature.name,cell_format_data)
        worksheet.write(row, 2, testline.test.test,cell_format_data)
        worksheet.write(row, 3, testline.estado,cell_format_data)
        worksheet.write(row, 4, tipo)
        worksheet.write(row, 5, testline.country)
        worksheet.write(row, 6, testline.runid.testname)
        worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
        worksheet.write(row, 8, testline.runid.corecodeversion)
        worksheet.write(row, 9, testline.test.qastatus)
        worksheet.write(row, 10, testline.stepsok,cell_format_data_ios)
        worksheet.write(row, 11, testline.stepsko,cell_format_data_ios)
        worksheet.write(row, 12, testline.stepsskipped,cell_format_data_ios)


        row += 1

    book.close()
    confios = ConfluenceInfo.objects.filter(country='IOSRESUME').first()
    confand = ConfluenceInfo.objects.filter(country='ANDROIDRESUME').first()
    if tipo=='IOS':
        uploadexceltoconfl(confios.confpageid, testname)
    else:
        uploadexceltoconfl(confand.confpageid, testname)


    return HttpResponse( status.HTTP_201_CREATED)



@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createexcelrunlogs(request):
    tipo= request.GET.get("tipo")
    testname= request.GET.get("testname")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)

    book = Workbook( 'graphimages/excel/'+testname+'testlogs.xlsx')
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet( 'TestLOGs1')
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', ' Auto : ' + testname, merge_format)

    # Light red fill with dark red text.
    format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

    # Light yellow fill with dark yellow text.
    format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

    # Green fill with dark green text.
    format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})



    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 25)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 6, 25)
    worksheet.set_column(7, 8, 10)
    worksheet.set_column(9, 25, 20)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'STATUS', cell_format)
    worksheet.write(2, 4, 'OS', cell_format)
    worksheet.write(2, 5, 'COUNTRY', cell_format)
    worksheet.write(2, 6, 'RUNID', cell_format)
    worksheet.write(2, 7, 'DATE', cell_format)
    worksheet.write(2, 8, 'CORE', cell_format)

    worksheet.write(2, 9, 'QASTATUS', cell_format)
    worksheet.write(2, 10, 'STEPS OK', cell_format)
    worksheet.write(2, 11, 'STEPS KO', cell_format)
    worksheet.write(2, 12, 'STEPS SKIP', cell_format)


    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

    if tipo == 'IOS':
        testrows = IosTestResults.objects.filter( runid__testname =testname )
    else:
        testrows = AndroidTestResults.objects.filter( runid__testname=testname )

    for testline in testrows:
        worksheet.set_row(row, None, None, {'level': 0})
        worksheet.write(row, 0, testline.test.id,cell_format_data )
        worksheet.write(row, 1, testline.feature.name,cell_format_data)
        worksheet.write(row, 2, testline.test.test,cell_format_data)
        worksheet.write(row, 3, testline.estado,cell_format_data)
        worksheet.write(row, 4, tipo)
        worksheet.write(row, 5, testline.country)
        worksheet.write(row, 6, testline.runid.testname)
        worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
        worksheet.write(row, 8, testline.runid.corecodeversion)
        worksheet.write(row, 9, testline.test.qastatus)
        worksheet.write(row, 10, testline.stepsok,cell_format_data_ios)
        worksheet.write(row, 11, testline.stepsko,cell_format_data_ios)
        worksheet.write(row, 12, testline.stepsskipped,cell_format_data_ios)
        row = row + 1
        if  row == 10:
            worksheet.set_row(row, None, None, {'level': 1})
            worksheet.write(row, 0, testline.test.id, )
            worksheet.write(row, 1, testline.feature.name,cell_format_data)
            worksheet.write(row, 2, testline.test.test,cell_format_data)
            worksheet.write(row, 3, testline.estado,cell_format_data)
            worksheet.write(row, 4, tipo)
            worksheet.write(row, 5, testline.country)
            worksheet.write(row, 6, testline.runid.testname)
            worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
            worksheet.write(row, 8, testline.runid.corecodeversion)
            worksheet.write(row, 9, testline.test.qastatus)
            worksheet.write(row, 10, testline.stepsok,cell_format_data_ios)
            worksheet.write(row, 11, testline.stepsko,cell_format_data_ios)
            worksheet.write(row, 12, testline.stepsskipped,cell_format_data_ios)
            worksheet.write(row, 13, '1111111')
            worksheet.write(row, 14, '2222222')
            row = row + 1
            worksheet.set_row(row, None, None, {'level': 2})
            worksheet.write(row, 0, testline.test.id, )
            worksheet.write(row, 1, testline.feature.name,cell_format_data)
            worksheet.write(row, 2, testline.test.test,cell_format_data)
            worksheet.write(row, 3, testline.estado,cell_format_data)
            worksheet.write(row, 4, tipo)
            worksheet.write(row, 5, testline.country)
            worksheet.write(row, 6, testline.runid.testname)
            worksheet.write(row, 7, testline.runid.testdate.strftime("%d/%m/%Y"))
            worksheet.write(row, 8, testline.runid.corecodeversion)
            worksheet.write(row, 9, testline.test.qastatus)
            worksheet.write(row, 10, testline.stepsok,cell_format_data_ios)
            worksheet.write(row, 11, testline.stepsko,cell_format_data_ios)
            worksheet.write(row, 12, testline.stepsskipped,cell_format_data_ios)
            worksheet.write(row, 13, '1111111')
            worksheet.write(row, 14, '2222222')
            row = row + 1
    book.close()
    return HttpResponse( status.HTTP_201_CREATED)





@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def createqaexcellogs(request):
    pais= request.GET.get("country")
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"
    book = Workbook(response, {'in_memory': True})
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_wrap = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'gray','border':1, 'border_color':'black'})
    cell_format_wrap.set_text_wrap()
    cell_format_wrap2 = book.add_format({'bold': True, 'font_color': 'black', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'yellow','border':1, 'border_color':'black'})
    cell_format_wrap2.set_text_wrap()

    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet = book.add_worksheet( 'AUTO'+ '-')
    worksheet.merge_range('A2:D2', 'Test Plan Definition', merge_format)
   # resx = serializers.serialize( 'json', IosTestResultsCert.objects.filter(country=pais).all())
    resx = TestsQA.objects.all()
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1, 1, 30)
    worksheet.set_column(2, 2, 45)
    worksheet.set_column(3, 4, 20)
    worksheet.set_column(5, 5, 0)
    worksheet.set_column(6, 7, 20)
    worksheet.set_column(8, 8, 40)

    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format)
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'BLOCK', cell_format)
    worksheet.write(2, 4, 'QA STATUS', cell_format)
    worksheet.write(2, 5, '', cell_format)
    worksheet.write(2, 6, 'Background', cell_format)
    worksheet.write(2, 7, 'CREATION DATE', cell_format)
    worksheet.write(2, 8, 'TEST CASE', cell_format)
    worksheet.write(2, 9, 'TEST CASE TYPE', cell_format)
    worksheet.write(2, 10, 'STEPS', cell_format)
    for rx in resx:
        accessibilityid = ''
        worksheet.set_row(row, None, None, {'level': 0})

        worksheet.write(row, 0, rx.id )
        worksheet.write(row, 1, rx.featureid.name, cell_format_wrap )
        worksheet.write(row, 2, rx.test, cell_format_wrap )
        worksheet.write(row, 3, rx.featureid.module )
        worksheet.write(row, 4, rx.qastatus )
        worksheet.write(row, 5, rx.comments )
        worksheet.write(row, 6, rx.background )

        worksheet.write(row, 7, rx.creationdate.strftime("%d/%m/%Y" ))
        row += 1
        logs = LogTestDefinition.objects.filter(testid_id=rx.id).all()
        if logs is not None:
            for ltest in logs:
                worksheet.set_row(row, None, None, {'level': 1, 'hidden': True})
                worksheet.write(row, 0, rx.testqaid)
                worksheet.write(row, 1, rx.featureid.name, cell_format_wrap2)
                worksheet.write(row, 2, rx.test, cell_format_wrap2)
                worksheet.write(row, 3, rx.featureid.module)
                worksheet.write(row, 4, rx.qastatus)
                worksheet.write(row, 5, rx.comments)
                worksheet.write(row, 6, rx.background)
                worksheet.write(row, 7, rx.creationdate.strftime("%d/%m/%Y"))
                worksheet.write(row, 8, ltest.logtestname , cell_format_wrap)
                worksheet.write(row, 9, ltest.logtesttype )
                worksheet.write(row, 10, ltest.numberofsteps )
                row = row + 1

    book.close()

    return response


