from dataclasses import dataclass
from typing import List


@dataclass
class Thumbnails:
    small: str
    medium: str
    large: str


@dataclass
class Image:
    width: int
    height: int
    originalurl: str
    thumbnails: Thumbnails


@dataclass
class Section:
    id: str


@dataclass
class Screen:
    id: str
    created: int
    updated: int
    tags: List[str]
    name: str
    image: Image
    section: Section
    numberofversions: int
    numberofnotes: int

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])