from dataclasses import dataclass
from typing import List, Any


@dataclass
class Color:
    r: int
    b: int
    g: int
    a: int


@dataclass
class Creator:
    id: str
    email: str
    username: str
    emotar: str
    lastseen: int


@dataclass
class Commit:
    color: Color
    author: Creator


@dataclass
class Thumbnails:
    small: str
    medium: str
    large: str


@dataclass
class ScreenVersion:
    id: str
    created: int
    creator: Creator
    commit: Commit
    imageurl: str
    thumbnails: Thumbnails
    source: str
    width: int
    height: int
    backgroundcolor: Color
    densityscale: int
    links: List[Any]

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])