from enum import Enum
from dataclasses import dataclass
from typing import Optional


class LinkedStyleguideID(Enum):
    the5f0c3f9d80d37581ecfca534 = "5f0c3f9d80d37581ecfca534"
    the5f0c401370e6d98c8633e993 = "5f0c401370e6d98c8633e993"
    the610bb8566429dc638155d02c = "610bb8566429dc638155d02c"


@dataclass
class LinkedStyleguide:
    id: str


@dataclass
class Organization:
    id: str
    name: str
    logo: str


class Platform(Enum):
    android = "android"
    ios = "ios"


class Status(Enum):
    active = "active"
    archived = "archived"


class ColorName(Enum):
    turquoise = "turquoise"


@dataclass
class Color:
    r: int
    g: int
    b: int
    a: int
    name: Optional[ColorName] = None


class WorkflowStatusID(Enum):
    the5ea2ce8e600339994639c845 = "5ea2ce8e600339994639c845"
    the5ee87a39f8827ab441ad9ee0 = "5ee87a39f8827ab441ad9ee0"


class WorkflowStatusName(Enum):
    Development = "Development"
    Finish = "Finish"


@dataclass
class WorkflowStatus:
    id: WorkflowStatusID
    name: WorkflowStatusName
    color: Color


@dataclass
class ProjectElement:
    id: str
    name: str
    thumbnail: str
    platform: Platform
    status: Status
    created: int
    updated: int
    organization: Organization
    numberofscreens: int
    numberofcomponents: int
    numberoftextstyles: int
    numberofcolors: int
    numberofmembers: int
    numberofspacingtokens: int
    workflowstatus: Optional[WorkflowStatus] = None
    linkedstyleguide: Optional[LinkedStyleguide] = None
    sceneurl: Optional[str] = None
    description: Optional[str] = None

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])