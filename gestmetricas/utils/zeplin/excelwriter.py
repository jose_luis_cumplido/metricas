import xlsxwriter
from django.http import HttpResponse
from xlsxwriter.workbook import Workbook

def your_view(request):
    # your view logic here

    # create the HttpResponse object ...
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = "attachment; filename=test.xlsx"


    book = Workbook(response, {'in_memory': True})
    sheet = book.add_worksheet('test')



    sheet.write(0, 0, 'Hello, world!')
    book.close()

    return response
