from enum import Enum
from dataclasses import dataclass
from typing import List, Optional, Any


class Format(Enum):
    pdf = "pdf"
    png = "png"
    svg = "svg"


@dataclass
class Content:
    url: str
    format: Format
    density: float


@dataclass
class Asset:
    layersourceid: str
    layername: str
    displayname: str
    contents: List[Content]


@dataclass
class Color:
    r: int
    b: int
    g: int
    a: float


@dataclass
class Creator:
    id: str
    email: str
    username: str
    emotar: str
    lastseen: int


@dataclass
class Commit:
    color: Color
    author: Creator


@dataclass
class Vertical:
    gutterwidth: float
    columnwidth: int
    numberofcols: int
    guttersonoutside: bool


@dataclass
class Grid:
    verticaloffset: int
    horizontaloffset: int
    vertical: Vertical


class BlendMode(Enum):
    multiply = "multiply"
    normal = "normal"


class FillType(Enum):
    color = "color"


@dataclass
class Fill:
    type: FillType
    color: Color
    opacity: int
    blendmode: BlendMode


class Position(Enum):
    center = "center"
    inside = "inside"


@dataclass
class Border:
    position: Position
    thickness: float
    fill: Fill


@dataclass
class Absolute:
    x: float
    y: float


@dataclass
class Rect:
    y: float
    x: float
    width: int
    height: float
    absolute: Absolute


@dataclass
class Range:
    location: int
    length: int


class FontFamily(Enum):
    SantanderHeadline = "SantanderHeadline"
    SantanderText = "SantanderText"


class PostscriptName(Enum):
    SantanderHeadlineBold = "SantanderHeadline-Bold"
    SantanderTextBold = "SantanderText-Bold"
    SantanderTextLight = "SantanderText-Light"
    SantanderTextRegular = "SantanderText-Regular"


@dataclass
class Style:
    postscriptname: PostscriptName
    fontfamily: FontFamily
    fontsize: int
    fontweight: int
    fontstyle: BlendMode
    fontstretch: int
    color: Color
    lineheight: Optional[int] = None
    letterspacing: Optional[float] = None
    textalign: Optional[str] = None


@dataclass
class TextStyle:
    range: Range
    style: Style


class LayerType(Enum):
    group = "group"
    shape = "shape"
    text = "text"


@dataclass
class TentacledLayer:
    id: str
    sourceid: str
    type: LayerType
    name: str
    rect: Rect
    fills: List[Fill]
    borders: List[Border]
    shadows: List[Any]
    opacity: float
    blendmode: BlendMode
    borderradius: float
    rotation: int
    exportable: bool
    layers: Optional[List['TentacledLayer']] = None
    content: Optional[str] = None
    textstyles: Optional[List[TextStyle]] = None


@dataclass
class Shadow:
    type: str
    offsetx: int
    offsety: int
    blurradius: int
    spread: int
    color: Color


@dataclass
class FluffyLayer:
    id: str
    sourceid: str
    type: LayerType
    name: str
    rect: Rect
    fills: List[Fill]
    borders: List[Border]
    shadows: List[Shadow]
    opacity: float
    blendmode: BlendMode
    borderradius: int
    rotation: int
    exportable: bool
    layers: Optional[List[TentacledLayer]] = None
    componentname: Optional[str] = None
    content: Optional[str] = None
    textstyles: Optional[List[TextStyle]] = None


@dataclass
class PurpleLayer:
    id: str
    sourceid: str
    type: LayerType
    name: str
    rect: Rect
    fills: List[Fill]
    borders: List[Border]
    shadows: List[Shadow]
    opacity: int
    blendmode: BlendMode
    borderradius: int
    rotation: int
    exportable: bool
    layers: Optional[List[FluffyLayer]] = None
    content: Optional[str] = None
    textstyles: Optional[List[TextStyle]] = None


@dataclass
class ScreenVersionsLayer:
    id: str
    sourceid: str
    type: LayerType
    name: str
    rect: Rect
    fills: List[Fill]
    borders: List[Any]
    shadows: List[Any]
    opacity: int
    blendmode: BlendMode
    borderradius: float
    rotation: int
    exportable: bool
    layers: Optional[List[PurpleLayer]] = None
    content: Optional[str] = None
    textstyles: Optional[List[TextStyle]] = None


@dataclass
class Thumbnails:
    small: str
    medium: str
    large: str


@dataclass
class ScreenComponents:
    id: str
    created: int
    creator: Creator
    commit: Commit
    imageurl: str
    thumbnails: Thumbnails
    source: str
    width: int
    height: int
    backgroundcolor: Color
    densityscale: int
    links: List[Any]
    layers: List[ScreenVersionsLayer]
    assets: List[Asset]
    grid: Grid

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])