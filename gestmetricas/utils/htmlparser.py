import datetime
import re
from bs4 import BeautifulSoup

from gestmetricas.models import StepsDefinition, TestsQA, IosTestRun, StepsResultsIos, IosTestResults, Feature, \
    AndroidTestRun, AndroidTestResults, StepsResults


def createsteps():
    with open("Spain_IOS.html", encoding='ISO-8859-1') as fp:
        soup = BeautifulSoup(fp, 'lxml')
        entradas = soup.find_all('li', {'class': 'level test'})
        for i, entrada in enumerate(entradas):
            # Con el método "getText()" no nos devuelve el HTML
            titulo = entrada.find('span').find(text=True, recursive=False)
            # Sino llamamos al método "getText()" nos devuelve también el HTML
            estado = entrada.find('em', {'class': 'status'}).getText()
            tiempo = entrada.find('em', {'class': 'time'}).getText()

            # Imprimo el Título, Autor y Fecha de las entradas
            print ("%d - %s  |  %s  |  %s" % (i + 1, titulo, estado, tiempo))
            pasos = entrada.find('ul').find_all('li')
            pasosdict=[]
            lastindex = 0
            for j, paso in enumerate(pasos):

                nombre = paso.find('span').find(text=True, recursive=False)
                if nombre is None:
                    continue
                elif nombre.startswith('#'):
                    continue
                elif len(nombre.strip()) == 0:
                    continue
                elif nombre.startswith('Given '):
                    pasosdict.append({'type': 'INIT', 'number':j, 'nombre' : nombre[len('Given '):]})
                    #print (nombre)
                elif nombre.startswith('And ') or nombre.startswith('and '):
                    pasosdict.append({'type': 'AND', 'number':j, 'nombre' : nombre[len('And '):]})
                    #print('-----' +nombre)
                    lastindex = j
                elif nombre.startswith('Then ') or nombre.startswith('then '):
                    pasosdict.append({'type': 'THEN', 'number':j, 'nombre' : nombre[len('Then '):]})
                    #print('-----' +nombre)
                else:
                    if len(pasosdict) > 0:
                        curr = next((x for x in pasosdict if x["number"] == lastindex), None)
                        if curr is not None:
                            curr["nombre"] = curr["nombre"] + ' ' + nombre

            for z in pasosdict:
                print( z["type"]+ '  ' + z["nombre"] + '  [' +str( z["number"])+']')
                stepsdef = StepsDefinition.objects.filter( stepname= z["nombre"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition( stepname= z["nombre"], steptype= z["type"])
                    stepsdef.save()


def createstepsfromhtml( htmlstr):
    try:
        soup = BeautifulSoup(htmlstr, 'lxml')
        entradas = soup.find_all('li', {'class': 'level test'})
        total = 0
        for i, entrada in enumerate(entradas):
            # Con el método "getText()" no nos devuelve el HTML
            titulo = entrada.find('span').find(text=True, recursive=False)
            # Sino llamamos al método "getText()" nos devuelve también el HTML
            estadotest = entrada.find('em', {'class': 'status'}).getText()
            tiempo = entrada.find('em', {'class': 'time'}).getText()

            # Imprimo el Título, Autor y Fecha de las entradas
            print ("%d - %s  |  %s  |  %s" % (i + 1, titulo, estadotest, tiempo))
            pasos = entrada.find('ul').find_all('li')
            pasosdict=[]
            lastindex = 0
            for j, paso in enumerate(pasos):

                nombre = paso.find('span').find(text=True, recursive=False)
                if nombre is not None:
                    nombre = nombre.strip()
                if nombre is None:
                    continue
                elif nombre.startswith('#'):
                    continue
                elif nombre.startswith('WARNING'):
                    continue
                elif nombre.startswith('Scenario'):
                    continue
                elif len(nombre.strip()) == 0:
                    continue
                elif nombre.startswith('Given '):
                    pasosdict.append({'type': 'INIT', 'number':j, 'nombre' : nombre[len('Given '):]})
                    #print (nombre)
                elif nombre.startswith('When '):
                    pasosdict.append({'type': 'WHEN', 'number':j, 'nombre' : nombre[len('When '):]})
                    #print (nombre)
                elif nombre.startswith('And ') or nombre.startswith('and '):
                    pasosdict.append({'type': 'AND', 'number':j, 'nombre' : nombre[len('And '):]})
                    #print('-----' +nombre)
                    lastindex = j
                elif nombre.startswith('Then ') or nombre.startswith('then '):
                    pasosdict.append({'type': 'THEN', 'number':j, 'nombre' : nombre[len('Then '):]})
                    #print('-----' +nombre)
                else:
                    if len(pasosdict) > 0:
                        curr = next((x for x in pasosdict if x["number"] == lastindex), None)
                        if curr is not None:
                            curr["nombre"] = curr["nombre"] + ' ' + nombre

            for z in pasosdict:
                print( z["type"]+ '  ' + z["nombre"] + '  [' +str( z["number"])+']')
                stepsdef = StepsDefinition.objects.filter( stepname= z["nombre"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition( stepname= z["nombre"], steptype= z["type"])
                    stepsdef.save()
            total = total +len(pasosdict)
    except Exception as e:
        print(e)
        return -1


    return total




def createstepsfromhtml2( htmlstr):
    try:
        soup = BeautifulSoup(htmlstr, 'lxml')
        entradas = soup.find_all('li', {'class': 'level test'})
        total = 0
        for i, entrada in enumerate(entradas):
            # Con el método "getText()" no nos devuelve el HTML
            titulo = entrada.find('span').find(text=True, recursive=False)
            # Sino llamamos al método "getText()" nos devuelve también el HTML
            estadotest = entrada.find('em', {'class': 'status'}).getText()
            tiempo = entrada.find('em', {'class': 'time'}).getText()

            # Imprimo el Título, Autor y Fecha de las entradas
            print ("%d - %s  |  %s  |  %s" % (i + 1, titulo, estadotest, tiempo))
            pasos = entrada.find('ul').find_all('li')
            pasosdict=[]
            lastindex = 0
            insubsteps = False
            for j, paso in enumerate(pasos):

                nombre = paso.find('span').find(text=True, recursive=False)
                if nombre is not None:
                    nombre = nombre.strip()
                if nombre is None:
                    continue
                elif nombre.startswith('#'):
                    continue
                elif nombre.startswith('WARNING'):
                    continue
                elif nombre.startswith('Scenario'):
                    continue
                elif len(nombre.strip()) == 0:
                    continue
                elif nombre.startswith('Given '):
                    pasosdict.append({'type': 'INIT', 'number':j, 'nombre' : nombre[len('Given '):]})
                    #print (nombre)
                elif nombre.startswith('When '):
                    pasosdict.append({'type': 'WHEN', 'number':j, 'nombre' : nombre[len('When '):]})
                    #print (nombre)
                elif nombre.startswith('And ') or nombre.startswith('and '):
                    pasosdict.append({'type': 'AND', 'number':j, 'nombre' : nombre[len('And '):]})
                    if insubsteps:
                        print("End suubsteps")
                        insubsteps = False

                    #print('-----' +nombre)
                    lastindex = j
                elif nombre.startswith('Then ') or nombre.startswith('then '):
                    pasosdict.append({'type': 'THEN', 'number':j, 'nombre' : nombre[len('Then '):]})
                    #print('-----' +nombre)
                elif nombre.startswith('SubStep: ') or nombre.startswith('sub '):
                    insubsteps = True
                    pasosdict.append({'type': 'SUBSTEP', 'number':j, 'nombre' : nombre[len('SubStep: '):]})
                    #print('-----' +nombre)
                else:
                    if len(pasosdict) > 0:
                        curr = next((x for x in pasosdict if x["number"] == lastindex), None)
                        if curr is not None:
                            curr["nombre"] = curr["nombre"] + ' ' + nombre

            for z in pasosdict:
                print( z["type"]+ '  ' + z["nombre"] + '  [' +str( z["number"])+']')
                stepsdef = StepsDefinition.objects.filter( stepname= z["nombre"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition( stepname= z["nombre"], steptype= z["type"])
                    stepsdef.save()
            total = total +len(pasosdict)
    except Exception as e:
        print(e)
        return -1


    return total




def createrunfromhtml( htmlstr, target, country):
    try:
        soup = BeautifulSoup(htmlstr, 'lxml')
        tests = soup.find_all('li', {'class': 'level test'})
        rundata = soup.find('h1')
        strpassed = rundata.find('span', {'class': 'passed'})
        strtotal = rundata.find('span', {'class': 'total'})
        strerror = rundata.find('span', {'class': 'error'})
        strignored = rundata.find('span', {'class': 'ignored'})
        strfailed = rundata.find('span', {'class': 'failed'})
        numtotal = '0'
        numpassed = '0'
        numerror = '0'
        numignored = '0'
        numfailed = '0'
        if strpassed is not None:
            numpassed = re.findall("\d+", strpassed.contents[0])[0]
        if strtotal is not None:
            numtotal = re.findall("\d+", strtotal.contents[0])[0]
        if strerror is not None:
            numerror = re.findall("\d+", strerror.contents[0])[0]
        if strignored is not None:
            numignored = re.findall("\d+", strignored.contents[0])[0]
        if strfailed is not None:
            numfailed = re.findall("\d+", strfailed.contents[0])[0]

        for t in tests:
            titulo = t.find('span').find(text=True, recursive=False)
            titulo = titulo.strip()
            status = t.find('em', {'class':'status'}).find(text=True)
            print( titulo + '    '+status)
            pasos = t.find('ul').find_all('li')
            featureFound = False
            rundateFound = False
            rounds = 0
            for p in pasos:
                ent =p.find('span', {'class':'stderr'})
                if ent is not None:
                    rundate = ent.text
                    print (rundate)
                    rundateparts = rundate.split('.M.')
                    rundate = rundateparts[0]
                    datetime_object = datetime.datetime.strptime(rundate +'M', '%b %d, %Y %I:%M:%S %p')
                    dt = datetime_object.replace(hour=0, minute=0, second=0, microsecond=0)
                    rundateFound = True
                if rundateFound is False:
                    continue
                ent2 = p.find('span', {'class': 'stdout'})
                if ent2 is not None:
                    rounds = rounds + 1
                    if rounds > 200:
                        scen = ""
                        scenario=''
                        featureFound = True
                    text1 = ent2.text
                    backg = ''
                    if text1 is not None and text1.startswith("Feature:"):
                        print (text1)
                        if "Background:" in text1:
                            parts = text1.split("Background:")
                            scen = parts[0]
                            backg = parts[1]

                        else:
                            scen = text1
                        scenario = ''
                        scen = scen.replace("Feature: ", "")
                        if "Scenario:" in scen:
                            scenparts = scen.split("Scenario:")
                            scen = scenparts[0]
                            scenario = scenparts[1]
                        featureFound = True
                        feat = Feature.objects.filter(name=scen).first()

                        if feat is None:
                            featureid = scen.lower()
                            featureid = featureid.replace(" ","-")
                            feat = Feature(name=scen, featureid=featureid)
                            feat.save()
                if rundateFound is True and featureFound is True:
                    if "." in titulo:
                        tmp = titulo.split(".")
                        titulo = tmp[1]
                    titulo = titulo.strip()
                    testname = TestsQA.objects.filter(test=titulo).first()
                    if testname is None:
                        feat = Feature.objects.filter(name=tmp[0]).first()
                        if feat is None:
                            featureid = tmp[0].lower()
                            featureid = featureid.replace(" ", "-")
                            feat = Feature(name=tmp[0], featureid=featureid)
                            feat.save()
                        testname = TestsQA( test=titulo, testqaid=1, feature=feat.name, background=backg,
                                            scenario=scenario, comments='', fromlocalrun=True, featureid=feat,
                                            qastatus='')
                        testname.save()
                    else:
                        testname.fromlocalrun = True
                        testname.save()
                    testrunname = 'IOS_'+target+'_' +str( dt.year)+ str(dt.month)+str(dt.day)
                    testrun = IosTestRun.objects.filter(testname=testrunname).first()

                    if testrun is None:
                        testrun = IosTestRun( testdate=dt, testname=testrunname, teststotal=numtotal, testsok=numpassed,
                                              testsko=numfailed, testserror=numerror, testsskipped=numignored, country=country)
                        testrun.save()
                    else:
                        testrun.teststotal =int( numtotal)
                        testrun.testsok=int( numpassed)
                        testrun.testsko=int( numfailed)
                        testrun.testsskipped=int( numignored)
                        testrun.testserror=int( numerror)
                        testrun.country=country
                        testrun.save()
                    testresults = IosTestResults.objects.filter(test=testname, runid=testrun).first()
                    if testresults is None:
                        testresults = IosTestResults( runid=testrun, test=testname, estado=status, country=country)
                        testresults.save()
                    break

        entradas = soup.find_all('li', {'class': 'level test'})
        for i, entrada in enumerate(entradas):
            # Con el método "getText()" no nos devuelve el HTML
            titulo = entrada.find('span').find(text=True, recursive=False)
            # Sino llamamos al método "getText()" nos devuelve también el HTML
            testname = TestsQA.objects.filter(test=titulo).first()
            estadotest = entrada.find('em', {'class': 'status'}).getText()
            tiempo = entrada.find('em', {'class': 'time'}).getText()

            print ("%d - %s  |  %s  |  %s" % (i + 1, titulo, estadotest, tiempo))
            pasos = entrada.find('ul').find_all('li')
            pasosdict=[]
            lastindex = 0
            for j, paso in enumerate(pasos):
                estadostep="passed"
                haserror = paso.find('span', {'class': 'stderr'})
                if haserror is not None:
                    estadostep="failed"
                nombre = paso.find('span').find(text=True, recursive=False)
                if nombre is not None:
                    nombre = nombre.strip()
                if nombre is None:
                    continue
                elif nombre.startswith('#'):
                    continue
                elif nombre.startswith('WARNING'):
                    lastent= pasosdict[-1]
                    allrows = paso.find('span').contents
                    wrn = ''
                    for r in allrows:
                        wrn = wrn + str(r)
                    #wrn = paso.find('span').find(text=True, recursive=True)
                    lastent["warning"] = wrn
                    continue
                elif nombre.startswith('Scenario'):
                    continue
                elif len(nombre.strip()) == 0:
                    continue
                elif nombre.startswith('Given '):
                    pasosdict.append({'type': 'INIT', 'number':j, 'nombre' : nombre[len('Given '):]})
                    #print (nombre)
                elif nombre.startswith('When '):
                    pasosdict.append({'type': 'WHEN', 'number': j, 'nombre': nombre[len('When '):]})
                    # print (nombre)
                elif nombre.startswith('And ') or nombre.startswith('and '):
                    pasosdict.append({'type': 'AND', 'number':j, 'nombre' : nombre[len('And '):]})
                    #print('-----' +nombre)
                    lastindex = j
                elif nombre.startswith('Then ') or nombre.startswith('then '):
                    pasosdict.append({'type': 'THEN', 'number':j, 'nombre' : nombre[len('Then '):]})
                    #print('-----' +nombre)
                else:
                    if len(pasosdict) > 0:
                        curr = next((x for x in pasosdict if x["number"] == lastindex), None)
                        if curr is not None:
                            curr["nombre"] = curr["nombre"] + ' ' + nombre
                            curr["estado"] = estadostep

            for z in pasosdict:
                if  "estado" not in z:
                    z["estado"] = "passed"
                print( z["type"]+ '  ' + z["nombre"] + '  [' +str( z["number"])+']' + '  ' +z["estado"])

                stepsdef = StepsDefinition.objects.filter( stepname= z["nombre"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition( stepname= z["nombre"], steptype= z["type"])
                    stepsdef.save()
                testresults = IosTestResults.objects.filter(test=testname, runid=testrun.id).first()
                if testresults is None:
                    testresults = IosTestResults(runid=testrun, test=testname, estado=estadotest, country=country)
                    testresults.save()
                else:
                    testresults.estado=estadotest
                    testresults.country=country
                    testresults.save()
                wrning = ''
                if 'warning' in z and z["warning"] is not None:
                    wrning = z["warning"]
                accessid = ''
                if 'accessibility id:' in wrning:
                    start = wrning.find("accessibility id:") + len("accessibility id:")
                    end = wrning.find("]<br/>")
                    substring = wrning[start:end]
                    accessid = substring.strip()
                stepexec = StepsResultsIos(testid=testname, runid=testrun, stepid=stepsdef , resultsid=testresults, haswarning=wrning,
                                           accesibilityid=accessid, estado=z["estado"])
                stepexec.save()
        return len(entradas)

    except Exception as e:
        print(e)
        return -1



def createrunfromhtml2( htmlstr, target, country):
    try:
        soup = BeautifulSoup(htmlstr, 'lxml')
        tests = soup.find_all('li', {'class': 'level test'})
        tests2 = soup.find_all('li', {'class': 'level test failed open'})
        rundata = soup.find('h1')
        strpassed = rundata.find('span', {'class': 'passed'})
        strtotal = rundata.find('span', {'class': 'total'})
        strerror = rundata.find('span', {'class': 'error'})
        strignored = rundata.find('span', {'class': 'ignored'})
        strfailed = rundata.find('span', {'class': 'failed'})
        numtotal = '0'
        numpassed = '0'
        numerror = '0'
        numignored = '0'
        numfailed = '0'
        if strpassed is not None:
            numpassed = re.findall("\d+", strpassed.contents[0])[0]
        if strtotal is not None:
            numtotal = re.findall("\d+", strtotal.contents[0])[0]
        if strerror is not None:
            numerror = re.findall("\d+", strerror.contents[0])[0]
        if strignored is not None:
            numignored = re.findall("\d+", strignored.contents[0])[0]
        if strfailed is not None:
            numfailed = re.findall("\d+", strfailed.contents[0])[0]
        for xx in tests2:
            tests.append(xx)
        for t in tests:
            titulo = t.find('span').find(text=True, recursive=False)
            titulo = titulo.strip()
            status = t.find('em', {'class':'status'}).find(text=True)
            print( titulo + '    '+status)
            pasos = t.find('ul').find_all('li')
            featureFound = False
            rundateFound = False
            rounds = 0
            for p in pasos:
                ent =p.find('span', {'class':'stderr'})
                if ent is not None:
                    rundate = ent.text
                    print (rundate)
                    rundate =rundate.replace('A.M.', 'AM ')
                    rundate =rundate.replace('P.M.', 'PM ')
                    rundateparts = rundate.split('M ')
                    rundate = rundateparts[0]
                    if rundate is not None and len(rundate) > 0:
                        try:
                            datetime_object = datetime.datetime.strptime(rundate +'M', '%b %d, %Y %I:%M:%S %p')
                            dt = datetime_object.replace(hour=0, minute=0, second=0, microsecond=0)
                            rundateFound = True
                        except Exception as e:
                            print(e)
                if rundateFound is False:
                    continue
                ent2 = p.find('span', {'class': 'stdout'})
                if ent2 is not None:
                    rounds = rounds + 1
                    if rounds > 100:
                        featureFound = True
                        scenario=''
                    text1 = ent2.text
                    backg = ''
                    if text1 is not None  and "Feature:" in text1:
                        if text1.startswith("Feature:"):
                            print(text1)
                        else:
                            texttmp = text1.split("Feature:")
                            text1 = texttmp[1].strip()
                            print (text1)
                        if "Background:" in text1:
                            parts = text1.split("Background:")
                            scen = parts[0]
                            backg = parts[1]

                        else:
                            scen = text1
                        scenario = ''
                        scen = scen.replace("Feature: ", "")
                        scen = scen.replace('  The aim of these scenarios is to test the Public Menu and all its functionalities on all login no remember screens.  ', '')
                        scen = scen.replace('  The aim of these scenarios is to test the Public Menu and all its functionalities on all login remember screens.  ', '')
                        if "Scenario:" in scen:
                            scenparts = scen.split("Scenario:")
                            scen = scenparts[0]
                            scenario = scenparts[1]
                        featureFound = True
                        feat = Feature.objects.filter(name=scen).first()

                        if feat is None:
                            featureid = scen.lower()
                            featureid = featureid.replace(" ","-")
                            feat = Feature(name=scen, featureid=featureid)
                            feat.save()
                if rundateFound is True and featureFound is True:
                    if "." in titulo:
                        tmp = titulo.split(".")
                        titulo = tmp[1]
                    titulo = titulo.strip()
                    testname = TestsQA.objects.filter(test=titulo, feature=tmp[0].strip()).first()
                    if testname is None:
                        feat = Feature.objects.filter(name=tmp[0]).first()
                        if feat is None:
                            featureid = tmp[0].lower()
                            featureid = featureid.replace(" ", "-")
                            feat = Feature(name=tmp[0], featureid=featureid)
                            feat.save()
                        testname = TestsQA( test=titulo, testqaid=1, feature=feat.name, background=backg,
                                            scenario=scenario, comments='', fromlocalrun=True, featureid=feat,
                                            qastatus='')
                        testname.save()
                    else:
                        testname.fromlocalrun = True
                        testname.save()
                    testrunname = 'IOS_'+target+'_' + country +'_'+str( dt.year)+ str(dt.month)+str(dt.day)
                    testrun = IosTestRun.objects.filter(testname=testrunname).first()

                    if testrun is None:
                        testrun = IosTestRun( testdate=dt, testname=testrunname, teststotal=numtotal, testsok=numpassed,
                                              testsko=numfailed, testserror=numerror, testsskipped=numignored, country=country)
                        testrun.save()
                    else:
                        testrun.teststotal =int( numtotal)
                        testrun.testsok=int( numpassed)
                        testrun.testsko=int( numfailed)
                        testrun.testsskipped=int( numignored)
                        testrun.testserror=int( numerror)
                        testrun.country=country
                        testrun.save()
                    testresults = IosTestResults.objects.filter(test=testname, runid=testrun).first()
                    if testresults is None:
                        testresults = IosTestResults( runid=testrun, test=testname, estado=status, country=country, feature=testname.featureid)
                        testresults.save()
                    else:
                        if testresults.feature_id != testname.featureid_id:
                            testresults.feature=testname.featureid
                            testresults.save()
                    break


        entradas = soup.find_all('li', {'class': 'level test'})
        for i, entrada in enumerate(entradas):
            # Con el método "getText()" no nos devuelve el HTML
            titulo = entrada.find('span').find(text=True, recursive=False)
            if "." in titulo:
                tmp = titulo.split(".")
                titulo = tmp[1]
            # Sino llamamos al método "getText()" nos devuelve también el HTML
            testname = TestsQA.objects.filter(test=titulo).first()
            estadotest = entrada.find('em', {'class': 'status'}).getText()
            tiempo = entrada.find('em', {'class': 'time'}).getText()

            print ("%d - %s  |  %s  |  %s" % (i + 1, titulo, estadotest, tiempo))
            pasos = entrada.find('ul').find_all('li')
            pasosdict=[]
            lastindex = 0
            for j, paso in enumerate(pasos):
                estadostep="passed"
                haserror = paso.find('span', {'class': 'stderr'})
                if haserror is not None:
                    estadostep="failed"
                nombre = paso.find('span').find(text=True, recursive=False)
                if nombre is not None:
                    nombre = nombre.strip()
                if nombre is None:
                    continue
                elif nombre.startswith('#'):
                    continue
                elif nombre.startswith('WARNING'):
                    lastent= pasosdict[-1]
                    allrows = paso.find('span').contents
                    wrn = ''
                    for r in allrows:
                        wrn = wrn + str(r)
                    #wrn = paso.find('span').find(text=True, recursive=True)
                    lastent["warning"] = wrn
                    continue
                elif nombre.startswith('Scenario'):
                    continue
                elif len(nombre.strip()) == 0:
                    continue
                elif nombre.startswith('Given '):
                    pasosdict.append({'type': 'INIT', 'number':j, 'nombre' : nombre[len('Given '):]})
                    #print (nombre)
                elif nombre.startswith('When '):
                    pasosdict.append({'type': 'WHEN', 'number': j, 'nombre': nombre[len('When '):]})
                    # print (nombre)
                elif nombre.startswith('And ') or nombre.startswith('and '):
                    pasosdict.append({'type': 'AND', 'number':j, 'nombre' : nombre[len('And '):]})
                    #print('-----' +nombre)
                    lastindex = j
                elif nombre.startswith('Then ') or nombre.startswith('then '):
                    pasosdict.append({'type': 'THEN', 'number':j, 'nombre' : nombre[len('Then '):]})
                    #print('-----' +nombre)
                else:
                    if len(pasosdict) > 0:
                        curr = next((x for x in pasosdict if x["number"] == lastindex), None)
                        if curr is not None:
                            curr["nombre"] = curr["nombre"] + ' ' + nombre
                            curr["estado"] = estadostep

            for z in pasosdict:
                if  "estado" not in z:
                    z["estado"] = "passed"
                print( z["type"]+ '  ' + z["nombre"] + '  [' +str( z["number"])+']' + '  ' +z["estado"])

                stepsdef = StepsDefinition.objects.filter( stepname= z["nombre"]).first()
                if stepsdef is None:
                    stepn = z["nombre"][0:380]
                    stepsdef = StepsDefinition(stepname=stepn, steptype=z["type"])
                    stepsdef.save()
                testresults = IosTestResults.objects.filter(test=testname, runid=testrun.id).first()
                if testresults is None:
                    testresults = IosTestResults(runid=testrun, test=testname, estado=estadotest, country=country)
                    testresults.save()
                else:
                    testresults.estado=estadotest
                    testresults.country=country
                    testresults.save()
                wrning = ''
                if 'warning' in z and z["warning"] is not None:
                    wrning = z["warning"]
                accessid = ''
                if 'accessibility id:' in wrning:
                    start = wrning.find("accessibility id:") + len("accessibility id:")
                    end = wrning.find("]<br/>")
                    substring = wrning[start:end]
                    accessid = substring.strip()
                stepexec = StepsResultsIos(testid=testname, runid=testrun, stepid=stepsdef , resultsid=testresults, haswarning=wrning,
                                           accesibilityid=accessid, estado=z["estado"])
                stepexec.save()
        return len(entradas)

    except Exception as e:
        print(e)
        return -1



def createrunfromhtml2android( htmlstr, target, country):
    try:
        soup = BeautifulSoup(htmlstr, 'lxml')
        tests = soup.find_all('li', {'class': 'level test'})
        tests2 = soup.find_all('li', {'class': 'level test failed open'})
        rundata = soup.find('h1')
        strpassed = rundata.find('span', {'class': 'passed'})
        strtotal = rundata.find('span', {'class': 'total'})
        strerror = rundata.find('span', {'class': 'error'})
        strignored = rundata.find('span', {'class': 'ignored'})
        strfailed = rundata.find('span', {'class': 'failed'})
        numtotal = '0'
        numpassed = '0'
        numerror = '0'
        numignored = '0'
        numfailed = '0'
        if strpassed is not None:
            numpassed = re.findall("\d+", strpassed.contents[0])[0]
        if strtotal is not None:
            numtotal = re.findall("\d+", strtotal.contents[0])[0]
        if strerror is not None:
            numerror = re.findall("\d+", strerror.contents[0])[0]
        if strignored is not None:
            numignored = re.findall("\d+", strignored.contents[0])[0]
        if strfailed is not None:
            numfailed = re.findall("\d+", strfailed.contents[0])[0]
        for xx in tests2:
            tests.append(xx)
        for t in tests:
            titulo = t.find('span').find(text=True, recursive=False)
            titulo = titulo.strip()
            status = t.find('em', {'class':'status'}).find(text=True)
            print( titulo + '    '+status)
            pasos = t.find('ul').find_all('li')
            featureFound = False
            rundateFound = False
            rounds = 0
            for p in pasos:
                ent =p.find('span', {'class':'stderr'})
                if ent is not None:
                    rundate = ent.text
                    print (rundate)
                    rundate =rundate.replace('A.M.', 'AM ')
                    rundate =rundate.replace('P.M.', 'PM ')
                    rundateparts = rundate.split('M ')
                    rundate = rundateparts[0]
                    print( "Rundate " + rundate+ '   ' )
                    if rundate is not None and len(rundate) > 0:
                        try:
                            datetime_object = datetime.datetime.strptime(rundate +'M', '%b %d, %Y %I:%M:%S %p')
                            dt = datetime_object.replace(hour=0, minute=0, second=0, microsecond=0)
                            rundateFound = True
                        except Exception as e:
                            print(e)
                if rundateFound is False:
                    continue
                ent2 = p.find('span', {'class': 'stdout'})
                if ent2 is not None:
                    rounds = rounds + 1
                    if rounds > 100:
                        featureFound = True
                        scenario=''
                    text1 = ent2.text
                    backg = ''
                    if text1 is not None  and "Feature:" in text1:
                        if text1.startswith("Feature:"):
                            print(text1)
                        else:
                            texttmp = text1.split("Feature:")
                            text1 = texttmp[1].strip()
                            print (text1)
                        if "Background:" in text1:
                            parts = text1.split("Background:")
                            scen = parts[0]
                            backg = parts[1]

                        else:
                            scen = text1
                        scenario = ''
                        scen = scen.replace("Feature: ", "")
                        if "Scenario:" in scen:
                            scenparts = scen.split("Scenario:")
                            scen = scenparts[0]
                            scenario = scenparts[1]
                        featureFound = True
                        feat = Feature.objects.filter(name=scen).first()

                        if feat is None:
                            featureid = scen.lower()
                            featureid = featureid.replace(" ","-")
                            feat = Feature(name=scen, featureid=featureid)
                            feat.save()
                if rundateFound is True and featureFound is True:
                    if "." in titulo:
                        tmp = titulo.split(".")
                        titulo = tmp[1]
                    titulo = titulo.strip()
                    testname = TestsQA.objects.filter(test=titulo, feature=tmp[0].strip()).first()
                    if testname is None:
                        feat = Feature.objects.filter(name=tmp[0]).first()
                        if feat is None:
                            featureid = tmp[0].lower()
                            featureid = featureid.replace(" ", "-")
                            feat = Feature(name=tmp[0], featureid=featureid)
                            feat.save()
                        testname = TestsQA( test=titulo, testqaid=1, feature=feat.name, background=backg,
                                            scenario=scenario, comments='', fromlocalrun=True, featureid=feat,
                                            qastatus='')
                        testname.save()
                    else:
                        testname.fromlocalrun = True
                        testname.save()
                    testrunname = 'ANDROID_'+target+'_' + country +'_'+str( dt.year)+ str(dt.month)+str(dt.day)
                    testrun = AndroidTestRun.objects.filter(testname=testrunname).first()

                    if testrun is None:
                        testrun = AndroidTestRun( testdate=dt, testname=testrunname, teststotal=numtotal, testsok=numpassed,
                                              testsko=numfailed, testserror=numerror, testsskipped=numignored, country=country)
                        testrun.save()
                    else:
                        testrun.teststotal =int( numtotal)
                        testrun.testsok=int( numpassed)
                        testrun.testsko=int( numfailed)
                        testrun.testsskipped=int( numignored)
                        testrun.testserror=int( numerror)
                        testrun.country=country
                        testrun.save()
                    testresults = AndroidTestResults.objects.filter(test=testname, runid=testrun).first()
                    if testresults is None:
                        testresults = AndroidTestResults( runid=testrun, test=testname, estado=status, country=country, feature=testname.featureid)
                        testresults.save()
                    else:
                        if testresults.feature_id != testname.featureid_id:
                            testresults.feature=testname.featureid
                            testresults.save()
                    break

        entradas = soup.find_all('li', {'class': 'level test'})
        for i, entrada in enumerate(entradas):
            # Con el método "getText()" no nos devuelve el HTML
            titulo = entrada.find('span').find(text=True, recursive=False)
            if "." in titulo:
                tmp = titulo.split(".")
                titulo = tmp[1]
            # Sino llamamos al método "getText()" nos devuelve también el HTML
            testname = TestsQA.objects.filter(test=titulo).first()
            estadotest = entrada.find('em', {'class': 'status'}).getText()
            tiempo = entrada.find('em', {'class': 'time'}).getText()

            print ("%d - %s  |  %s  |  %s" % (i + 1, titulo, estadotest, tiempo))
            pasos = entrada.find('ul').find_all('li')
            pasosdict=[]
            lastindex = 0
            for j, paso in enumerate(pasos):
                estadostep="passed"
                haserror = paso.find('span', {'class': 'stderr'})
                if haserror is not None:
                    estadostep="failed"
                nombre = paso.find('span').find(text=True, recursive=False)
                if nombre is not None:
                    nombre = nombre.strip()
                if nombre is None:
                    continue
                elif nombre.startswith('#'):
                    continue
                elif nombre.startswith('WARNING'):
                    lastent= pasosdict[-1]
                    allrows = paso.find('span').contents
                    wrn = ''
                    for r in allrows:
                        wrn = wrn + str(r)
                    #wrn = paso.find('span').find(text=True, recursive=True)
                    lastent["warning"] = wrn
                    continue
                elif nombre.startswith('Scenario'):
                    continue
                elif len(nombre.strip()) == 0:
                    continue
                elif nombre.startswith('Given '):
                    pasosdict.append({'type': 'INIT', 'number':j, 'nombre' : nombre[len('Given '):]})
                    #print (nombre)
                elif nombre.startswith('When '):
                    pasosdict.append({'type': 'WHEN', 'number': j, 'nombre': nombre[len('When '):]})
                    # print (nombre)
                elif nombre.startswith('And ') or nombre.startswith('and '):
                    pasosdict.append({'type': 'AND', 'number':j, 'nombre' : nombre[len('And '):]})
                    #print('-----' +nombre)
                    lastindex = j
                elif nombre.startswith('Then ') or nombre.startswith('then '):
                    pasosdict.append({'type': 'THEN', 'number':j, 'nombre' : nombre[len('Then '):]})
                    #print('-----' +nombre)
                else:
                    if len(pasosdict) > 0:
                        curr = next((x for x in pasosdict if x["number"] == lastindex), None)
                        if curr is not None:
                            curr["nombre"] = curr["nombre"] + ' ' + nombre
                            curr["estado"] = estadostep

            for z in pasosdict:
                if  "estado" not in z:
                    z["estado"] = "passed"
                print( z["type"]+ '  ' + z["nombre"] + '  [' +str( z["number"])+']' + '  ' +z["estado"])

                stepsdef = StepsDefinition.objects.filter( stepname= z["nombre"]).first()
                if stepsdef is None:
                    stepn= z["nombre"][0:380]
                    stepsdef = StepsDefinition( stepname= stepn, steptype= z["type"])
                    stepsdef.save()
                testresults = AndroidTestResults.objects.filter(test=testname, runid=testrun.id).first()
                if testresults is None:
                    testresults = AndroidTestResults(runid=testrun, test=testname, estado=estadotest, country=country)
                    testresults.save()
                else:
                    testresults.estado=estadotest
                    testresults.country=country
                    testresults.save()
                wrning = ''
                if 'warning' in z and z["warning"] is not None:
                    wrning = z["warning"]
                accessid = ''
                if 'accessibility id:' in wrning:
                    start = wrning.find("accessibility id:") + len("accessibility id:")
                    end = wrning.find("]<br/>")
                    substring = wrning[start:end]
                    accessid = substring.strip()
                stepexec = StepsResults(testid=testname, runid=testrun, stepid=stepsdef , resultsid=testresults, haswarning=wrning,
                                           accesibilityid=accessid, estado=z["estado"])
                stepexec.save()
        return len(entradas)

    except Exception as e:
        print(e)
        return -1


