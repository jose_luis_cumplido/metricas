
import datetime
import json
import re
from bs4 import BeautifulSoup

from gestmetricas.models import StepsDefinition, TestsQA, IosTestRun, StepsResultsIos, IosTestResults, Feature, \
    AndroidTestRun, AndroidTestResults, StepsResults, LogTestDefinition, BackgroundSt, BackgroundSteps
from gestmetricas.utils.jsonparser.cucumberparser import CucumberOutputElement


def createnewlogsfromjson( jsonstr, target, country):
    firstscenario = None
    testrun = None
    runelems = json.loads(jsonstr)
    for z in runelems:
        feature = CucumberOutputElement(z)
        a = feature.name
        numscenarios = 0
        numsteps = 0
        numstepsbck = 0
        feat = Feature.objects.filter(name=a).first()
        if feat is None:
            feat = Feature( name=a, featureid=feature.id)
            feat.save()
        print(a)

        testsqacol = TestsQA.objects.filter(feature=a).all()
        for testqa in testsqacol:
            if testqa.featureid is None:
                testqa.featureid=feat
            #    testqa.save()
                print('Updating ' + testqa.test)
            # carga de steps
        for elem in feature.elements:
            if elem['keyword'] =='Scenario':
                numscenarios = numscenarios +1
                numsteps =  numsteps +len(elem['steps'])
                print("Escenario: " + elem['name'])
            if elem['keyword'] =='Background':
                numstepsbck = len(elem['steps'])

            steps = elem['steps']
            for step in steps:
                stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition(stepname=step["name"], steptype=step["keyword"].upper())
                    stepsdef.save()

        currbackground = None
        currbackgroundname =''
        for elem in feature.elements:
            if elem['type'] == 'background' and currbackground is None:
                currbackgroundname=elem['name']
                currbackground = elem
                backgr = BackgroundSt.objects.filter(name=currbackgroundname, featureid= feat.name).first()
                if backgr is None:
                    backgr = BackgroundSt(name=currbackgroundname, featureid= feat.name)
                    backgr.save()

                for step in steps:
                    stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                    backgrstep = BackgroundSteps.objects.filter(backgroundid=backgr, stepid=stepsdef).first()
                    if backgrstep is None:
                        backgrstep = BackgroundSteps(backgroundid=backgr, stepid=stepsdef)
                        backgrstep.save()
                break

        for elem in feature.elements:
            if elem['type']=='scenario':

                curscen = TestsQA.objects.filter(test= elem['name']).first()
                if curscen is None:
                    curscen =   TestsQA(test=elem['name'], testqaid=1, feature=feature.name, background=currbackgroundname,
                            scenario='', comments='', featureid=feat,
                            qastatus='')
                    curscen.save()
                    print('Saving '+ elem['name'])
                else:
                    curscen.featureid=feat
                    curscen.background=currbackgroundname
                #    curscen.save()
                testname = elem['name']
                dur = elem['after'][0]['result']['duration']
                stat = elem['after'][0]['result']['status']
                dur = dur/1000
                contstepok=0
                contstepko=0
                contstepskipped=0
                testfinalstatus= stat
                haserrorid = False


                for step in elem['steps']:
                    wrningmsg = ''
                    errormsg =''
                    accessid = ''
                    stepstatus = step['result']['status']
                    if 'duration' in step['result']:
                        stepdur = step['result']['duration']
                    else:
                        stepdur = 0
                    if 'Test_Starts' in step['name']:
                        texttmp = step['name'].split("Test_Starts:")
                        text1 = texttmp[1].strip()
                        text1=text1.replace('"','')
                        print(text1)
                        contsteps = 0
                        ltest = LogTestDefinition.objects.filter(logtestname=text1).first()
                        if ltest is None:
                            ltest = LogTestDefinition(logtestname=text1, testid=curscen)
                            ltest.save()
                    if 'Test_Ends' in step['name']:
                        texttmp = step['name'].split("Test_Ends:")
                        text1 = texttmp[1].strip()
                        text1=text1.replace('"','')
                        print(text1)
                        ltest = LogTestDefinition.objects.filter(logtestname=text1).first()
                        if ltest is not None:
                            ltest.numberofsteps = contsteps
                            ltest.save()

    return ''

