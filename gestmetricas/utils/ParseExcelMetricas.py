import math
from datetime import datetime

import pandas as pd
import xlrd
import json

from openpyxl import Workbook, load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows

from django.contrib.auth.models import User
from django.db import IntegrityError


import logging
import unidecode

# Get an instance of a logger
from gestmetricas.models import ExcelMetricas

logger = logging.getLogger(__name__)

def preparseExcel( myfile ):
    book = xlrd.open_workbook(file_contents=myfile.read())
    df = pd.read_excel(book, engine='xlrd')
    rows = len(df)
    cols = len(df.columns)
    data = {}
    data['rows'] = rows
    data['cols'] = cols
    json_data = json.dumps(data)
    return data


def parseExcelMetricas( excelfile, importuuid ):
    book = xlrd.open_workbook(file_contents=excelfile.read())
    df1 = pd.read_excel(book, engine='xlrd')
    eventoid = importuuid


   # evt = ExcelMetricas.objects.filter(id=eventouuid).first()
    wb = load_workbook(filename=excelfile)
    ws = wb.active

  #  for r in dataframe_to_rows(df1, index=True, header=True):
   #     ws.append(r)

    df1 = df1.rename(columns=str.lower)
   # df1 = df1.applymap(lambda s: s.upper() if type(s) == str else s)
    firstCol = ['acción evento', 'acción_evento']
    df1.rename(columns={typo: 'accion_evento' for typo in firstCol}, inplace=True)
    secondCol = ['¿tiene parámetros?', '¿tiene parámetros?']
    df1.rename(columns={typo: 'tieneparametros' for typo in secondCol}, inplace=True)
    secondCol = ['comentarios ios', 'comentarios ios']
    df1.rename(columns={typo: 'comentarios_ios' for typo in secondCol}, inplace=True)
    secondCol = ['comentarios android', 'comentarios android']
    df1.rename(columns={typo: 'comentarios_android' for typo in secondCol}, inplace=True)
    secondCol = ['descripción']
    df1.rename(columns={typo: 'descripcion' for typo in secondCol}, inplace=True)
    secondCol = ['tipo_operacion.1']
    df1.rename(columns={typo: 'tipo_operacion_1' for typo in secondCol}, inplace=True)
    cols = df1.columns
    print(cols)
    fieldarray = df1.columns

    for i in df1.index:
        colorcell = ws.cell(i+1,1).font.color.rgb
        strikecell = ws.cell(i+1,1).font.strike
        if strikecell is None:
            strikecell = False
        if isinstance(colorcell, str):
            if len(colorcell) > 9 :
                colorcell='000000'
        else:
            colorcell = '000000'
        ax = dict.fromkeys(fieldarray)
        for cc in fieldarray:
            print(df1[cc][i])
            if cc == 'tieneparametros':
                if df1[cc][i] is None:
                    ax[cc] = 0
                else:
                    if math.isnan(df1[cc][i]):
                        ax[cc] = 0
                    else:
                        ax[cc] = df1[cc][i]
            else:
                ax[cc] = str(df1[cc][i])
                if ax[cc] == 'nan':
                    ax[cc] = ''
         #   isinstance(ax[cc], str)


        dt = datetime.now()
        try:
            newline= ExcelMetricas(**ax, colororiginal=colorcell,ordenoriginal=i,striken=strikecell, modificado=dt, uuidimport=eventoid)
            newline.save()

        except Exception as e:
            logger.error(e)
    return True



