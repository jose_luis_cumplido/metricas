
import pandas as pd
import json

from gestmetricas.models import TestDefinition, StatusInfo, StatusInfoPL


def parseExcelTestDef( excelname , sheetname ):
  #  xls = pd.ExcelFile(excelname)
    df1 = pd.read_excel(excelname, sheet_name = sheetname)
    df1.columns = df1.iloc[0]
    documents={}
    faqtypes =[]
    faqs = {}
    dnis = []
    dictdni = dict()
    for x in df1.index:
        i = x +1
        testid=df1['ID'][i]
        bloque=df1['Bloque'][i]
        testname=df1['Test'][i]
        automatizado=df1['Automatizado'][i]
        corelocal=df1['Core / Local'][i]
        comentario=df1['Comentario'][i]
        androidpre=df1['ID'][i]
        scenario=df1['Scenario'][i]
        tdo = TestDefinition.objects.filter(testid=testid).first()
        if tdo is None:
            tdo = TestDefinition( testid=testid, test=testname, bloque=bloque, comentario=comentario,
                                             corelocal=corelocal, automatizado=automatizado)
            tdo.save()
        else:
            tdo.automatizado=automatizado
            tdo.comentario=comentario
            tdo.save()



def parseExcelTestStatus( excelname , sheetname ):
  #  xls = pd.ExcelFile(excelname)
    df1 = pd.read_excel(excelname, sheet_name = sheetname)
    df1.columns = df1.iloc[0]

    for x in df1.index:
        i = x +1
        try:
            bloque=df1['BLOCK'][i]
        except Exception as e:
            print(e)
            return i
        testname=df1['TEST'][i]
        automatizado=df1['STATUS'][i]
        scenario=df1['SCENARIO'][i]
        comentario=df1['COMMENTS'][i]
        print(testname)
        tdo = StatusInfo.objects.filter(testid=i).first()
        if tdo is None:
            tdo = StatusInfo(  test=testname, bloque=bloque, comments=comentario,
                                         status=automatizado, scenario= scenario, testid=i)
            tdo.save()
        else:
            tdo.status=automatizado
            tdo.comments=comentario
            tdo.save()



def parseExcelTestStatusPL( excelname , sheetname ):
  #  xls = pd.ExcelFile(excelname)
    df1 = pd.read_excel(excelname, sheet_name = sheetname)
    df1.columns = df1.iloc[0]

    for x in df1.index:
        i = x +1
        try:
            bloque=df1['BLOCK'][i]
        except Exception as e:
            print(e)
            return i
        testname=df1['TEST'][i]
        automatizado=df1['STATUS CORE'][i]
        scenario=df1['SCENARIO'][i]
        comentario=df1['COMMENTS'][i]
        statuspl=df1['POLAND'][i]
        print(testname)
        tdo = StatusInfoPL.objects.filter(testid=i).first()
        if tdo is None:
            tdo = StatusInfoPL(  test=testname, bloque=bloque, comments=comentario,
                                         statuscore=automatizado, statuspl=statuspl, scenario= scenario, testid=i)
            tdo.save()
        else:
            tdo.status=automatizado
            tdo.comments=comentario
            tdo.save()
