import datetime

from jira import JIRA

from gestmetricas.models import TestsQA, JiraInfo, JiraCertificationReport


def listjiraissues2( ):
    options = {'server': 'https://sanone.atlassian.net'}
    jira = JIRA(options, basic_auth=('x851335@gruposantander.es', 'UJGw8KrPUDvStXZVDBok0F61'))

    size = 100
    initial = 0

    start = initial * size
    issues = jira.search_issues( jql_str= 'project=MOVEU & labels=AutoBUG', startAt=0 )
    if len(issues) == 0:
        return
    initial += 1
    for issue in issues:
        print('ticket-no=' + issue.key)
        print(    'IssueType='+ issue.fields.issuetype.name)
        print('Status='+ issue.fields.status.name)
        print('Summary='+ issue.fields.summary)
        print('Description='+ issue.fields.description)
        jiralabels =''
        for a in issue.fields.labels:
            jiralabels = jiralabels+ a+','
        jiracomponents =''
        for b in issue.fields.components:
            jiracomponents = jiracomponents+ b.name+','
        jiracreated = datetime.datetime.strptime(issue.fields.created, '%Y-%m-%dT%H:%M:%S.%f%z')
        jiraupdated = datetime.datetime.strptime(issue.fields.updated, '%Y-%m-%dT%H:%M:%S.%f%z')

        if '*Scenario*' in issue.fields.description:
            textparts = issue.fields.description.split('*Scenario*')
            contpart=1
            for tpart in textparts:
                if contpart == len(textparts):
                    break
                text1 = textparts[contpart].split("\n")[0]
                text1 = text1.replace('{{','')
                text1 = text1.replace('}}','')
                text1 = text1.replace(':','')

                text1 = text1.strip()
                print("TEST:  " + text1)
                testqa = TestsQA.objects.filter(test=text1).first()
                if testqa is not None:
                    issueinfo = JiraInfo.objects.filter(issueid=issue.key).first()
                    if issueinfo is None:
                        issueinfo = JiraInfo( issueid=issue.key, ticettype=issue.fields.issuetype.name, status= issue.fields.status.name,
                                              testid=testqa, summary= issue.fields.summary, labels=jiralabels,assignee=issue.fields.assignee,
                                              components=jiracomponents,creator=issue.fields.creator,jiracreated=jiracreated,jiraupdated=jiraupdated,
                                              lastupdate=datetime.datetime.now())
                        issueinfo.save()
                    else:
                        issueinfo.status = issue.fields.status.name
                        issueinfo.summary = issue.fields.summary
                        issueinfo.assignee = issue.fields.assignee
                        issueinfo.creator = issue.fields.creator
                        issueinfo.labels = jiralabels
                        issueinfo.components=jiracomponents
                        issueinfo.lastupdate=datetime.datetime.now()
                        issueinfo.jiracreated = jiracreated
                        issueinfo.jiraupdated = jiraupdated
                        issueinfo.save()
                contpart=contpart+1
            else:
                print('============ No encontrado escenario : ' + text1)
        else:
            issueinfo = JiraInfo.objects.filter(issueid=issue.key).first()
            if issueinfo is None:
                issueinfo = JiraInfo(issueid=issue.key, ticettype=issue.fields.issuetype.name,
                                     status=issue.fields.status.name,
                                     summary=issue.fields.summary, labels=jiralabels, assignee=issue.fields.assignee,
                                     components=jiracomponents,creator=issue.fields.creator,jiracreated=jiracreated,jiraupdated=jiraupdated,
                                     lastupdate=datetime.datetime.now())
                issueinfo.save()
            else:
                issueinfo.status = issue.fields.status.name
                issueinfo.summary = issue.fields.summary
                issueinfo.assignee = issue.fields.assignee
                issueinfo.creator = issue.fields.creator

                issueinfo.labels = jiralabels
                issueinfo.components = jiracomponents
                issueinfo.lastupdate = datetime.datetime.now()
                issueinfo.jiracreated=jiracreated
                issueinfo.jiraupdated=jiraupdated
                issueinfo.save()

#listjiraissues()


def makejiracertreport( releasename ):
    options = {'server': 'https://sanone.atlassian.net'}
    jira = JIRA(options, basic_auth=('x851335@gruposantander.es', 'UJGw8KrPUDvStXZVDBok0F61'))

    size = 100
    initial = 0

    start = initial * size
    issues = jira.search_issues(jql_str='project=MOVEU & affectedVersion=' + releasename, startAt=0, maxResults=100)
    if len(issues) == 0:
        return
    initial += 1
    for issue in issues:
        print('ticket-no=' + issue.key)
        print('IssueType=' + issue.fields.issuetype.name)
        print('Status=' + issue.fields.status.name)
        print('Summary=' + issue.fields.summary)
        print('LINK =' + issue.raw['self'])
        sp=''
        pt=''
        pl=''
        uk=''
        uatbug=''
        autobug=''
        hotfix=''
        bugcore=''
        ptowned=''
        hotfixcountry=''
        jiralabels = ''
        core=''
        for a in issue.fields.labels:
            jiralabels = jiralabels + a + ','
            if a == 'SP':
                sp='YES'
            if a == 'PT':
                pt='YES'
            if a == 'PL':
                pl = 'YES'
            if a == 'UK':
                uk = 'YES'
            if a == 'BugCore' :
                bugcore = 'YES'
            if  a =='Core':
                core = 'YES'
            if a == 'AutoBUG':
                autobug = 'YES'
            if a == 'UATBUG':
                uatbug = 'YES'
            if a == 'HOTFIX':
                hotfix = 'YES'
            if a == 'CountryHOTFIX':
                hotfixcountry = 'YES'
            if a == 'PT_Owned_bugs':
                ptowned = 'YES'
        prio = 'P' + issue.fields.priority.id

        jiracomponents =''
        for b in issue.fields.components:
            jiracomponents = jiracomponents+ b.name+','
        jiracreated = datetime.datetime.strptime(issue.fields.created, '%Y-%m-%dT%H:%M:%S.%f%z')
        jiraupdated = datetime.datetime.strptime(issue.fields.updated, '%Y-%m-%dT%H:%M:%S.%f%z')

        issueinfo = JiraCertificationReport.objects.filter(issueid=issue.key, reportrelease=releasename).first()
        if issueinfo is None:
            issueinfo = JiraCertificationReport(issueid=issue.key, ticettype=issue.fields.issuetype.name, reportrelease=releasename,
                                     status=issue.fields.status.name,
                                     summary=issue.fields.summary, labels=jiralabels, assignee=issue.fields.assignee,
                                     components=jiracomponents,creator=issue.fields.creator,jiracreated=jiracreated,jiraupdated=jiraupdated,
                                                priority=prio,
                                        issuelink=issue.raw['self'],
                                     lastupdate=datetime.datetime.now(), publiclink='https://sanone.atlassian.net/browse/'+issue.key)
            issueinfo.save()
        else:
            issueinfo.status = issue.fields.status.name
            issueinfo.summary = issue.fields.summary
            issueinfo.assignee = issue.fields.assignee
            issueinfo.creator = issue.fields.creator

            issueinfo.labels = jiralabels
            issueinfo.components = jiracomponents
            issueinfo.lastupdate = datetime.datetime.now()
            issueinfo.jiracreated=jiracreated
            issueinfo.jiraupdated=jiraupdated
            issueinfo.priority= prio #issue.fields.priority
            issueinfo.sp=sp
            issueinfo.pl=pl
            issueinfo.pt=pt
            issueinfo.uk=uk
            issueinfo.hotfix=hotfix
            issueinfo.countryhotfix=hotfixcountry
            issueinfo.corebug=bugcore
            issueinfo.uatbug=uatbug
            issueinfo.autobug=autobug
            issueinfo.ptowned=ptowned
            issueinfo.core=core
            issueinfo.publiclink='https://sanone.atlassian.net/browse/'+issue.key
            ve = ''
            if issue.fields.fixVersions is not None:
                for v in issue.fields.fixVersions:
                    ve = ve + v.name +','
                issueinfo.fixversions=ve
            issueinfo.save()
