import json
from datetime import datetime

from gestmetricas.models import Feature, TestsQA, TestsQA2, StepsDefinition, StepsDefinition2, IosTestRun, BackgroundSt, \
    BackgroundSteps, IosTestResults, StepsResultsIos, ScenarioSteps, AndroidTestRun, AndroidTestResults, StepsResults
from gestmetricas.utils.jsonparser.cucumberparser import CucumberOutputElement

def createrun( dt, target, techno, inputtype, country):
    testrunname = techno + '_' + target + '_'+ inputtype +'_'+country+'_'+ str(dt.year) + str(dt.month) + str(dt.day)
    if techno == 'IOS':
        testrun = IosTestRun.objects.filter(testname=testrunname).first()

        if testrun is None:
            testrun = IosTestRun(testdate=dt, testname=testrunname, teststotal=0, testsok=0,
                             testsko=0, testserror=0, testsskipped=0, runtype=inputtype, country=country)
            testrun.save()
        return testrun
    else:
        testrun = AndroidTestRun.objects.filter(testname=testrunname).first()

        if testrun is None:
            testrun = AndroidTestRun(testdate=dt, testname=testrunname, teststotal=0, testsok=0,
                                 testsko=0, testserror=0, testsskipped=0, runtype=inputtype, country=country )
            testrun.save()
        return testrun


def getelementnamefromerror(errormsg1):
    if 'NoSuchElementException' in errormsg1:
        try:
            parts = errormsg1.split("Element info: ")
            part2 = parts[1]
            elems = part2.split("}")
            elem = elems[0]
            return elem
        except Exception as e:
            print(e)
            return ''
    return ''


def createdeffromjson( jsonstr, target, techno, country):
    firstscenario = None
    testrun = None
    runelems = json.loads(jsonstr)
    for z in runelems:
        feature = CucumberOutputElement(z)
        a = feature.name
        feat = Feature.objects.filter(name=a).first()
        if feat is None:
            feat = Feature( name=a, featureid=feature.id)
            feat.save()
        print(a)

        testsqacol = TestsQA.objects.filter(feature=a).all()
        for testqa in testsqacol:
            if testqa.featureid is None:
                testqa.featureid=feat
                testqa.save()
                print('Updating ' + testqa.test)
            # carga de steps
        for elem in feature.elements:
            steps = elem['steps']
            for step in steps:
                stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition(stepname=step["name"], steptype=step["keyword"].upper())
                    stepsdef.save()

        currbackground = None
        currbackgroundname =''
        for elem in feature.elements:
            if elem['type'] == 'background' and currbackground is None:
                currbackgroundname=elem['name']
                currbackground = elem
                backgr = BackgroundSt.objects.filter(name=currbackgroundname, featureid= feat.name).first()
                if backgr is None:
                    backgr = BackgroundSt(name=currbackgroundname, featureid= feat.name)
                    backgr.save()

                for step in steps:
                    stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                    backgrstep = BackgroundSteps.objects.filter(backgroundid=backgr, stepid=stepsdef).first()
                    if backgrstep is None:
                        backgrstep = BackgroundSteps(backgroundid=backgr, stepid=stepsdef)
                        backgrstep.save()
                break

        for elem in feature.elements:
            if elem['type']=='scenario':
                if firstscenario is None:
                    firstscenario=elem
                    # crear RUN con la fecha del primer escenario
                    dt1 = datetime.strptime(firstscenario['start_timestamp'], "%Y-%m-%dT%H:%M:%S.%fZ")
                    testrun = createrun(dt1, target, techno, 'JSON', country)
                curscen = TestsQA.objects.filter(test= elem['name']).first()
                if curscen is None:
                    curscen =   TestsQA(test=elem['name'], testqaid=1, feature=feature.name, background=currbackgroundname,
                            scenario='', comments='', featureid=feat,
                            qastatus='')
                    curscen.save()
                    print('Saving '+ elem['name'])



def createrunfromjson( jsonstr, target, techno, country):
    firstscenario = None
    testrun = None
    runelems = json.loads(jsonstr)
    for z in runelems:
        feature = CucumberOutputElement(z)
        a = feature.name
        numscenarios = 0
        numsteps = 0
        numstepsbck = 0
        feat = Feature.objects.filter(name=a).first()
        if feat is None:
            feat = Feature( name=a, featureid=feature.id)
            feat.save()
        print(a)

        testsqacol = TestsQA.objects.filter(feature=a).all()
        for testqa in testsqacol:
            if testqa.featureid is None:
                testqa.featureid=feat
                testqa.save()
                print('Updating ' + testqa.test)
            # carga de steps
        for elem in feature.elements:
            if elem['keyword'] =='Scenario':
                numscenarios = numscenarios +1
                numsteps =  numsteps +len(elem['steps'])
            if elem['keyword'] =='Background':
                numstepsbck = len(elem['steps'])

            steps = elem['steps']
            for step in steps:
                stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition(stepname=step["name"], steptype=step["keyword"].upper())
                    stepsdef.save()
        feat.numsteps = numsteps
        feat.numscenarios = numscenarios
        feat.numbackgsteps = numstepsbck
        feat.save()
        currbackground = None
        currbackgroundname =''
        for elem in feature.elements:
            if elem['type'] == 'background' and currbackground is None:
                currbackgroundname=elem['name']
                currbackground = elem
                backgr = BackgroundSt.objects.filter(name=currbackgroundname, featureid= feat.name).first()
                if backgr is None:
                    backgr = BackgroundSt(name=currbackgroundname, featureid= feat.name)
                    backgr.save()

                for step in steps:
                    stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                    backgrstep = BackgroundSteps.objects.filter(backgroundid=backgr, stepid=stepsdef).first()
                    if backgrstep is None:
                        backgrstep = BackgroundSteps(backgroundid=backgr, stepid=stepsdef)
                        backgrstep.save()
                break

        prevBackgroundSteps=[]
        for elem in feature.elements:
            if elem['type']=='background':
                prevBackgroundSteps = []
                for step in elem['steps']:
                    prevBackgroundSteps.append(step)
            if elem['type']=='scenario':
                if firstscenario is None:
                    firstscenario=elem
                    # crear RUN con la fecha del primer escenario
                    dt1 = datetime.strptime(firstscenario['start_timestamp'], "%Y-%m-%dT%H:%M:%S.%fZ")
                    testrun = createrun(dt1, target, techno, 'JSON', country)
                curscen = TestsQA.objects.filter(test= elem['name']).first()
                if curscen is None:
                    curscen =   TestsQA(test=elem['name'], testqaid=1, feature=feature.name, background=currbackgroundname,
                            scenario='', comments='', featureid=feat,
                            qastatus='')
                    curscen.save()
                    print('Saving '+ elem['name'])
                else:
                    curscen.featureid=feat
                    curscen.background=currbackgroundname
                    curscen.save()
                testname = elem['name']
                dur = elem['after'][0]['result']['duration']
                stat = elem['after'][0]['result']['status']
                dur = dur/1000
                contstepok=0
                contstepko=0
                contstepskipped=0
                testfinalstatus= stat
                haserrorid = False

                testresults = IosTestResults.objects.filter(test=curscen, runid=testrun).first()
                if testresults is None:
                    testresults = IosTestResults(runid=testrun, test=curscen, estado=stat, duration=dur, feature=feat, country=country)
                    testresults.save()
                else:
                    testresults.feature=feat
                    testresults.duration = dur
                    testresults.estado=stat
                    testresults.country=country
                    testresults.save()
                loginerror = False
                accessibilityerror = False
                loginerrormsg =''
                totalSteps = []
                for xx in prevBackgroundSteps:
                    totalSteps.append(xx)
                for step1 in elem['steps']:
                    totalSteps.append(step1)

                for step in totalSteps:
                    wrningmsg = ''
                    errormsg =''
                    accessid = ''
                    stepstatus = step['result']['status']
                    if 'duration' in step['result']:
                        stepdur = step['result']['duration']
                    else:
                        stepdur = 0
                    if stepstatus == 'failed':
                        contstepko = contstepko + 1
                        errormsg = step['result']['error_message']
                        if len(errormsg) > 100:
                            accessid = getelementnamefromerror(errormsg)
                        else:
                            accessid = ''
                        haserrorid = False
                        if len(accessid) > 2:
                            haserrorid = True
                            print( accessid)
                        if "The login process has failed" in errormsg:
                            loginerror=True
                            loginerrormsg=errormsg
                        if '{' in errormsg:
                            accessibilityerror = True
                        errormsg = errormsg[:3000]
                        testfinalstatus = 'failed'
                    elif stepstatus == 'skipped':
                        contstepskipped = contstepskipped + 1
                    else:
                        contstepok = contstepok+1
                    stepdur = stepdur / 1000
                    stepsdef1 = StepsDefinition.objects.filter(stepname=step["name"]).first()
                    if stepsdef1 is None:
                        stepsdef1 = StepsDefinition(stepname=step["name"], steptype=step["keyword"].upper())
                        stepsdef1.save()
                    stepexec = StepsResultsIos.objects.filter(testid=curscen, runid=testrun, stepid=stepsdef1, resultsid=testresults).first()
                    if stepexec is None:
                        stepexec = StepsResultsIos(testid=curscen, runid=testrun, stepid=stepsdef1, resultsid=testresults,
                                               haswarning=wrningmsg, haserror= errormsg,hasaccerrorid=haserrorid,
                                               accesibilityid=accessid, estado=stepstatus, duration=stepdur)
                    else:
                        stepexec.haserror=errormsg
                        stepexec.accesibilityid=accessid
                        stepexec.hasaccerrorid = haserrorid
                        stepexec.haswarning=wrningmsg
                        stepexec.estado=stepstatus
                        stepexec.duration=stepdur

                    try:
                        stepexec.save()
                        scenstep = ScenarioSteps.objects.filter(scenario=curscen, stepid=stepsdef1).first()
                        if scenstep is None:
                            scenstep = ScenarioSteps(scenario=curscen, stepid=stepsdef1)
                            scenstep.save()
                    except Exception as e:
                        print(e)

                testresults.stepsok = contstepok
                testresults.stepsko = contstepko
                testresults.stepsskipped = contstepskipped
                testresults.estado = testfinalstatus
                testresults.loginerror=loginerror
                testresults.loginerrortext=loginerrormsg
                testresults.accessibilityerror = accessibilityerror
                testresults.save()
                print('Saved scenario result : ' + curscen.test + " --- "+ testresults.estado)

                prevBackgroundSteps = []
    return testrun


def createandroidrunfromjson( jsonstr, target, techno, country):
    firstscenario = None
    testrun = None
    runelems = json.loads(jsonstr)
    for z in runelems:
        feature = CucumberOutputElement(z)
        a = feature.name
        numscenarios = 0
        numsteps = 0
        numstepsbck = 0
        feat = Feature.objects.filter(name=a).first()
        if feat is None:
            feat = Feature( name=a, featureid=feature.id)
            feat.save()
        print(a)

        testsqacol = TestsQA.objects.filter(feature=a).all()
        for testqa in testsqacol:
            if testqa.featureid is None:
                testqa.featureid=feat
                testqa.save()
                print('Updating ' + testqa.test)
            # carga de steps
        for elem in feature.elements:
            if elem['keyword'] =='Scenario':
                numscenarios = numscenarios +1
                numsteps =  numsteps +len(elem['steps'])
                print("Escenario: " + elem['name'])
            if elem['keyword'] =='Background':
                numstepsbck = len(elem['steps'])

            steps = elem['steps']
            for step in steps:
                stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                if stepsdef is None:
                    stepsdef = StepsDefinition(stepname=step["name"], steptype=step["keyword"].upper())
                    stepsdef.save()
        feat.numsteps = numsteps
        feat.numscenarios = numscenarios
        feat.numbackgsteps = numstepsbck
        feat.save()
        currbackground = None
        currbackgroundname =''
        for elem in feature.elements:
            if elem['type'] == 'background' and currbackground is None:
                currbackgroundname=elem['name']
                currbackground = elem
                backgr = BackgroundSt.objects.filter(name=currbackgroundname, featureid= feat.name).first()
                if backgr is None:
                    backgr = BackgroundSt(name=currbackgroundname, featureid= feat.name)
                    backgr.save()

                for step in steps:
                    stepsdef = StepsDefinition.objects.filter(stepname=step["name"]).first()
                    backgrstep = BackgroundSteps.objects.filter(backgroundid=backgr, stepid=stepsdef).first()
                    if backgrstep is None:
                        backgrstep = BackgroundSteps(backgroundid=backgr, stepid=stepsdef)
                        backgrstep.save()
                break
        prevBackgroundSteps=[]
        for elem in feature.elements:
            if elem['type']=='background':
                prevBackgroundSteps = []
                for step in elem['steps']:
                    prevBackgroundSteps.append(step)

            if elem['type']=='scenario':
                if firstscenario is None:
                    firstscenario=elem
                    # crear RUN con la fecha del primer escenario
                    dt1 = datetime.strptime(firstscenario['start_timestamp'], "%Y-%m-%dT%H:%M:%S.%fZ")
                    testrun = createrun(dt1, target, techno, 'JSON', country)
                curscen = TestsQA.objects.filter(test= elem['name']).first()
                if curscen is None:
                    curscen =   TestsQA(test=elem['name'], testqaid=1, feature=feature.name, background=currbackgroundname,
                            scenario='', comments='', featureid=feat,
                            qastatus='')
                    curscen.save()
                    print('Saving '+ elem['name'])
                else:
                    curscen.featureid=feat
                    curscen.background=currbackgroundname
                    curscen.save()
                testname = elem['name']
                dur = elem['after'][0]['result']['duration']
                stat = elem['after'][0]['result']['status']
                dur = dur/1000
                contstepok=0
                contstepko=0
                contstepskipped=0
                testfinalstatus= stat
                haserrorid = False

                testresults = AndroidTestResults.objects.filter(test=curscen, runid=testrun).first()
                if testresults is None:
                    testresults = AndroidTestResults(runid=testrun, test=curscen, estado=stat, duration=dur, feature=feat, country=country)
                    testresults.save()
                else:
                    testresults.feature=feat
                    testresults.duration = dur
                    testresults.estado=stat
                    testresults.country=country
                    testresults.save()
                loginerror = False
                accessibilityerror = False
                loginerrormsg =''
                totalSteps = []
                for xx in prevBackgroundSteps:
                    totalSteps.append(xx)
                for step1 in elem['steps']:
                    totalSteps.append(step1)

                for step in totalSteps:
                    wrningmsg = ''
                    errormsg =''
                    accessid = ''
                    stepstatus = step['result']['status']
                    if 'duration' in step['result']:
                        stepdur = step['result']['duration']
                    else:
                        stepdur = 0
                    if stepstatus == 'failed':
                        contstepko = contstepko + 1
                        errormsg = step['result']['error_message']
                        if len(errormsg) > 100:
                            accessid=getelementnamefromerror(errormsg)
                        else:
                            accessid = ''
                        haserrorid = False
                        if len(accessid) > 2:
                            haserrorid = True
                            print( accessid)
                        if "The login process has failed" in errormsg:
                            loginerror=True
                            loginerrormsg=errormsg
                        if '{' in errormsg:
                            accessibilityerror = True

                        errormsg = errormsg[:3000]

                        testfinalstatus = 'failed'
                    elif stepstatus == 'skipped':
                        contstepskipped = contstepskipped + 1
                    else:
                        contstepok = contstepok+1
                    stepdur = stepdur / 1000
                    stepsdef1 = StepsDefinition.objects.filter(stepname=step["name"]).first()
                    if stepsdef1 is None:
                        stepsdef1 = StepsDefinition(stepname=step["name"], steptype=step["keyword"].upper())
                        stepsdef1.save()
                    stepexec = StepsResults.objects.filter(testid=curscen, runid=testrun, stepid=stepsdef1, resultsid=testresults).first()
                    if stepexec is None:
                        stepexec = StepsResults(testid=curscen, runid=testrun, stepid=stepsdef1, resultsid=testresults,
                                               haswarning=wrningmsg, haserror= errormsg,hasaccerrorid=haserrorid,
                                               accesibilityid=accessid, estado=stepstatus, duration=stepdur)
                    else:
                        stepexec.haserror=errormsg
                        stepexec.accesibilityid=accessid
                        stepexec.hasaccerrorid = haserrorid
                        stepexec.haswarning=wrningmsg
                        stepexec.estado=stepstatus
                        stepexec.duration=stepdur

                    try:
                        stepexec.save()
                        scenstep = ScenarioSteps.objects.filter(scenario=curscen, stepid=stepsdef1).first()
                        if scenstep is None:
                            scenstep = ScenarioSteps(scenario=curscen, stepid=stepsdef1)
                            scenstep.save()
                    except Exception as e:
                        print(e)

                testresults.stepsok = contstepok
                testresults.stepsko = contstepko
                testresults.stepsskipped = contstepskipped
                testresults.estado = testfinalstatus
                testresults.loginerror=loginerror
                testresults.loginerrortext=loginerrormsg
                testresults.accessibilityerror = accessibilityerror
                testresults.save()
                print('Saved scenario result : ' + curscen.test + " --- "+ testresults.estado)

                prevBackgroundSteps = []
    return testrun


