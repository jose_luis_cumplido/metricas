from enum import Enum
from dataclasses import dataclass
from typing import Optional, List, Any
from datetime import datetime


class Location(Enum):
    AppiumTestkt14 = "AppiumTest.kt:14"
    AppiumTestkt23 = "AppiumTest.kt:23"
    AppiumTestkt27 = "AppiumTest.kt:27"


@dataclass
class AfterMatch:
    location: Location


class Status(Enum):
    failed = "failed"
    passed = "passed"
    pending = "pending"
    skipped = "skipped"


@dataclass
class AfterResult:
    status: Status
    duration: Optional[int] = None


@dataclass
class After:
    result: AfterResult
    match: AfterMatch


class ElementKeyword(Enum):
    Background = "Background"
    Scenario = "Scenario"
    ScenarioOutline = "Scenario Outline"


class StepKeyword(Enum):
    And = "And "
    Given = "Given "
    Then = "Then "
    When = "When "


@dataclass
class Argument:
    val: str
    offset: int


@dataclass
class StepMatch:
    location: str
    arguments: Optional[List[Argument]] = None


@dataclass
class StepResult:
    status: Status
    duration: Optional[int] = None
    errormessage: Optional[str] = None


@dataclass
class Step:
    result: StepResult
    before: List[After]
    line: int
    name: str
    match: StepMatch
    after: List[After]
    keyword: StepKeyword


@dataclass
class Tag:
    name: str


class TypeEnum(Enum):
    background = "background"
    scenario = "scenario"


@dataclass
class Element:
    line: int
    name: str
    description: str
    type: TypeEnum
    keyword: ElementKeyword
    steps: List[Step]
    starttimestamp: Optional[datetime] = None
    id: Optional[str] = None
    after: Optional[List[After]] = None
    tags: Optional[List[Tag]] = None


class CucumberOutputKeyword(Enum):
    Feature = "Feature"


@dataclass
class CucumberOutputElement:
    line: int
    elements: List[Element]
    name: str
    description: str
    id: str
    keyword: CucumberOutputKeyword
    uri: str
    tags: List[Any]

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                setattr(self, key, dictionary[key])
        for key in kwargs:
            setattr(self, key, kwargs[key])