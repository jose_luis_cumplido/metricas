import os
from io import StringIO, BytesIO
from metricas import settings

import pysftp as sftp

def downloadall( rundate):
  host = 'sftp.ciber-es.com'
  port = 22
  username = 'autoReportsReadOnly'
  password= 'gZ03We9mHqE4'

  cnopts = sftp.CnOpts()
  cnopts.hostkeys = None

  try:
    conn = sftp.Connection(host=host,port=port,username=username, password=password, cnopts=cnopts)
    print("connection established successfully")
    current_dir = conn.pwd
    print('our current working directory is: ', current_dir)
    print('available list of directories: ', conn.listdir())
    countries =['spain', 'portugal','poland','uk']
    plats=['ios', 'android']
    envs =['master','develop']

    runpath =settings.FTP_DOWNLOAD_DIR +rundate
  #  runpath ='/Users/jose/jenkinsreports/'+rundate
    if not os.path.exists(runpath):
      os.makedirs(runpath)
    for country in countries:
      p1 = runpath+'/'+country
      if not os.path.exists(p1):
        os.makedirs(p1)
      for plat in plats:
        p2 = p1 + '/' + plat
        if not os.path.exists(p2):
          os.makedirs(p2)
        for env1 in envs:
          p3 = p2 + '/' + env1
          if not os.path.exists(p3):
            os.makedirs(p3)
          try:
            with conn.cd( country+'/'+plat+'/'+env1):
              r = BytesIO()
              conn.get('cucumber.json', p3 + '/cucumber.json')
              a = 1
              print("Copiado " + p3)
          except Exception as e:
            print('Error  con ' +  country+'/'+plat+'/'+env1)
            print(e)

  except Exception as e:
    print( e)