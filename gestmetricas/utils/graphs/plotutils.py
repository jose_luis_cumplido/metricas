import matplotlib.pyplot as plt
import numpy as np

def pltcreatepie( pltdata ):
    names = ["1", "2", "3", "4", "5"]
    work = []

    colors = ['yellow', 'b', 'green', 'cyan', 'red']

    # plotting pie chart
    plt.pie(pltdata, labels=names, colors=colors, startangle=90,
            shadow=True, radius=1.2, autopct='%1.1f%%')
    plt.savefig('graphimages/my_plot.png')
    plt.close()

def pltcreatepierun( pltdata, pltname ):
    names = ["OK", "KO", "SKIP","LOGIN ERROR", "IDERROR"]
    work = []

    colors = ['green', 'red', 'yellow', 'orange', 'grey']

    # plotting pie chart
    try:
        plt.pie(pltdata, labels=names, colors=colors, startangle=90,
            shadow=True, radius=1.2, autopct='%1.1f%%')
        plt.savefig('graphimages/'+pltname+'.png')
    except Exception as e:
        print('Error en pltpie')
    plt.close()


def pltcreatebarruns( pltdata, allnames, imgname):
    SMALL_SIZE = 8
    MEDIUM_SIZE = 10
    BIGGER_SIZE = 12

    plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=SMALL_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)
    n_groups = len(pltdata[0])
    colors =['b','g','r','y']
    # create plot
    fig, ax = plt.subplots()
    index = np.arange(n_groups)
    alllabels=['All', 'OK', 'KO', 'Skip']
    bar_width = 0.25
    opacity = 0.8
    contador = 0
    bars=[]
    for d1 in pltdata:
        rects1 = plt.bar(index + bar_width * contador , d1, bar_width,
                     alpha=opacity,
                     color=colors[contador],
                     label= alllabels[contador])
        contador = contador + 1


    plt.xlabel('Tests')
    plt.ylabel('Results')
    plt.title('Tests runs')
    plt.xticks(index + bar_width,allnames, rotation=60)
    for bar in rects1:
        yval = bar.get_height()
        plt.text(bar.get_x(), yval + .005, yval)
    plt.legend()

    plt.tight_layout()

    plt.savefig('graphimages/bars/'+imgname+ '.png')
    plt.close()