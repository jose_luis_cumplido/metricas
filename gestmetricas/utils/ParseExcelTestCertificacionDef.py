
import pandas as pd
import json

from django.db.models import Q

from gestmetricas.models import TestDefinition, StatusInfo, StatusInfoPL, TestCertificacion, TestCertificacionRunVersion


def parseExcelTestCertificacionDef( excelname , sheetname ):
  #  xls = pd.ExcelFile(excelname)
    df1 = pd.read_excel(excelname, sheet_name = sheetname)
    df1.columns = df1.iloc[0]
    df1.fillna('', inplace=True)

    dictdni = dict()
    for x in df1.index:
        i = x +1
        print("Procesando linea " + str(i))
        testid=df1['ID'][i]
        bloque=df1['BLOCK'][i]
        testname=df1['TEST'][i]
        automatizadoios=df1['AUTO IOS'][i]
        automatizadoandroid=df1['AUTO ANDROID'][i]
        manual=df1['MANUAL'][i]
        statusqa=df1['STATUS'][i]
        scenarioqa=df1['SCENARIO'][i]
        comentario=df1['COMMENTS'][i]
        iscore=df1['CORE'][i]
        issp=df1['SP'][i]
        ispt=df1['PT'][i]
        ispl=df1['PL'][i]
        isuk=df1['UK'][i]
        tdo = TestCertificacion.objects.filter(testid=testid).first()
        if tdo is None:
            tdo = TestCertificacion( testid=testid, test=testname, bloque=bloque, comentario=comentario,
                                            autoios=automatizadoios, autoandroid=automatizadoandroid, manual=manual,
                                     testqastatus=statusqa, scenarioqa=scenarioqa, iscore=iscore, issp=issp,
                                     ispt=ispt, ispl=ispl, isuk=isuk)
            tdo.save()
        else:
            tdo.autoios=automatizadoios
            tdo.autoandroid=automatizadoandroid
            tdo.comentario=comentario
            tdo.manual=manual
            tdo.iscore=iscore
            tdo.ispl=ispl
            tdo.issp=issp
            tdo.ispt=ispt
            tdo.isuk=isuk
            tdo.save()



def parseExcelTestCertificacionEjecucion( excelname , sheetname, relversion, countrysel ):
  #  xls = pd.ExcelFile(excelname)
    try:
        df1 = pd.read_excel(excelname, sheet_name = sheetname)
    except Exception as e:
        df1 = pd.read_excel(excelname, sheet_name='Hoja1')
    #detectar fila de headers
    for r in range(10):
        val1 = df1.iat[r,0]
        print(val1)
        if val1 == 'ID':
            break
    contadorSinIds = 0
    df1.columns = df1.iloc[r]
    df1.fillna('', inplace=True)

    dictdni = dict()
    for x in df1.index:
        i = x + r + 1
        try:
            testid = df1['ID'][i]
        except Exception as e:
            print('FIn de resultados')
            return
        print("Procesando linea " + str(i))
        testid=df1['ID'][i]
        bloque=df1['BLOCK'][i]
        testname=df1['TEST'][i]
        automatizadoios=df1['AUTO IOS'][i]
        automatizadoandroid=df1['AUTO ANDROID'][i]
        manual=df1['MANUAL'][i]

        comentario=df1['COMMENTS'][i]
        iscore=df1['CORE'][i]
        country=df1['COUNTRY'][i]
        tester=df1['TESTER'][i]

        deviceios=df1['DEVICE IOS'][i]
        versionios=df1['APP VERSION IOS'][i]
        envios=df1['ENV IOS'][i]
        userios=df1['USER IOS'][i]
        manresultios=df1[' MANUAL REPORT IOS'][i]
        autoresultios=df1['AUTO REPORT IOS'][i]
        bugjiraios=df1['BUG JIRA IOS'][i]
        priorityios=df1['PRIORITY IOS'][i]

        deviceandroid=df1['DEVICE ANDROID'][i]
        versionandroid=df1['APP VERSION ANDROID'][i]
        envandroid=df1['ENV IOS'][i]
        userandroid=df1['USER ANDROID'][i]
        manresultandroid=df1['MANUAL REPORT ANDROID'][i]
        autoresultandroid=df1['AUTO REPORT ANDROID'][i]
        bugjiraandroid=df1['BUG JIRA ANDROID'][i]
        priorityandroid=df1['PRIORITY ANDROID'][i]
        if testid == '  ' or testid == '':
            testid=100000 + contadorSinIds
            contadorSinIds = contadorSinIds + 1
        if manual == '' and automatizadoios=='':
            manual='YES'
        if comentario=='NA':
            print(comentario)
        tdo = TestCertificacionRunVersion.objects.filter(testid=testid, country=countrysel, releaseversion=relversion).first()
        if tdo is None:
            tdo = TestCertificacionRunVersion( testid=testid, test=testname, bloque=bloque, comentario=comentario,
                                            autoios=automatizadoios, autoandroid=automatizadoandroid, manual=manual,
                                      scenarioqa='', iscore=iscore, country=countrysel,
                                     tester=tester, deviceios=deviceios,versionios=versionios, envios=envios, userios=userios, manresultios=manresultios,
                                     autoresultios=autoresultios, bugjiraios=bugjiraios,priorityios=priorityios,
                                     deviceandroid=deviceandroid, versionandroid=versionandroid,envandroid=envandroid,
                                     userandroid=userandroid, manresultandroid=manresultandroid,autoresultandroid=autoresultandroid,
                                     bugjiraandroid=bugjiraandroid,priorityandroid=priorityandroid,
                                               releaseversion=relversion)
            tdo.save()
        else:
            tdo.country=countrysel
            tdo.autoios=automatizadoios
            tdo.autoandroid=automatizadoandroid
            tdo.comentario=comentario
            tdo.manual=manual
            tdo.iscore=iscore
            tdo.tester=tester
            tdo.deviceios=deviceios
            tdo.deviceandroid=deviceandroid
            tdo.versionios=versionios
            tdo.userios=userios
            tdo.manresultios=manresultios
            tdo.autoresultios=autoresultios
            tdo.bugjiraios=bugjiraios
            tdo.priorityios=priorityios
            tdo.versionandroid=versionandroid
            tdo.userandroid=userandroid
            tdo.manresultandroid=manresultandroid
            tdo.autoresultandroid=autoresultandroid
            tdo.bugjiraandroid=bugjiraandroid
            tdo.priorityandroid=priorityandroid
            tdo.save()





