import django
from django.db import models

# Create your models here.


class ExcelMetricas(models.Model):
    ordenoriginal=models.IntegerField(default=0)
    colororiginal=models.CharField(max_length=20, blank=True)
    striken= models.BooleanField(default=False)
    faseoriginal=models.CharField(max_length=20, blank=True)
    faseoriginalint=models.IntegerField(default=0)
    modificado=models.DateTimeField(blank=True, null=True, default='2000-01-01 00:00:00')
    uuidimport=models.CharField(max_length=100, blank=True)
    screen_title= models.CharField(max_length=200, blank=True)
    accion_evento= models.CharField(max_length=100, blank=True)
    descripcion= models.CharField(max_length=1000, blank=True)
    android= models.CharField(max_length=20, blank=True)
    ios= models.CharField(max_length=20, blank=True)
    comentarios_android= models.CharField(max_length=200, blank=True)
    comentarios_ios= models.CharField(max_length=200, blank=True)
    tieneparametros= models.BooleanField()
    importe= models.CharField(max_length=20, blank=True)
    divisa= models.CharField(max_length=20, blank=True)
    tipo_operacion= models.CharField(max_length=100, blank=True)
    tipo_pg= models.CharField(max_length=20, blank=True)
    tipo_foto= models.CharField(max_length=20, blank=True)
    cod_error= models.CharField(max_length=20, blank=True)
    desc_error= models.CharField(max_length=20, blank=True)
    termino_busqueda= models.CharField(max_length=20, blank=True)
    id_oferta= models.CharField(max_length=20, blank=True)
    recordar_usuario= models.CharField(max_length=20, blank=True)
    tipo_acceso_login= models.CharField(max_length=20, blank=True)
    deeplink_login= models.CharField(max_length=20, blank=True)
    telefono= models.CharField(max_length=20, blank=True)
    products_id= models.CharField(max_length=20, blank=True)
    location= models.CharField(max_length=20, blank=True)
    tipo_tarjeta= models.CharField(max_length=20, blank=True)
    id_categoria= models.CharField(max_length=20, blank=True)
    ticker= models.CharField(max_length=20, blank=True)
    participaciones= models.CharField(max_length=20, blank=True)
    num_gestores_sant_personal= models.CharField(max_length=20, blank=True)
    num_gestores_oficina= models.CharField(max_length=20, blank=True)
    tipo_envio= models.CharField(max_length=40, blank=True)
    tipo_envio_programado= models.CharField(max_length=40, blank=True)
    idioma= models.CharField(max_length=100, blank=True)
    num_meses= models.CharField(max_length=20, blank=True)
    tipo_gestor= models.CharField(max_length=20, blank=True)
    tipo_usuario= models.CharField(max_length=20, blank=True)
    tipo_viaje= models.CharField(max_length=20, blank=True)
    tipo_movimiento= models.CharField(max_length=20, blank=True)
    tipo_operacion_1= models.CharField(max_length=100, blank=True)
    tipo_busqueda= models.CharField(max_length=20, blank=True)
    operativa_cuentas= models.CharField(max_length=20, blank=True)
    operativa_tarjetas= models.CharField(max_length=20, blank=True)
    estado_recibos= models.CharField(max_length=20, blank=True)
    tipo_recibo= models.CharField(max_length=20, blank=True)
    tipo_historico_transfe= models.CharField(max_length=20, blank=True)
    tipo_operatividad= models.CharField(max_length=20, blank=True)
    id_idea= models.CharField(max_length=20, blank=True)
    operar= models.CharField(max_length=20, blank=True)
    tipo_envio_bizum= models.CharField(max_length=20, blank=True)
    tipo_simple_multiple= models.CharField(max_length=20, blank=True)
    tipo_bizum_historico= models.CharField(max_length=20, blank=True)
    acceso_directo= models.CharField(max_length=20, blank=True)
    opcion_perfil_digital_no_completada= models.CharField(max_length=20, blank=True)
    id_faq= models.CharField(max_length=20, blank=True)
    extra= models.CharField(max_length=1000, blank=True)


class ExcelUpdates(models.Model):
    actualizadopor= models.CharField(max_length=100, blank=True)
    ultimaactualizacion=models.DateTimeField(blank=True, null=True, default='2000-01-01 00:00:00')
    excelfile = models.FileField(upload_to='metricasexcel', default=None)
    uuid=models.CharField(max_length=100, blank=True)


class ExcelExports(models.Model):
    actualizadopor= models.CharField(max_length=100, blank=True)
    ultimaactualizacion=models.DateTimeField(blank=True, null=True, default='2000-01-01 00:00:00')
    excelfile = models.FileField(upload_to='metricasexcel', default=None)
    uuid=models.CharField(max_length=100, blank=True)


class TestDefinition(models.Model):
    testid=models.IntegerField(default=0)
    bloque=models.CharField(max_length=50,blank=True)
    test=models.CharField(max_length=400,blank=True)
    automatizado=models.CharField(max_length=50,blank=True)
    testname=models.CharField(max_length=450,blank=True)
    corelocal=models.CharField(max_length=50,blank=True)
    comentario=models.CharField(max_length=1000,blank=True)
    androidpre=models.BooleanField(default=False)
    androidalfa=models.BooleanField(default=False)
    iospre=models.BooleanField(default=False)
    iostestflight=models.BooleanField(default=False)


class Feature(models.Model):
    name=models.CharField(max_length=100,blank=True)
    featureid=models.CharField(max_length=100,blank=True)
    numscenarios=models.IntegerField(default=0)
    numsteps=models.IntegerField(default=0)
    numbackgsteps=models.IntegerField(default=0)
    extra=models.CharField(max_length=1000,blank=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    iscritical=models.BooleanField(default=False)
    coresp=models.BooleanField(default=False)
    corept=models.BooleanField(default=False)
    corepl=models.BooleanField(default=False)
    coreuk=models.BooleanField(default=False)
    module=models.CharField(max_length=200,blank=True)



class BackgroundSt(models.Model):
    name=models.CharField(max_length=100,blank=True)
    featureid=models.CharField(max_length=100,blank=True)
    numsteps=models.IntegerField(default=0)
    extra=models.CharField(max_length=1000,blank=True)


class TestsQA(models.Model):
    testqaid=models.IntegerField(default=0)
    feature=models.CharField(max_length=350,blank=True)
    test=models.CharField(max_length=400,blank=True)
    qastatus=models.CharField(max_length=50,blank=True)
    scenario=models.CharField(max_length=350,blank=True)
    background=models.CharField(max_length=350,blank=True)
    comments=models.CharField(max_length=350,blank=True)
    devstatus=models.CharField(max_length=20,blank=True)
    featureid=models.ForeignKey(Feature,on_delete=models.CASCADE,null=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    iscritical=models.BooleanField(default=False)
    coresp=models.BooleanField(default=False)
    corept=models.BooleanField(default=False)
    corepl=models.BooleanField(default=False)
    coreuk=models.BooleanField(default=False)
    fromlocalrun=models.BooleanField(default=False)


class StatusInfo(models.Model):
    testid=models.IntegerField(default=0)
    bloque=models.CharField(max_length=50,blank=True)
    test=models.CharField(max_length=400,blank=True)
    status=models.CharField(max_length=50,blank=True)
    scenario=models.CharField(max_length=250,blank=True)
    comments=models.CharField(max_length=350,blank=True)
    scenarioid=models.ForeignKey(TestsQA,on_delete=models.CASCADE,null=True)
    coresp=models.BooleanField(default=False)
    corept=models.BooleanField(default=False)
    corepl=models.BooleanField(default=False)
    coreuk=models.BooleanField(default=False)
    country=models.CharField(max_length=20,blank=True)


class StatusInfoPL(models.Model):
    testid=models.IntegerField(default=0)
    bloque=models.CharField(max_length=50,blank=True)
    test=models.CharField(max_length=400,blank=True)
    statuscore=models.CharField(max_length=50,blank=True)
    statuspl=models.CharField(max_length=50,blank=True)
    scenario=models.CharField(max_length=250,blank=True)
    comments=models.CharField(max_length=350,blank=True)
    scenarioid=models.ForeignKey(TestsQA,on_delete=models.CASCADE,null=True)
    coresp=models.BooleanField(default=False)
    corept=models.BooleanField(default=False)
    corepl=models.BooleanField(default=False)
    coreuk=models.BooleanField(default=False)
    country=models.CharField(max_length=20,blank=True)


class TestsQA2(models.Model):
    testqaid=models.IntegerField(default=0)
    feature=models.CharField(max_length=350,blank=True)
    test=models.CharField(max_length=400,blank=True)
    qastatus=models.CharField(max_length=50,blank=True)
    scenario=models.CharField(max_length=350,blank=True)
    background=models.CharField(max_length=350,blank=True)
    comments=models.CharField(max_length=350,blank=True)
    testdef=models.ForeignKey(TestDefinition,on_delete=models.CASCADE,null=True)
    devstatus=models.CharField(max_length=20,blank=True)
    featureid=models.ForeignKey(Feature,on_delete=models.CASCADE,null=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)



class IosTestRun(models.Model):
    testname = models.CharField(max_length=200, blank=True)
    testdate=models.DateTimeField( default= django.utils.timezone.now)
    teststotal=models.IntegerField(default=0)
    testsok=models.IntegerField(default=0)
    testsko=models.IntegerField(default=0)
    testserror=models.IntegerField(default=0)
    testsskipped=models.IntegerField(default=0)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    runtype = models.CharField(max_length=20, blank=True)
    corecodeversion = models.CharField(max_length=40, blank=True)
    appversion = models.CharField(max_length=40, blank=True)
    environment = models.CharField(max_length=40, blank=True)
    epic = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True, default="")


class IosTestResults(models.Model):
    runid=models.ForeignKey(IosTestRun,on_delete=models.CASCADE,default=1)
    test=models.ForeignKey(TestsQA,on_delete=models.CASCADE,default=1)
    feature=models.ForeignKey(Feature,on_delete=models.CASCADE,default=1)
    estado=models.CharField(max_length=20,blank=True)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    stepspending=models.IntegerField(default=0)
    duration=models.IntegerField(default=0)
    log=models.CharField(max_length=4000,blank=True)
    country = models.CharField(max_length=100, blank=True, default="")
    loginerror = models.BooleanField(default=False)
    loginerrortext= models.CharField(max_length=4000,blank=True,default='')
    accessibilityerror = models.BooleanField(default=False)


class AndroidTestRun(models.Model):
    testname = models.CharField(max_length=200, blank=True)
    testdate=models.DateTimeField( default= django.utils.timezone.now)
    teststotal=models.IntegerField(default=0)
    testsok=models.IntegerField(default=0)
    testsko=models.IntegerField(default=0)
    testserror=models.IntegerField(default=0)
    testsskipped=models.IntegerField(default=0)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    runtype = models.CharField(max_length=20, blank=True)
    corecodeversion = models.CharField(max_length=40, blank=True)
    appversion = models.CharField(max_length=40, blank=True)
    environment = models.CharField(max_length=40, blank=True)
    epic = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True, default="")


class AndroidTestResults(models.Model):
    runid=models.ForeignKey(AndroidTestRun,on_delete=models.CASCADE,default=1)
    test=models.ForeignKey(TestsQA,on_delete=models.CASCADE,default=1)
    feature=models.ForeignKey(Feature,on_delete=models.CASCADE,default=1)
    estado=models.CharField(max_length=20,blank=True)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    stepspending=models.IntegerField(default=0)
    duration=models.IntegerField(default=0)
    log=models.CharField(max_length=4000,blank=True)
    country = models.CharField(max_length=100, blank=True, default="")
    loginerror = models.BooleanField(default=False)
    loginerrortext= models.CharField(max_length=4000,blank=True,default='')
    accessibilityerror = models.BooleanField(default=False)


class StepsDefinition(models.Model):
    stepname=models.CharField(max_length=400,blank=True)
    steptype=models.CharField(max_length=20,blank=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)


class StepsDefinition2(models.Model):
    stepname=models.CharField(max_length=400,blank=True)
    steptype=models.CharField(max_length=20,blank=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)


class SubStepsDefinition(models.Model):
    substepname=models.CharField(max_length=400,blank=True)
    steptype=models.CharField(max_length=20,blank=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)


class LogTestDefinition(models.Model):
    logtestname=models.CharField(max_length=400,blank=True)
    logtesttype=models.CharField(max_length=20,blank=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    testid = models.ForeignKey(TestsQA, on_delete=models.CASCADE, default=1)
    numberofsteps=models.IntegerField(default=0)


class StepsResults  (models.Model):
    runid=models.ForeignKey(AndroidTestRun,on_delete=models.CASCADE,default=1)
    stepid = models.ForeignKey(StepsDefinition, on_delete=models.CASCADE, default=1, related_name="stepsinres")
    testid = models.ForeignKey(TestsQA, on_delete=models.CASCADE, default=1)
    resultsid = models.ForeignKey(AndroidTestResults, on_delete=models.CASCADE, null=True )
    estado=models.CharField(max_length=20,blank=True, default='')
    duration=models.IntegerField(default=0)
    haswarning=models.CharField(max_length=15000,blank=True, default='')
    haserror=models.CharField(max_length=4000,blank=True, default='')
    accesibilityid=models.CharField(max_length=400,blank=True, default='')
    hasaccerrorid=models.BooleanField(default=False)


class StepsResultsIos  (models.Model):
    runid=models.ForeignKey(IosTestRun,on_delete=models.CASCADE,default=1)
    stepid = models.ForeignKey(StepsDefinition, on_delete=models.CASCADE, default=1, related_name="stepsinresios")
    testid = models.ForeignKey(TestsQA, on_delete=models.CASCADE, default=1)
    resultsid = models.ForeignKey(IosTestResults, on_delete=models.CASCADE, null=True )
    estado=models.CharField(max_length=20,blank=True, default='')
    duration=models.IntegerField(default=0)
    haswarning=models.CharField(max_length=15000,blank=True, default='')
    haserror=models.CharField(max_length=4000,blank=True, default='')
    accesibilityid=models.CharField(max_length=400,blank=True, default='')
    hasaccerrorid=models.BooleanField(default=False)


class LogTestResultsIos  (models.Model):
    runid=models.ForeignKey(IosTestRun,on_delete=models.CASCADE,default=1)
    logtestid = models.ForeignKey(LogTestDefinition, on_delete=models.CASCADE, default=1, related_name="logtestinresios")
    testid = models.ForeignKey(TestsQA, on_delete=models.CASCADE, default=1)
    resultsid = models.ForeignKey(IosTestResults, on_delete=models.CASCADE, null=True )
    estado=models.CharField(max_length=20,blank=True, default='')
    duration=models.IntegerField(default=0)
    haswarning=models.TextField(max_length=15000,blank=True, default='')
    haserror=models.CharField(max_length=4000,blank=True, default='')
    accesibilityid=models.CharField(max_length=400,blank=True, default='')
    hasaccerrorid=models.BooleanField(default=False)


class LogTestResultsAndroid  (models.Model):
    runid=models.ForeignKey(IosTestRun,on_delete=models.CASCADE,default=1)
    logtestid = models.ForeignKey(LogTestDefinition, on_delete=models.CASCADE, default=1, related_name="logtestinresandroid")
    testid = models.ForeignKey(TestsQA, on_delete=models.CASCADE, default=1)
    resultsid = models.ForeignKey(AndroidTestResults, on_delete=models.CASCADE, null=True )
    estado=models.CharField(max_length=20,blank=True, default='')
    duration=models.IntegerField(default=0)
    haswarning=models.TextField(max_length=15000,blank=True, default='')
    haserror=models.CharField(max_length=4000,blank=True, default='')
    accesibilityid=models.CharField(max_length=2000,blank=True, default='')
    hasaccerrorid=models.BooleanField(default=False)


class BackgroundSteps  (models.Model):
    backgroundid = models.ForeignKey(BackgroundSt, on_delete=models.CASCADE, null=True )
    stepid = models.ForeignKey(StepsDefinition, on_delete=models.CASCADE, null=True )


class ScenarioSteps  (models.Model):
    scenario = models.ForeignKey(TestsQA, on_delete=models.CASCADE, null=True )
    stepid = models.ForeignKey(StepsDefinition, on_delete=models.CASCADE, null=True )


class HistoryPerTestIos (models.Model):
    runid=models.ForeignKey(IosTestRun,on_delete=models.CASCADE,default=1)
    test=models.ForeignKey(TestsQA,on_delete=models.CASCADE,default=1)
    result=models.ForeignKey(IosTestResults, on_delete=models.CASCADE, null=True)
    prevoktoko=models.BooleanField(default=False)
    prevkotook=models.BooleanField( default=False)
    country=models.CharField(max_length=20, default='')
    estado=models.CharField(max_length=20, default='')
    testname=models.CharField(max_length=500, default='')


class HistoryPerTestAndroid (models.Model):
    runid=models.ForeignKey(AndroidTestRun,on_delete=models.CASCADE,default=1)
    test=models.ForeignKey(TestsQA,on_delete=models.CASCADE,default=1)
    result=models.ForeignKey(AndroidTestResults, on_delete=models.CASCADE, null=True)
    prevoktoko=models.BooleanField(default=False)
    prevkotook=models.BooleanField(default=False)
    country=models.CharField(max_length=20, default='')
    estado=models.CharField(max_length=20, default='')
    testname=models.CharField(max_length=500, default='')


class CountryInfo( models.Model):
    country = models.CharField(max_length=40)
    countrycode = models.CharField(max_length=4)
    coreversion = models.CharField(max_length=40, blank=True)
    lastreleaseversion = models.CharField(max_length=40, blank=True)
    proversion = models.CharField(max_length=40, blank=True)
    updatedate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    lasttested=models.DateTimeField(  null=True)
    confluencepage=models.CharField(max_length=40, blank=True)



class ConfluenceInfo( models.Model):
    country = models.CharField(max_length=40)
    countrycode = models.CharField(max_length=20)
    confpageid = models.CharField(max_length=40, blank=True)
    lastupdate=models.DateTimeField(  null=True)
    pagename= models.CharField(max_length=200, default='')



class JiraInfo( models.Model):
    issueid = models.CharField(max_length=40)
    labels = models.CharField(max_length=200, default="")
    testid = models.ForeignKey(TestsQA, on_delete=models.CASCADE, null=True,  related_name="jiraissues")
    ticettype =   models.CharField(max_length=40, default="")
    status =   models.CharField(max_length=40, default="")
    description =   models.CharField(max_length=4000, default="")
    summary =   models.CharField(max_length=1000, default="")
    lastupdate=models.DateTimeField(  null=True)
    labels=models.CharField(max_length=400, default='')
    fixversions=models.CharField(max_length=400,default='')
    creator=models.CharField(max_length=100,default='')
    jiracreated=models.DateTimeField(  null=True)
    assignee=models.CharField(max_length=100,default='',null=True)
    jiraupdated=models.DateTimeField(  null=True)
    components=models.CharField(max_length=100,default='')


class JiraCertificationReport( models.Model):
    reportrelease = models.CharField(max_length=200, default="")
    issueid = models.CharField(max_length=40)
    labels = models.CharField(max_length=200, default="")
    testid = models.ForeignKey(TestsQA, on_delete=models.CASCADE, null=True,  related_name="jiraissuescert")
    ticettype =   models.CharField(max_length=40, default="")
    status =   models.CharField(max_length=40, default="")
    description =   models.CharField(max_length=4000, default="")
    summary =   models.CharField(max_length=1000, default="")
    lastupdate=models.DateTimeField(  null=True)
    labels=models.CharField(max_length=400, default='')
    fixversions=models.CharField(max_length=400,default='')
    creator=models.CharField(max_length=100,default='')
    jiracreated=models.DateTimeField(  null=True)
    assignee=models.CharField(max_length=100,default='',null=True)
    jiraupdated=models.DateTimeField(  null=True)
    components=models.CharField(max_length=100,default='')
    issuelink=models.CharField(max_length=400,default='')
    publiclink=models.CharField(max_length=400,default='')
    autobug=models.CharField(max_length=10,default='')
    uatbug=models.CharField(max_length=10,default='')
    corebug=models.CharField(max_length=10,default='')
    sp=models.CharField(max_length=10,default='')
    pt=models.CharField(max_length=10,default='')
    pl=models.CharField(max_length=10,default='')
    uk=models.CharField(max_length=10,default='')
    ptowned=models.CharField(max_length=10,default='')
    hotfix=models.CharField(max_length=10,default='')
    countryhotfix=models.CharField(max_length=10,default='')
    priority=models.CharField(max_length=20,default='')
    core=models.CharField(max_length=10,default='')






class ZeplinProject( models.Model):
    id = models.CharField(max_length=40, primary_key=True)
    name = models.CharField(max_length=140)
    thumbnail = models.CharField(max_length=400)
    platform = models.CharField(max_length=400)
    status = models.CharField(max_length=40)
    organization = models.CharField(max_length=140)
    numberofscreens = models.IntegerField(default=0)
    numberofcomponents = models.IntegerField(default=0)
    numberoftextstyles = models.IntegerField(default=0)
    numberofcolors = models.IntegerField(default=0)
    numberofmembers = models.IntegerField(default=0)
    linkedstyleguide= models.CharField(max_length=40)
    description= models.CharField(max_length=40, default='')


class ZeplinScreen( models.Model):
    id = models.CharField(max_length=40, primary_key=True)
    name = models.CharField(max_length=140)
    tags = models.CharField(max_length=240)
    image = models.CharField(max_length=6000)
    section = models.CharField(max_length=240)
    numberofversions = models.IntegerField(default=0)
    numberofnotes = models.IntegerField(default=0)
    project = models.ForeignKey(ZeplinProject,  on_delete=models.CASCADE)


class ZeplinScreenVersion(models.Model):
    id = models.CharField(max_length=40, primary_key=True)
    imageurl = models.CharField(max_length=6000 , default='')
    source = models.CharField(max_length=400, default='')
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    screen = models.ForeignKey(ZeplinScreen, related_name='versions', on_delete=models.CASCADE)


class ZeplinScreenComponent( models.Model):
    id = models.CharField(max_length=40, primary_key=True)
    name = models.CharField(max_length=240)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    absx = models.IntegerField(default=0)
    absy = models.IntegerField(default=0)
    elemtype=models.CharField(max_length=40, default='')
    screen = models.ForeignKey(ZeplinScreen,  on_delete=models.CASCADE)
    screenversion = models.ForeignKey(ZeplinScreenVersion,  on_delete=models.CASCADE)


class TestCertificacion(models.Model):
    testid=models.IntegerField(default=0)
    bloque=models.CharField(max_length=50,blank=True)
    test=models.CharField(max_length=400,blank=True)
    autoios=models.CharField(max_length=10,blank=True)
    autoandroid=models.CharField(max_length=10,blank=True)
    manual=models.CharField(max_length=10,blank=True)
    testqastatus=models.CharField(max_length=200,blank=True)
    scenarioqa=models.CharField(max_length=400,blank=True)
    comentario=models.CharField(max_length=1000,blank=True)
    comentario_pt = models.CharField(max_length=1000, blank=True)
    comentario_pl = models.CharField(max_length=1000, blank=True)
    comentario_uk = models.CharField(max_length=1000, blank=True)
    iscore=models.CharField(max_length=10,blank=True)
    issp=models.CharField(max_length=10,blank=True)
    ispt=models.CharField(max_length=10,blank=True)
    ispl=models.CharField(max_length=10,blank=True)
    isuk=models.CharField(max_length=10,blank=True)
    iospre=models.BooleanField(default=False)
    iostestflight=models.BooleanField(default=False)
    updatedate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    releasename=models.CharField(max_length=40,blank=True, default='')
    logtest=models.ForeignKey(LogTestDefinition,on_delete=models.CASCADE,null=True)


class TestCertificacionRunVersion(models.Model):
    testid=models.IntegerField(default=0)
    bloque=models.CharField(max_length=50,blank=True)
    test=models.CharField(max_length=400,blank=True)
    autoios=models.CharField(max_length=20,blank=True)
    autoandroid=models.CharField(max_length=20,blank=True)
    manual=models.CharField(max_length=20,blank=True)
    testqastatus=models.CharField(max_length=200,blank=True)
    scenarioqa=models.CharField(max_length=400,blank=True)
    comentario=models.CharField(max_length=1000,blank=True)
    comentario_pt=models.CharField(max_length=1000,blank=True)
    comentario_pl=models.CharField(max_length=1000,blank=True)
    comentario_uk=models.CharField(max_length=1000,blank=True)
    iscore=models.CharField(max_length=10,blank=True)
    country=models.CharField(max_length=20,blank=True)
    releaseversion=models.CharField(max_length=100,blank=True)
    updatedate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    tester=models.CharField(max_length=100,blank=True)
    deviceios=models.CharField(max_length=50,blank=True)
    versionios=models.CharField(max_length=50,blank=True)
    envios=models.CharField(max_length=20,blank=True)
    userios=models.CharField(max_length=20,blank=True)
    manresultios=models.CharField(max_length=100,blank=True)
    autoresultios=models.CharField(max_length=100,blank=True)
    bugjiraios=models.CharField(max_length=200,blank=True)
    priorityios=models.CharField(max_length=20,blank=True)
    deviceandroid=models.CharField(max_length=50,blank=True)
    versionandroid=models.CharField(max_length=100,blank=True)
    envandroid=models.CharField(max_length=20,blank=True)
    userandroid=models.CharField(max_length=20,blank=True)
    manresultandroid=models.CharField(max_length=100,blank=True)
    autoresultandroid=models.CharField(max_length=200,blank=True)
    bugjiraandroid=models.CharField(max_length=200,blank=True)
    priorityandroid=models.CharField(max_length=20,blank=True)


class TestsQA2Cert(models.Model):
    testqaid=models.IntegerField(default=0)
    feature=models.CharField(max_length=350,blank=True)
    test=models.CharField(max_length=400,blank=True)
    qastatus=models.CharField(max_length=50,blank=True)
    scenario=models.CharField(max_length=350,blank=True)
    background=models.CharField(max_length=350,blank=True)
    comments=models.CharField(max_length=350,blank=True)
    testdef=models.ForeignKey(TestCertificacion,on_delete=models.CASCADE,null=True)
    devstatus=models.CharField(max_length=20,blank=True)
    featureid=models.ForeignKey(Feature,on_delete=models.CASCADE,null=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)


class FeatureCert(models.Model):
    name=models.CharField(max_length=100,blank=True)
    featureid=models.CharField(max_length=100,blank=True)
    numscenarios=models.IntegerField(default=0)
    numsteps=models.IntegerField(default=0)
    numbackgsteps=models.IntegerField(default=0)
    extra=models.CharField(max_length=1000,blank=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    iscritical=models.BooleanField(default=False)
    coresp=models.BooleanField(default=False)
    corept=models.BooleanField(default=False)
    corepl=models.BooleanField(default=False)
    coreuk=models.BooleanField(default=False)
    bloque=models.CharField(max_length=200,blank=True)


class TestsQACert(models.Model):
    testqaid=models.IntegerField(default=0)
    feature=models.CharField(max_length=350,blank=True)
    test=models.CharField(max_length=400,blank=True)
    qastatus=models.CharField(max_length=50,blank=True)
    scenario=models.CharField(max_length=350,blank=True)
    background=models.CharField(max_length=350,blank=True)
    comments=models.CharField(max_length=350,blank=True)
    devstatus=models.CharField(max_length=20,blank=True)
    featureid=models.ForeignKey(FeatureCert,on_delete=models.CASCADE,null=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)
    iscritical=models.BooleanField(default=False)
    coresp=models.BooleanField(default=False)
    corept=models.BooleanField(default=False)
    corepl=models.BooleanField(default=False)
    coreuk=models.BooleanField(default=False)
    fromlocalrun=models.BooleanField(default=False)


class StepsDefinitionCert(models.Model):
    stepname=models.CharField(max_length=400,blank=True)
    steptype=models.CharField(max_length=20,blank=True)
    creationdate=models.DateTimeField( default= django.utils.timezone.now, null=True)



class IosTestRunCert(models.Model):
    testname = models.CharField(max_length=200, blank=True)
    testdate=models.DateTimeField( default= django.utils.timezone.now)
    teststotal=models.IntegerField(default=0)
    testsok=models.IntegerField(default=0)
    testsko=models.IntegerField(default=0)
    testserror=models.IntegerField(default=0)
    testsskipped=models.IntegerField(default=0)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    runtype = models.CharField(max_length=20, blank=True)
    corecodeversion = models.CharField(max_length=40, blank=True)
    appversion = models.CharField(max_length=40, blank=True)
    environment = models.CharField(max_length=40, blank=True)
    epic = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True)


class IosTestResultsCert(models.Model):
    runid=models.ForeignKey(IosTestRunCert,on_delete=models.CASCADE,default=1)
    test=models.ForeignKey(TestsQACert,on_delete=models.CASCADE,default=1)
    feature=models.ForeignKey(FeatureCert,on_delete=models.CASCADE,default=1)
    estado=models.CharField(max_length=20,blank=True)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    stepspending=models.IntegerField(default=0)
    duration=models.IntegerField(default=0)
    log=models.CharField(max_length=4000,blank=True)
    country = models.CharField(max_length=100, blank=True)


class StepsResultsIosCert  (models.Model):
    runid=models.ForeignKey(IosTestRunCert,on_delete=models.CASCADE,default=1)
    stepid = models.ForeignKey(StepsDefinitionCert, on_delete=models.CASCADE, default=1, related_name="stepsinresios")
    testid = models.ForeignKey(TestsQACert, on_delete=models.CASCADE, default=1)
    resultsid = models.ForeignKey(IosTestResultsCert, on_delete=models.CASCADE, null=True )
    estado=models.CharField(max_length=20,blank=True, default='')
    duration=models.IntegerField(default=0)
    haswarning=models.CharField(max_length=4000,blank=True, default='')
    haserror=models.CharField(max_length=4000,blank=True, default='')
    accesibilityid=models.CharField(max_length=400,blank=True, default='')
    hasaccerrorid=models.BooleanField(default=False)




class AndroidTestRunCert(models.Model):
    testname = models.CharField(max_length=200, blank=True)
    testdate=models.DateTimeField( default= django.utils.timezone.now)
    teststotal=models.IntegerField(default=0)
    testsok=models.IntegerField(default=0)
    testsko=models.IntegerField(default=0)
    testserror=models.IntegerField(default=0)
    testsskipped=models.IntegerField(default=0)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    runtype = models.CharField(max_length=20, blank=True)
    corecodeversion = models.CharField(max_length=40, blank=True)
    appversion = models.CharField(max_length=40, blank=True)
    environment = models.CharField(max_length=40, blank=True)
    epic = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True)


class AndroidTestResultsCert(models.Model):
    runid=models.ForeignKey(AndroidTestRunCert,on_delete=models.CASCADE,default=1)
    test=models.ForeignKey(TestsQACert,on_delete=models.CASCADE,default=1)
    feature=models.ForeignKey(FeatureCert,on_delete=models.CASCADE,default=1)
    estado=models.CharField(max_length=20,blank=True)
    stepsok=models.IntegerField(default=0)
    stepsko=models.IntegerField(default=0)
    stepsskipped=models.IntegerField(default=0)
    stepspending=models.IntegerField(default=0)
    duration=models.IntegerField(default=0)
    log=models.CharField(max_length=4000,blank=True)
    country = models.CharField(max_length=100, blank=True)


class StepsResultsAndroidCert  (models.Model):
    runid=models.ForeignKey(AndroidTestRunCert,on_delete=models.CASCADE,default=1)
    stepid = models.ForeignKey(StepsDefinitionCert, on_delete=models.CASCADE, default=1, related_name="stepsinresandroid")
    testid = models.ForeignKey(TestsQACert, on_delete=models.CASCADE, default=1)
    resultsid = models.ForeignKey(AndroidTestResultsCert, on_delete=models.CASCADE, null=True )
    estado=models.CharField(max_length=20,blank=True, default='')
    duration=models.IntegerField(default=0)
    haswarning=models.CharField(max_length=4000,blank=True, default='')
    haserror=models.CharField(max_length=4000,blank=True, default='')
    accesibilityid=models.CharField(max_length=400,blank=True, default='')
    hasaccerrorid=models.BooleanField(default=False)
