import os
from collections import defaultdict
from datetime import datetime

from django.db.models import Q
from django.http import HttpResponseServerError, HttpResponse, JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from requests import Response
from rest_framework import viewsets, status, decorators, response, permissions
from django.contrib.auth.models import User

from gestmetricas.models import TestCertificacion, TestCertificacionRunVersion, IosTestResultsCert, StepsResultsIosCert, \
    StepsResultsAndroidCert, AndroidTestResultsCert, IosTestResults, StepsResultsIos, StepsResults, AndroidTestResults, \
    IosTestRun, ConfluenceInfo, AndroidTestRun, TestsQA, HistoryPerTestAndroid, HistoryPerTestIos
from gestmetricas.utils.ParseExcelTestCertificacionDef import parseExcelTestCertificacionDef, \
    parseExcelTestCertificacionEjecucion
from xlsxwriter import Workbook

from gestmetricas.utils.ftputils import downloadall
from gestmetricas.utils.graphs.plotutils import pltcreatepierun
from gestmetricas.utils.jsonparser.parsefile import createrunfromjson, createandroidrunfromjson
from gestmetricas.views import fixcountersios2, fixcountersandroid2, creahtml, updatepage, uploadimagerun, \
    creahtmlmaster, uploadexceltoconfl


@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def analizechanges(request):
    tipo= request.GET.get("tipo")
    firstrunnumber= request.GET.get("runid")
    pais= request.GET.get("pais")
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if firstrunnumber is None or len(firstrunnumber) == 0:
        firstrunnumber=40
    maphist = defaultdict(dict)
    alltestdefs = TestsQA.objects.all()
    #Android
    for testdef in alltestdefs:
        if pais is None or pais == '':
            testsall = AndroidTestResults.objects.filter(test=testdef, runid_id__gt=firstrunnumber).order_by('id','runid_id').all()
        else:
            testsall=AndroidTestResults.objects.filter( test=testdef, runid_id__gt=firstrunnumber, runid__country=pais).order_by('id', 'runid_id').all()
        for test1 in testsall:
            hist = HistoryPerTestAndroid.objects.filter( result=test1).first()
            if hist is None:
                hist = HistoryPerTestAndroid( result=test1, runid=test1.runid, test=test1.test, country=test1.country, testname=test1.test.test)
                hist.save()
            prevhist=HistoryPerTestAndroid.objects.filter(runid_id__lt=hist.runid_id, test=test1.test, country=hist.country).order_by('-runid').first()
            if prevhist is not None:
                if prevhist.result.estado=='failed' and hist.result.estado=='passed':
                    hist.prevkotook=True
                    hist.prevoktoko=False
                elif  prevhist.result.estado=='passed' and hist.result.estado=='failed':
                    hist.prevkotook=False
                    hist.prevoktoko=True
                else:
                    hist.prevkotook=False
                    hist.prevoktoko=False
                hist.estado=hist.result.estado
                hist.save()

           # maphist[test1.test_id][test1.id] = res1
            print("===> " + str(test1.test_id) + ' - ' + str(
                test1.runid_id) + '---  ' + test1.test.test + ' --- ' + test1.estado + ' -- ' + str(
                hist.prevkotook) + ' ++ ' + str( hist.prevoktoko))
    #IOS
    for testdef in alltestdefs:
        if pais is None or pais=='':
            testsall = IosTestResults.objects.filter(test=testdef, runid_id__gt=firstrunnumber).order_by('id','runid_id').all()
        else:
            testsall = IosTestResults.objects.filter(test=testdef, runid_id__gt=firstrunnumber, runid__country=pais).order_by('id','runid_id').all()

        for test1 in testsall:
            hist = HistoryPerTestIos.objects.filter(result=test1).first()
            if hist is None:
                hist = HistoryPerTestIos(result=test1, runid=test1.runid, test=test1.test, country=test1.country,
                                             testname=test1.test.test)
                hist.save()
            prevhist = HistoryPerTestIos.objects.filter(runid_id__lt=hist.runid_id, test=test1.test, country=hist.country).order_by(
                '-runid').first()
            if prevhist is not None:
                if prevhist.result.estado == 'failed' and hist.result.estado == 'passed':
                    hist.prevkotook = True
                    hist.prevoktoko = False
                elif prevhist.result.estado == 'passed' and hist.result.estado == 'failed':
                    hist.prevkotook = False
                    hist.prevoktoko = True
                else:
                    hist.prevkotook = False
                    hist.prevoktoko = False
                hist.estado = hist.result.estado
                hist.save()

            # maphist[test1.test_id][test1.id] = res1
            print("===> " + str(test1.test_id) + ' - ' + str(
                test1.runid_id) + '---  ' + test1.test.test + ' --- ' + test1.estado + ' -- '+str(hist.prevkotook) + ' ++ ' + str(hist.prevoktoko))

    numvers = len(maphist)
    #for a in maphist:
    return HttpResponse( status.HTTP_200_OK)





@decorators.api_view(["GET"])
@decorators.permission_classes([permissions.IsAuthenticated])
def  createexcelhistorychanges(request):
    tipo= request.GET.get("tipo")
    filter= request.GET.get("filter")
    runnumber= request.GET.get("runid")
    country= request.GET.get("pais")
    fromdate= request.GET.get("fromdate")
    datefrom = None
    if runnumber is None or runnumber=='':
        runnumber=0
    else:
        runnumber=int(runnumber)
    if fromdate is None or fromdate=='':
        print("All dates ")
    else:
        datefrom = datetime.strptime(fromdate, '%d/%m/%Y')
    if filter is None:
        filter='ALL'
    if tipo is None:
        return HttpResponse(status.HTTP_400_BAD_REQUEST)
    if tipo== 'IOS':
        if runnumber is None or runnumber =='':
            runs = IosTestRun.objects.filter(country=country).all()
            runnumber=1
        else:
            runs=IosTestRun.objects.filter(country=country, id__gt=runnumber).all()
    else:
        if runnumber is None or runnumber =='':
            runs = AndroidTestRun.objects.filter(country=country).all()
        else:
            runs=AndroidTestRun.objects.filter( country=country,  id__gt=runnumber).all()

    runcolumns=[]
    runcoumnsvalues={}
    for r in runs:

        if filter=='MASTER':
            if '_DEVELOP_' in r.testname:
                continue

        if country is None or country=='ALL':
            print( 'Country ' + r.country)
        else:
            if r.country != country:
                print(' Bypassing Country ' + r.country)
                continue
        runcolumns.append(r.id)

        tname= r.testname


    book = Workbook( 'graphimages/excel/HISTORYALL_'+country+'_'+tipo+'.xlsx')
    cell_format = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format2 = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'red','border':1, 'border_color':'black'})
    cell_format_ios = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'853a7d', 'border':1,'border_color':'black'})
    cell_format_android = book.add_format({'bold': True, 'font_color': 'white', 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color':'9760a3','border':1, 'border_color':'black'})
    worksheet = book.add_worksheet( 'HISTORY_' +country+'_'+tipo)
    merge_format = book.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'fg_color': 'yellow', 'locked':True})

    worksheet.merge_range('A2:D2', ' Historic results : ' + country + '  from ' + fromdate)

    # Light red fill with dark red text.
    format1 = book.add_format({'bg_color': '#FFC7CE',
                                   'font_color': '#9C0006'})

    # Light yellow fill with dark yellow text.
    format2 = book.add_format({'bg_color': '#FFEB9C',
                                   'font_color': '#9C6500'})

    # Green fill with dark green text.
    format3 = book.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'failed',
                                           'format': format1})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                           'criteria': 'containing',
                                           'value': 'passed',
                                           'format': format3})
    worksheet.conditional_format('D4:D500', {'type': 'text',
                                             'criteria': 'containing',
                                             'value': 'skipped',
                                             'format': format2})
    row = 3
    col = 0
    worksheet.set_column(0, 0, 5)
    worksheet.set_column(1,1, 25)
    worksheet.set_column(2,2, 35)
    worksheet.set_column(3, 5, 10)
    worksheet.set_column(6, 20, 8)


    worksheet.write(2, 0, 'ID', cell_format)
    worksheet.write(2, 1, 'FEATURE', cell_format )
    worksheet.write(2, 2, 'TEST', cell_format)
    worksheet.write(2, 3, 'STATUS', cell_format)
    worksheet.write(2, 4, 'OS', cell_format)
    worksheet.write(2, 5, 'COUNTRY', cell_format)
    f = 6
    for col in runcolumns:
        worksheet.write (2, f, str(col), cell_format)
        runcoumnsvalues[col] = f

        f = f +1



    cell_format_data = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','border':1, 'border_color':'black'})
    cell_format_data_ios = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center', 'bg_color': '#65b4ba', 'border':1,'border_color':'black'})
    cell_format_data_android = book.add_format({ 'font_size' : 12,'text_wrap': True, 'align':'center','bg_color': '#9dbd77','border':1, 'border_color':'black'})

    disttestsqa=[] # = HistoryPerTestIos.objects.filter(country=country, runid_id__gt=(runcolumns[0] -1 )).order_by('runid_id').distinct('test_id').all()
    idsincluded=[]
    if tipo == 'IOS':
        alltests = HistoryPerTestIos.objects.filter(country=country, runid_id__gt=runnumber).order_by('runid_id').all()
    else:
        alltests = HistoryPerTestAndroid.objects.filter(country=country, runid_id__gt=runnumber).order_by('runid_id').all()

    for htest in alltests:

        if htest.test_id not  in idsincluded:
            disttestsqa.append(htest)
            idsincluded.append(htest.test_id)
    for disttest in disttestsqa:
        if tipo == 'IOS':
            testrows = HistoryPerTestIos.objects.filter( country=country, test=disttest.test ).order_by('runid_id').all()
        else:
            testrows = HistoryPerTestAndroid.objects.filter( country=country, test=disttest.test ).order_by('runid_id').all()

        worksheet.write(row, 0, disttest.test.id, )
        worksheet.write(row, 1, disttest.test.featureid.name,cell_format_data)
        worksheet.write(row, 2, disttest.test.test,cell_format_data)
        worksheet.write(row, 3, disttest.test.qastatus,cell_format_data)
        worksheet.write(row, 4, tipo)
        worksheet.write(row, 5, disttest.country)

        cont=6
        for testline in testrows:
            if testline.prevoktoko:
                try:
                    worksheet.write(row, runcoumnsvalues[testline.runid_id], testline.estado, format1)
                except Exception as e:
                    print(e)
            elif testline.prevkotook:
                try:
                    worksheet.write(row, runcoumnsvalues[testline.runid_id], testline.estado, format3)
                except Exception as e:
                    print(e)
            else:
                if testline.estado=='passed':
                    try:
                        worksheet.write(row, runcoumnsvalues[testline.runid_id], testline.estado, format3)
                    except Exception as e:
                        print(e)
                else:
                    try:
                        worksheet.write(row, runcoumnsvalues[testline.runid_id], testline.estado, cell_format_data)
                    except Exception as e:
                        print(e)
            cont = cont + 1

        row += 1

    book.close()
    confall = ConfluenceInfo.objects.filter(country='HISTORYALL').first()


    print("Created excel for "  + country+'_'+tipo)

    if tipo == 'IOS':
        uploadexceltoconfl(confall.confpageid, 'HISTORYALL_'+ country+'_'+tipo )
    else:
        uploadexceltoconfl(confall.confpageid, 'HISTORYALL_'+country+'_'+tipo )

    return HttpResponse( status.HTTP_201_CREATED)



